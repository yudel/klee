#ifndef KLEE_STATICANALYSER_H
#define KLEE_STATICANALYSER_H

#include"CallGraph.h"

#include"klee/Config/Version.h"

#include"llvm/Module.h"
#include"llvm/Support/raw_ostream.h"

#include<set>

namespace klee {

class StaticAnalyser {
 public:
 StaticAnalyser()
   : module(NULL), cg(NULL), mapConstructed(false) {}
 StaticAnalyser(llvm::Module *m)
   : module(m), cg(NULL), mapConstructed(false) {}
  
  ~StaticAnalyser() {
    if (cg)
      delete cg;
  }

  void setModule(llvm::Module *m) { module = m; }

  CallGraph *getCG() { return cg; }

  int isSkippingSupported(const llvm::Function *f);
  void support(const llvm::Function *f, bool yes);

  bool isMapConstructed() { return mapConstructed; }
  bool functionUsesAny(const llvm::Function *f);
  bool functionUses(const llvm::Function *f,
		    const llvm::GlobalVariable *global);

  void setMapConstructed() { mapConstructed = true; }
  void constructFunctionGlobalChain(const llvm::GlobalVariable *global);
  
  void constructCallGraph() {
    if (module) {
      if (!cg) {
	cg = new CallGraph(module);
	cg->initialise();
      }
    } else {
      llvm::errs() << "No module to construct call graph!\n";
    }
  }

 private:
  llvm::Module *module;
  CallGraph *cg;
  bool mapConstructed;
  typedef std::set<const llvm::GlobalVariable *> GS;
  typedef std::map<const llvm::Function *, GS> MFTOGS;
  MFTOGS fgMap;
  std::set<const llvm::Function *> supported;
  std::set<const llvm::Function *> unsupported;

  void insertChain(const llvm::Function *f,
		   const llvm::GlobalVariable *global,
		   std::set<const llvm::Function *> &visited);
};

} // namespace klee

#endif
