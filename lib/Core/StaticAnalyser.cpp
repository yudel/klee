#include"StaticAnalyser.h"

#if LLVM_VERSION_CODE >= LLVM_VERSION(3, 3)
#include "llvm/IR/Instructions.h"
#else
#include "llvm/Instructions.h"
#endif

#include<vector>

using namespace klee;

int StaticAnalyser::isSkippingSupported(const llvm::Function *f) {
  if (supported.find(f) != supported.end())
    return 1;
  else if (unsupported.find(f) != unsupported.end())
    return 0;
  else
    return -1; // unknown
}
void StaticAnalyser::support(const llvm::Function *f, bool yes) {
  std::set<const llvm::Function *> *knowledge;
  if (yes)
    knowledge = &supported;
  else
    knowledge = &unsupported;
  knowledge->insert(f);
}

bool StaticAnalyser::functionUsesAny(const llvm::Function *f) {
  MFTOGS::iterator found = fgMap.find(f);
  if (found != fgMap.end())
    return true;
  return false;
}
bool StaticAnalyser::functionUses(const llvm::Function *f,
				  const llvm::GlobalVariable *global) {
  MFTOGS::iterator found = fgMap.find(f);
  if (found != fgMap.end())
    if (found->second.find(global) != found->second.end())
      return true;
  return false;
}

void StaticAnalyser::insertChain(const llvm::Function *f,
				 const llvm::GlobalVariable *global,
				 std::set<const llvm::Function *> &visited) {
  std::pair<MFTOGS::iterator, bool> insertion =
    fgMap.insert(std::pair<const llvm::Function *, GS>(f, GS()));
  if (insertion.second) // newly inserted
    insertion.first->second.insert(global);
  else // existed
    insertion.first->second.insert(global);
  visited.insert(f);
  for (llvm::Function::const_use_iterator ui = f->use_begin(),
	 uj = f->use_end(); ui != uj; ++ui) {
    if (const llvm::CallInst *ci =
	llvm::dyn_cast<const llvm::CallInst>(*ui)) {
      const llvm::Function *caller = ci->getParent()->getParent();
      if (visited.find(caller) == visited.end())
	insertChain(caller, global, visited);
    } else if (const llvm::InvokeInst *vi =
	       llvm::dyn_cast<const llvm::InvokeInst>(*ui)) {
      const llvm::Function *caller = vi->getParent()->getParent();
      if (visited.find(caller) == visited.end())
	insertChain(caller, global, visited);
    } else if (const llvm::StoreInst *si =
	       llvm::dyn_cast<const llvm::StoreInst>(*ui)) {
      if (si->getValueOperand() == f) {
	const llvm::Value *ptr = si->getPointerOperand();
	for (llvm::Value::const_use_iterator vcui = ptr->use_begin(),
	       vcuj = ptr->use_end(); vcui != vcuj; ++vcui) {
	  const llvm::User *user = *vcui;
	  if (const llvm::Function *alias =
	      llvm::dyn_cast<const llvm::Function>(user)) {
	    if (visited.find(alias) == visited.end())
	      insertChain(alias, global, visited);
	  } else {
	    // did some checking, this case might not matter
	  }
	}
      } else {
	llvm::errs() << "Unexpected store that uses function\n";
      }
    } else if (const llvm::Constant *c =
	     llvm::dyn_cast<const llvm::Constant>(*ui)) {
      if (const llvm::Function *alias =
	  llvm::dyn_cast<const llvm::Function>(c)) {
	if (visited.find(alias) == visited.end())
	  insertChain(alias, global, visited);
      } else {
	// did some checking, this case might not matter
      }
    } else {
      llvm::errs() << "Unexpected kind of user of function\n"; // 1 violation
    }
  }
}

void getUsingInstruction(const llvm::User *user,
			 std::set<const llvm::User *> &visited,
			 std::vector<const llvm::Instruction *> &is) {
  if (const llvm::Instruction *i =
      llvm::dyn_cast<const llvm::Instruction>(user)) {
    is.push_back(i);
  } else if (const llvm::Constant *c =
	     llvm::dyn_cast<const llvm::Constant>(user)) {
    if (visited.find(user) == visited.end())
      visited.insert(user);
    else // how does this happen
      return;
    for (llvm::Constant::const_use_iterator ccui = c->use_begin(),
	   ccuj = c->use_end(); ccui != ccuj; ++ccui) {
      const llvm::User *usersUser = *ccui;
      getUsingInstruction(usersUser, visited, is);
    }
  } else {
    llvm::errs() << "Unexpected kind of user\n";
  }
}
void StaticAnalyser::constructFunctionGlobalChain
(const llvm::GlobalVariable *global) {
  if (global->isConstant())
    return;
  for (llvm::GlobalVariable::const_use_iterator ui = global->use_begin(),
	 uj = global->use_end(); ui != uj; ++ui) {
    const llvm::User *user = *ui;
    std::set<const llvm::User *> visitedUsers;
    std::vector<const llvm::Instruction *> is;
    getUsingInstruction(user, visitedUsers, is);
    for (std::vector<const llvm::Instruction *>::iterator vi = is.begin(),
	   vj = is.end(); vi != vj; ++vi) {
      const llvm::Function *f = (*vi)->getParent()->getParent();
      std::set<const llvm::Function *> visited;
      insertChain(f, global, visited);
    }
  }
}
