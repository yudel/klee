#ifndef KLEE_CALLGRAPH_H
#define KLEE_CALLGRAPH_H

#include"klee/Config/Version.h"

#include"llvm/Module.h"
#if LLVM_VERSION_CODE < LLVM_VERSION(3, 5)
#include"llvm/Support/CallSite.h"
#else
#include"llvm/IR/CallSite.h"
#endif
#include"llvm/Support/raw_ostream.h"

#include<map>
#include<vector>

namespace klee {

// mostly copied from llvm src, crashed when used their implementation
// (AssertVH)
class CallGraphNode {
  friend class CallGraph;
  
public:
  typedef std::pair<llvm::CallSite, CallGraphNode *> CallRecord;
  typedef std::vector<CallRecord> CalledFunctionsVector;

  CallGraphNode(llvm::Function *f) : F(f) {}

  typedef std::vector<CallRecord>::iterator iterator;
  typedef std::vector<CallRecord>::const_iterator const_iterator;

  llvm::Function *getFunction() const { return F; }

  inline iterator begin() { return CalledFunctions.begin(); }
  inline iterator end()   { return CalledFunctions.end();   }
  inline const_iterator begin() const { return CalledFunctions.begin(); }
  inline const_iterator end()   const { return CalledFunctions.end();   }
  inline bool empty() const { return CalledFunctions.empty(); }
  inline unsigned size() const { return (unsigned)CalledFunctions.size(); }

  CallGraphNode *operator[](unsigned i) const {
    assert(i < CalledFunctions.size() && "Invalid index");
    return CalledFunctions[i].second;
  }
  
private:
  llvm::Function *F;
  std::vector<CallRecord> CalledFunctions;

  void addCalledFunction(llvm::CallSite CS, CallGraphNode *M) {
    CalledFunctions.push_back(std::make_pair(CS, M));
  }
};

class CallGraph {
private:
  typedef std::map<const llvm::Function *, CallGraphNode *> FunctionMapTy;

public:
  llvm::Module *Mod;

  typedef FunctionMapTy::iterator iterator;
  typedef FunctionMapTy::const_iterator const_iterator;

  inline       iterator begin()       { return FunctionMap.begin(); }
  inline       iterator end()         { return FunctionMap.end();   }
  inline const_iterator begin() const { return FunctionMap.begin(); }
  inline const_iterator end()   const { return FunctionMap.end();   }

  inline const CallGraphNode *operator[](const llvm::Function *F) const {
    const_iterator I = FunctionMap.find(F);
    assert(I != FunctionMap.end() && "Function not in callgraph!");
    return I->second;
  }
  inline CallGraphNode *operator[](const llvm::Function *F) {
    const_iterator I = FunctionMap.find(F);
    assert(I != FunctionMap.end() && "Function not in callgraph!");
    return I->second;
  }

  // Root is root of the call graph, or the external node if a 'main' function
  // couldn't be found.
  CallGraphNode *Root;
  // ExternalCallingNode - This node has edges to all external functions and
  // those internal functions that have their address taken.
  CallGraphNode *ExternalCallingNode;
  // CallsExternalNode - This node has edges to it from all functions making
  // indirect calls or calling an external function.
  CallGraphNode *CallsExternalNode;

  CallGraph(llvm::Module *module)
    : Mod(module), Root(0), ExternalCallingNode(0), CallsExternalNode(0) {}
  ~CallGraph();

  void initialise();
  
public:
  FunctionMapTy FunctionMap;

  void addToCallGraph(llvm::Function *F);
  CallGraphNode *getOrInsertFunction(const llvm::Function *F);
};

}

#endif
