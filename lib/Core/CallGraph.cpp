#include "CallGraph.h"

#include "llvm/IntrinsicInst.h"

using namespace klee;
using namespace llvm;

CallGraph::~CallGraph() {
  if (CallsExternalNode) {
    delete CallsExternalNode;
    CallsExternalNode = 0;
  }
  
  if (FunctionMap.empty()) return;
  
  for (FunctionMapTy::iterator I = FunctionMap.begin(), E = FunctionMap.end();
       I != E; ++I)
    delete I->second;
  FunctionMap.clear();
}

void CallGraph::initialise() {
  Module &M = *Mod;
  
  ExternalCallingNode = this->getOrInsertFunction(0);
  CallsExternalNode = new CallGraphNode(0);
  Root = 0;
  
  // Add every function to the call graph.
  for (Module::iterator I = M.begin(), E = M.end(); I != E; ++I)
    addToCallGraph(I);
  
  // If we didn't find a main function, use the external call graph node
  if (Root == 0) Root = ExternalCallingNode;
}

void CallGraph::addToCallGraph(Function *F) {
  CallGraphNode *Node = getOrInsertFunction(F);

  // If this function has external linkage, anything could call it.
  if (!F->hasLocalLinkage()) {
    ExternalCallingNode->addCalledFunction(CallSite(), Node);

    // Found the entry point?
    if (F->getName() == "main") {
      if (Root) // Found multiple external mains? Don't pick one.
	Root = ExternalCallingNode;
      else
	Root = Node; // Found a main, keep track of it!
    }
  }

  // Loop over all of the users of the function, looking for non-call uses.
  for (Value::use_iterator I = F->use_begin(), E = F->use_end();
       I != E; ++I) {
    User *U = *I;
    if ((!isa<CallInst>(U) && !isa<InvokeInst>(U))
	|| !CallSite(cast<Instruction>(U)).isCallee(I)) {
      // Not a call, or being used as a parameter rather than as the callee.
      ExternalCallingNode->addCalledFunction(CallSite(), Node);
      break;
    }
  }

  // If this function is not defined in this translation unit, it could call
  // anything.
  if (F->isDeclaration() && !F->isIntrinsic())
    Node->addCalledFunction(CallSite(), CallsExternalNode);

  // Look for calls by this function.
  for (Function::iterator BB = F->begin(), BBE = F->end();
       BB != BBE; ++BB)
    for (BasicBlock::iterator II = BB->begin(), IE = BB->end();
	 II != IE; ++II) {
      CallSite CS(cast<Value>(II));
      if (CS && !isa<DbgInfoIntrinsic>(II)) {
	const Function *Callee = CS.getCalledFunction();
	if (Callee)
	  Node->addCalledFunction(CS, getOrInsertFunction(Callee));
	else
	  Node->addCalledFunction(CS, CallsExternalNode);
      }
    }
}

CallGraphNode *CallGraph::getOrInsertFunction(const Function *F) {
  CallGraphNode *&CGN = FunctionMap[F];
  if (CGN) return CGN;
  
  assert((!F || F->getParent() == Mod) && "Function not in current module!");
  return CGN = new CallGraphNode(const_cast<Function *>(F));
}
