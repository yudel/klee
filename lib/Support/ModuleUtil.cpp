// migrated from lib module (conflict!) for cse project only
// used under llvm-3.8, won't compile otherwise

#include "klee/Config/Version.h"
#if LLVM_VERSION_CODE >= LLVM_VERSION(3, 8)
// FIXME: This does not belong here.
#include "../Core/Common.h"
#include "../Core/SpecialFunctionHandler.h"

#include "klee/Internal/Support/Debug.h"
#include "klee/Internal/Support/ModuleUtil.h"

// only works for llvm-3.8
#include "llvm/Analysis/ValueTracking.h"
#include "llvm/Bitcode/ReaderWriter.h"
#include "llvm/IR/AssemblyAnnotationWriter.h"
#include "llvm/IR/CallSite.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/IntrinsicInst.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/ValueSymbolTable.h"
#include "llvm/IR/Module.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/Linker/Linker.h"
#include "llvm/Object/Archive.h"
#include "llvm/Object/Binary.h"
#include "llvm/Object/Error.h"
#include "llvm/Object/ObjectFile.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/DataStream.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/Path.h"

#include <map>
#include <set>
#include <fstream>
#include <sstream>
#include <string>

using namespace klee;
using namespace llvm;

/// Based on GetAllUndefinedSymbols() from LLVM3.2
///
/// GetAllUndefinedSymbols - calculates the set of undefined symbols
/// that still exist in an LLVM module. This is a bit tricky because
/// there may be two symbols with the same name but different LLVM
/// types that will be resolved to each other but aren't currently
/// (thus we need to treat it as resolved).
///
/// Inputs:
///  M - The module in which to find undefined symbols.
///
/// Outputs:
///  UndefinedSymbols - A set of C++ strings containing the name of all
///                     undefined symbols.
///
static void
GetAllUndefinedSymbols(Module *M, std::set<std::string> &UndefinedSymbols) {
  static const std::string llvmIntrinsicPrefix="llvm.";
  std::set<std::string> DefinedSymbols;
  UndefinedSymbols.clear();
  KLEE_DEBUG_WITH_TYPE("klee_linker",
                       dbgs() << "*** Computing undefined symbols ***\n");

  for (Module::iterator I = M->begin(), E = M->end(); I != E; ++I)
    if (I->hasName()) {
      if (I->isDeclaration())
        UndefinedSymbols.insert(I->getName());
      else if (!I->hasLocalLinkage()) {
#if LLVM_VERSION_CODE < LLVM_VERSION(3, 5)
            assert(!I->hasDLLImportLinkage() && "Found dllimported non-external symbol!");
#else
            assert(!I->hasDLLImportStorageClass() && "Found dllimported non-external symbol!");
#endif
        DefinedSymbols.insert(I->getName());
      }
    }

  for (Module::global_iterator I = M->global_begin(), E = M->global_end();
       I != E; ++I)
    if (I->hasName()) {
      if (I->isDeclaration())
        UndefinedSymbols.insert(I->getName());
      else if (!I->hasLocalLinkage()) {
#if LLVM_VERSION_CODE < LLVM_VERSION(3, 5)
            assert(!I->hasDLLImportLinkage() && "Found dllimported non-external symbol!");
#else
            assert(!I->hasDLLImportStorageClass() && "Found dllimported non-external symbol!");
#endif
        DefinedSymbols.insert(I->getName());
      }
    }

  for (Module::alias_iterator I = M->alias_begin(), E = M->alias_end();
       I != E; ++I)
    if (I->hasName())
      DefinedSymbols.insert(I->getName());

  // Prune out any defined symbols from the undefined symbols set
  // and other symbols we don't want to treat as an undefined symbol
  std::vector<std::string> SymbolsToRemove;
  for (std::set<std::string>::iterator I = UndefinedSymbols.begin();
       I != UndefinedSymbols.end(); ++I )
  {
    if (DefinedSymbols.count(*I))
    {
      SymbolsToRemove.push_back(*I);
      continue;
    }

    // Strip out llvm intrinsics
    if ( (I->size() >= llvmIntrinsicPrefix.size() ) &&
       (I->compare(0, llvmIntrinsicPrefix.size(), llvmIntrinsicPrefix) == 0) )
    {
      KLEE_DEBUG_WITH_TYPE("klee_linker", dbgs() << "LLVM intrinsic " << *I <<
                      " has will be removed from undefined symbols"<< "\n");
      SymbolsToRemove.push_back(*I);
      continue;
    }

    // Symbol really is undefined
    KLEE_DEBUG_WITH_TYPE("klee_linker",
                         dbgs() << "Symbol " << *I << " is undefined.\n");
  }

  // Remove KLEE intrinsics from set of undefined symbols
  // not removing

  // Now remove the symbols from undefined set.
  for (size_t i = 0, j = SymbolsToRemove.size(); i < j; ++i )
    UndefinedSymbols.erase(SymbolsToRemove[i]);

  KLEE_DEBUG_WITH_TYPE("klee_linker",
                       dbgs() << "*** Finished computing undefined symbols ***\n");
}

/*! A helper function for klee::linkWithLibrary() that
 *  links in an archive of bitcode modules into a composite bitcode module
 *
 *  \param[in] archive Archive of bitcode modules
 *  \param[in,out] composite The bitcode module to link against the archive
 *  \param[out] errorMessage Set to an error message if linking fails
 *
 *  \return True if linking succeeds otherwise false
 */
static bool linkBCA(object::Archive* archive, Module* composite,
		    std::string& errorMessage)
{
  llvm::raw_string_ostream SS(errorMessage);
  std::vector<std::unique_ptr<Module>> archiveModules;

  // Is this efficient? Could we use StringRef instead?
  std::set<std::string> undefinedSymbols;
  GetAllUndefinedSymbols(composite, undefinedSymbols);

  if (undefinedSymbols.size() == 0)
  {
    // Nothing to do
    KLEE_DEBUG_WITH_TYPE("klee_linker", dbgs()
			 << "No undefined symbols. Not linking anything in!\n");
    return true;
  }

  KLEE_DEBUG_WITH_TYPE("klee_linker", dbgs() << "Loading modules\n");
  // Load all bitcode files in to memory so we can examine their symbols
  for (object::Archive::child_iterator AI = archive->child_begin(),
       AE = archive->child_end(); AI != AE; ++AI)
  {

    if ( *AI )
    {
      ErrorOr<StringRef> memberName = AI->get().getName();
      if ( memberName ) {
	KLEE_DEBUG_WITH_TYPE("klee_linker", dbgs() << "Loading archive member "
			     << memberName.get() << "\n");
      } else {
	return false;
      }
    }
    else
    {
      errorMessage="Archive member does not have a name!\n";
      return false;
    }

    object::Archive::Child child = AI->get();
    ErrorOr<std::unique_ptr<object::Binary>> binary = child.getAsBinary();
    if (!binary)
    {
      // If we can't open as a binary object file its hopefully a bitcode file
      // Once this is destroyed will Module still be valid??

      ErrorOr<MemoryBufferRef> buff = child.getMemoryBufferRef();

      if (!buff)
      {
        SS << "Failed to get MemoryBuffer: " << buff.getError().message();
        SS.flush();
        return false;
      }

      if (buff)
      {
        // FIXME: Maybe load bitcode file lazily?
	// Then if we need to link, materialise the module
	ErrorOr<std::unique_ptr<Module>> Result =
	  parseBitcodeFile(buff.get(), getGlobalContext());

        if(!Result)
        {
          SS << "Loading module failed : "
	     << Result.getError().message() << "\n";
          SS.flush();
          return false;
        }
        archiveModules.push_back(std::unique_ptr<Module>
				 (std::move(Result.get())));
      }
      else
      {
        errorMessage="Buffer was NULL!";
        return false;
      }

    }
    else if (object::ObjectFile *o =
	     dyn_cast<object::ObjectFile>(binary.get().get()))
    {
      SS << "Object file " << o->getFileName().data() <<
            " in archive is not supported";
      SS.flush();
      return false;
    }
    else
    {
      SS << "Loading archive child with error "<< binary.getError().message();
      SS.flush();
      return false;
    }

  }

  KLEE_DEBUG_WITH_TYPE("klee_linker", dbgs()
		       << "Loaded " << archiveModules.size() << " modules\n");

  std::set<std::string> previouslyUndefinedSymbols;

  // Walk through the modules looking for definitions of undefined symbols
  // if we find a match we should link that module in.
  unsigned int passCounter=0;
  do
  {

    unsigned int modulesLoadedOnPass=0;
    previouslyUndefinedSymbols = undefinedSymbols;

    for (size_t i = 0, j = archiveModules.size(); i < j; ++i)
    {
      // skip empty archives
      if (!archiveModules[i])
        continue;
      // cause the module to be deleted automatically
      std::unique_ptr<Module> M(std::move(archiveModules[i]));
      bool linked = false;
      // Look for the undefined symbols in the composite module
      for (std::set<std::string>::iterator S = undefinedSymbols.begin(),
	     SE = undefinedSymbols.end(); S != SE; ++S)
      {

        // FIXME: We aren't handling weak symbols here!
        // However the algorithm used in LLVM3.2 didn't seem to either
        // so maybe it doesn't matter?

        if ( GlobalValue* GV = dyn_cast_or_null<GlobalValue>
	     (M->getValueSymbolTable().lookup(*S)) )
        {
          if (GV->isDeclaration()) continue; // Not a definition

          KLEE_DEBUG_WITH_TYPE("klee_linker", dbgs() << "Found "
			       << GV->getName() << " in "
			       << M->getModuleIdentifier() << "\n");

	  // documentation so not trustworthy
          if (Linker::linkModules(*composite, *M))
          {
            // Linking failed
            SS << "Linking archive module with composite failed: ???\n";
            SS.flush();
            return false;
          }
          else
          {
            // Link succeed, now clean up
            modulesLoadedOnPass++;
            KLEE_DEBUG_WITH_TYPE("klee_linker", dbgs()
				 << "Linking succeeded.\n");

            // automatically delete M;

            // We need to recompute the undefined symbols in the
	    // composite module after linking
            GetAllUndefinedSymbols(composite, undefinedSymbols);

	    linked = true;
            break; // Look for symbols in next module
          }
	  
        }
      }

      // if not this module is not linked, get it back to the set,
      // otherwise it will be deleted
      // kind of stupid, but I don't know another way to get around
      // the unique ptr
      if (!linked)
	archiveModules[i] = std::move(M);
    }

    passCounter++;
    KLEE_DEBUG_WITH_TYPE("klee_linker", dbgs()
			 << "Completed " << passCounter << " linker passes.\n"
			 << modulesLoadedOnPass
			 << " modules loaded on the last pass\n");
  } while (undefinedSymbols != previouslyUndefinedSymbols);
  // Iterate until we reach a fixed point
  // omg so nested

  // What's left in archiveModules we don't want to link in so free it
  // (automatically)

  return true;

}

Module *klee::linkWithLibrary(Module *module,
                              const std::string &libraryName) {
  KLEE_DEBUG_WITH_TYPE("klee_linker", dbgs() << "Linking file "
		       << libraryName << "\n");
  if (!sys::fs::exists(libraryName)) {
    exit(1);
  }

  ErrorOr<std::unique_ptr<MemoryBuffer>> Buffer =
    MemoryBuffer::getFile(libraryName);
  if (!Buffer) {
    exit(1);
  }

  sys::fs::file_magic magic =
    sys::fs::identify_magic(Buffer.get()->getBuffer());

  LLVMContext &Context = getGlobalContext();

  if (magic == sys::fs::file_magic::bitcode) {
    ErrorOr<std::unique_ptr<Module>> Result =
      parseBitcodeFile(Buffer.get()->getMemBufferRef(), Context);

    if (!Result || Linker::linkModules(*module, *(Result.get().get())))
      exit(1);
    
  } else if (magic == sys::fs::file_magic::archive) {
    ErrorOr<std::unique_ptr<object::Binary>> binary =
      object::createBinary(Buffer.get()->getMemBufferRef());
    if (!binary)
      exit(1);
    
    if (object::Archive *a = dyn_cast<object::Archive>(binary.get().get())) {
      // Handle in helper
      std::string ErrorMessage;
      if (!linkBCA(a, module, ErrorMessage))
	exit(1);
      
    }
    else {
      exit(1);
    }

  } else if (magic.is_object()) {
    // unsupported
  } else {
    exit(1);
  }

  return module;
}

Function *klee::getDirectCallTarget(CallSite cs) {
  Value *v = cs.getCalledValue();
  if (Function *f = dyn_cast<Function>(v)) {
    return f;
  } else if (llvm::ConstantExpr *ce = dyn_cast<llvm::ConstantExpr>(v)) {
    if (ce->getOpcode()==Instruction::BitCast)
      if (Function *f = dyn_cast<Function>(ce->getOperand(0)->stripPointerCasts()))
        return f;

    // NOTE: This assert may fire, it isn't necessarily a problem and
    // can be disabled, I just wanted to know when and if it happened.
    assert(0 && "FIXME: Unresolved direct target for a constant expression.");
  }
  
  return 0;
}

static bool valueIsOnlyCalled(const Value *v) {
  for (Value::const_use_iterator it = v->use_begin(), ie = v->use_end();
       it != ie; ++it) {
    if (const Instruction *instr = dyn_cast<Instruction>(*it)) {
      if (instr->getOpcode()==0) continue; // XXX function numbering inst
      if (!isa<CallInst>(instr) && !isa<InvokeInst>(instr)) return false;
      
      // Make sure that the value is only the target of this call and
      // not an argument.
      for (unsigned i=1,e=instr->getNumOperands(); i!=e; ++i)
        if (instr->getOperand(i)==v)
          return false;
    } else if (const llvm::ConstantExpr *ce =
               dyn_cast<llvm::ConstantExpr>(*it)) {
      if (ce->getOpcode()==Instruction::BitCast)
        if (valueIsOnlyCalled(ce))
          continue;
      return false;
    } else if (const GlobalAlias *ga = dyn_cast<GlobalAlias>(*it)) {
      // XXX what about v is bitcast of aliasee?
      if (v==ga->getAliasee() && !valueIsOnlyCalled(ga))
        return false;
    } else {
      return false;
    }
  }

  return true;
}

bool klee::functionEscapes(const Function *f) {
  return !valueIsOnlyCalled(f);
}
#endif
