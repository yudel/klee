#include"cse/Sumbase.h"

namespace cse {

  void Sumbase::registerInstrSummary(PCINSTR instr,
				     const Summary *summary) {
    MI i = instrSums.find(instr);
    if (i == instrSums.end()) {
      std::pair<MI, bool> ret = instrSums
	.insert(std::pair<PCINSTR, Summaries>
		(instr, Summaries(instr)));
      ret.first->second.push_back(summary);
    } else {
      i->second.push_back(summary);
    }
  }

  void Sumbase::registerSummary(PCINSTR head, PCINSTR tail,
				const Summary *summary) {
    MI i = sums.find(head);
    if (i == sums.end()) {
      std::pair<MI, bool> ret = sums
	.insert(std::pair<PCINSTR, Summaries>
		(head, Summaries(head, tail, false)));
      ret.first->second.push_back(summary);
    } else {
      i->second.push_back(summary);
    }
  }

  void Sumbase::registerIncomplete(PCINSTR instr) {
    incompletes.insert(instr);
  }

  const Summary *Sumbase::getInstrSum(PCINSTR instr) {
    Summaries *ss = getInstrSums(instr);
    if (ss) {
      if (ss->numOfSums == 1)
	return ss->vs.front();
      else
	return NULL;
    } else
      return NULL;
  }
  const Summary *Sumbase::getInstrSum(PCINSTR instr, const llvm::Value *corr) {
    Summaries *ss = getInstrSums(instr);
    if (ss) {
      for (Summaries::VCI vci = ss->begin(), vcj = ss->end();
	   vci != vcj; vci++) {
	if (!corr || (*vci)->getCorr() == corr)
	  return (*vci);
      }
      return NULL;
    } else
      return NULL;
  }
  
  Summaries *Sumbase::getInstrSums(PCINSTR instr) {
    MI i = instrSums.find(instr);
    if (i == instrSums.end()) {
      return NULL;
    } else {
      return &(i->second);
    }
  }

  Summaries *Sumbase::getSums(PCINSTR head) {
    MI i = sums.find(head);
    if (i == sums.end()) {
      return NULL;
    } else {
      return &(i->second);
    }
  }

  bool Sumbase::isIncomplete(PCINSTR instr) {
    if (incompletes.find(instr) == incompletes.end())
      return false;
    else
      return true;
  }

  bool Sumbase::isKnown(PCINSTR head) {
    if (!head)
      return false;
    MI i = sums.find(head);
    if (i == sums.end()) {
      return false;
    } else {
      return true;
    }
  }
  
  void Sumbase::addToKnownFuncs(const llvm::Function *func) {
    knownFuncs.insert(func);
  }
  bool Sumbase::isKnown(const llvm::Function *func) {
    return knownFuncs.find(func) != knownFuncs.end();
  }

  void Sumbase::registerToFuncModel(const llvm::Function *func,
				    const Summary *summary) {
    FMI i = funcModels.find(func);
    if (i == funcModels.end()) {
      std::pair<FMI, bool> ret = funcModels
	.insert(std::pair<const llvm::Function *, Summaries>
		// the heads and tails are not likely to match, which is fine
		(func, Summaries(summary->getHead(),
				 summary->getTail(), false)));
      ret.first->second.push_back(summary);
    } else {
      i->second.push_back(summary);
    }
  }
  const Summary *Sumbase::getFromFunctionModel(const llvm::Function *func) {
    FMI i = funcModels.find(func);
    if (i == funcModels.end()) {
      return NULL;
    } else {
      // currently only use one summary to approximate
      return i->second.vs.front();
    }
  }

  void Sumbase::prtSumsCount() {
    llvm::outs() << "Num of instrs have summaries: " << sums.size() << "\n";
    for (MI mi = sums.begin(), mj = sums.end(); mi != mj; mi++) {
      llvm::outs() << mi->second.size() << " summaries from:\n";
      mi->first->dump();
      llvm::outs() << "\n";
    }
  }

  void Sumbase::dump() {
    /*
    for (MI mi = instrSums.begin(), mj = instrSums.end(); mi != mj; mi++) {
      mi->first->dump();
      for (Summaries::VCI vci = mi->second.begin(), vcj = mi->second.end();
	   vci != vcj; vci++) {
	(*vci)->dump();
      }
    }
    */
    for (MI mi = sums.begin(), mj = sums.end(); mi != mj; mi++) {
      llvm::outs() << "\nSummaries from:\n";
      mi->first->dump();
      for (Summaries::VCI vci = mi->second.begin(), vcj = mi->second.end();
	   vci != vcj; vci++) {
	(*vci)->dump();
      }
    }
  }

  std::map<PCINSTR, Summaries> Sumbase::instrSums;
  std::map<PCINSTR, Summaries> Sumbase::sums;
  std::set<PCINSTR> Sumbase::incompletes;
  std::set<const llvm::Function *> Sumbase::knownFuncs;

  std::map<const llvm::Function *, Summaries> Sumbase::funcModels;

}
