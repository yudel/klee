#include"cse/Summaxecutor.h"
#include"cse/StackFrame.h"
#include"cse/Sumbase.h"
#include"cse/Util.h"

#include"llvm/IR/Instructions.h"
#include"llvm/IR/Metadata.h"
#include"llvm/IR/Operator.h"
#include"llvm/IR/Value.h"
#include"llvm/Support/Casting.h"

namespace cse {

  void handlingBinaryOperation(const llvm::Instruction &instr,
			       Expr::Kind kind) {
    assert(instr.getNumOperands() == 2 && "Illegal binary operator");
    Summary *s = new Summary(&instr);
    // a pointer to an instruction is also a pointer to the result value
    EquationExpr *eqExpr =
      new EquationExpr(kind, &instr, instr.getOperand(0), instr.getOperand(1));
    s->addConstraint(eqExpr, false);
    Sumbase::registerInstrSummary(&instr, s);
  }

  void handlingCasting(const llvm::Instruction &instr,
		       Expr::Kind kind) {
    if (const llvm::CastInst *ci = llvm::dyn_cast<llvm::CastInst>(&instr)) {
      const llvm::Type *type = ci->getDestTy();
      const llvm::Value *orig = ci->getOperand(0);
      assert(instr.getNumOperands() == 1 && "Illegal casting");
      CastingExpr *ce =
	new CastingExpr(kind, ci, type, orig);
      Summary *s = new Summary(&instr);
      s->addConstraint(ce, false);
      Sumbase::registerInstrSummary(&instr, s);
    }
  }

  void handlingComparison(const llvm::Instruction &instr) {
    assert(instr.getNumOperands() == 2 && "Illegal comparison");
    const llvm::CmpInst *cmpi = static_cast<const llvm::CmpInst *>(&instr);
    Summary *s = new Summary(&instr);
    ComparisonExpr *cmpExpr =
      new ComparisonExpr(cmpi, cmpi->getOperand(0), cmpi->getOperand(1));
    s->addConstraint(cmpExpr, false);
    Sumbase::registerInstrSummary(cmpi, s);
  }

  // lots of values not const expr summarised, to be done
  void Summaxecutor::summariseInstr(const llvm::Instruction &instr) {
    switch (instr.getOpcode()) {
      /* Terminator instructions */
    case llvm::Instruction::Ret: {
      // the constraint on ret is only known dynamically,
      // so we don't have to do anything here
      break;
    }
#if LLVM_VERSION_CODE < LLVM_VERSION(3, 1)
    case llvm::Instruction::Unwind: {
      report("unhandled unwind", 0x0201, true);
      break;
    }
#endif
    case llvm::Instruction::Br: {
      const llvm::BranchInst *bi =
	static_cast<const llvm::BranchInst *>(&instr);
      if (bi->isUnconditional()) {
      } else {
	// this value is not the instr value though
	llvm::Value *value = bi->getCondition();
	// true branch
	// I'm not sure about the successor id
	Summary *sTrue = new Summary(0, &instr, &instr,
				     &(bi->getSuccessor(0)->front()));
	UnaryExpr *ueTrue = new UnaryExpr(Expr::IS_TRUE, value, &instr);
	sTrue->addConstraintBoth(ueTrue, false);
	Sumbase::registerInstrSummary(&instr, sTrue);
	// false branch
	Summary *sFalse = new Summary(0, &instr, &instr,
				      &(bi->getSuccessor(1)->front()));
	UnaryExpr *ueFalse = new UnaryExpr(Expr::IS_FALSE, value, &instr);
	sFalse->addConstraintBoth(ueFalse, false);
	Sumbase::registerInstrSummary(&instr, sFalse);
      }
      break;
    } // end of br
    case llvm::Instruction::Switch: {
      const llvm::SwitchInst *si =
	static_cast<const llvm::SwitchInst *>(&instr);
      // this value is not the instr value though
      llvm::Value *value = si->getCondition();
      unsigned int n = si->getNumCases(); // not including default
      if (n == 0) // no constraint, directly jump to the default successor
	break;
      Summary *sDefault = new Summary(0, &instr, &instr,
				      &(si->getDefaultDest()->front()));
      InequalityExpr *ie = new InequalityExpr(value, n, &instr); // default case
      unsigned int i = 0;
      for (llvm::SwitchInst::ConstCaseIt cci = si->case_begin(),
	     ccj = si->case_end(); cci != ccj; ++cci) {
	const llvm::Value *caseV = cci.getCaseValue(); // it's a const int
	Summary *sCase = new Summary(0, &instr, &instr,
				     &(cci.getCaseSuccessor()->front()));
	EqualityExpr *ee = new EqualityExpr(value, false, caseV, &instr);
	sCase->addConstraintBoth(ee, false);
	Sumbase::registerInstrSummary(&instr, sCase);
	ie->setOp(i++, caseV);
      }
      sDefault->addConstraintBoth(ie, false);
      Sumbase::registerInstrSummary(&instr, sDefault);
      break;
    }
      // missing indirectbr
    case llvm::Instruction::Invoke:
      report("unhandled invoke", 0x0203, true);
      break;
      // missing resume etc.
    case llvm::Instruction::Unreachable:
      // a terminator with no successor; I think we've handled it,
      // but not made use of its "unreachable" nature, but again
      // a test will be generated if we can cover it and trigger
      // an error from LLVM
      break;

      /* Binary operations */
    case llvm::Instruction::Add:
      handlingBinaryOperation(instr, Expr::ADD);
      break;
    case llvm::Instruction::FAdd:
      handlingBinaryOperation(instr, Expr::FADD);
      break;
    case llvm::Instruction::Sub:
      handlingBinaryOperation(instr, Expr::SUB);
      break;
    case llvm::Instruction::FSub:
      handlingBinaryOperation(instr, Expr::FSUB);
      break;
    case llvm::Instruction::Mul:
      handlingBinaryOperation(instr, Expr::MUL);
      break;
    case llvm::Instruction::FMul:
      handlingBinaryOperation(instr, Expr::FMUL);
      break;
    case llvm::Instruction::UDiv:
      handlingBinaryOperation(instr, Expr::UDIV);
      break;
    case llvm::Instruction::SDiv:
      handlingBinaryOperation(instr, Expr::SDIV);
      break;
    case llvm::Instruction::FDiv:
      handlingBinaryOperation(instr, Expr::FDIV);
      break;
    case llvm::Instruction::URem:
      handlingBinaryOperation(instr, Expr::UREM);
      break;
    case llvm::Instruction::SRem:
      handlingBinaryOperation(instr, Expr::SREM);
      break;
    case llvm::Instruction::FRem:
      handlingBinaryOperation(instr, Expr::FREM);
      break;
      
      /* Bitwise binary operations */
    case llvm::Instruction::Shl:
      handlingBinaryOperation(instr, Expr::SHL);
      break;
    case llvm::Instruction::LShr:
      handlingBinaryOperation(instr, Expr::LSHR);
      break;
    case llvm::Instruction::AShr:
      handlingBinaryOperation(instr, Expr::ASHR);
      break;
    case llvm::Instruction::And:
      handlingBinaryOperation(instr, Expr::AND);
      break;
    case llvm::Instruction::Or:
      handlingBinaryOperation(instr, Expr::OR);
      break;
    case llvm::Instruction::Xor:
      handlingBinaryOperation(instr, Expr::XOR);
      break;

      // missing vector operations

      /* Aggregate operations */
    case llvm::Instruction::ExtractValue: {
    }
    case llvm::Instruction::InsertValue: {
      report("unhandled aggregated", 0x0204, true);
      break;
    }

      /* Memory access and addressing operations */
    case llvm::Instruction::Alloca: {
      const llvm::AllocaInst *ai = llvm::cast<const llvm::AllocaInst>(&instr);
      llvm::Type *allocatedTy = ai->getAllocatedType();
      unsigned int size;
      if (MEMORY_FLATTEN) {
	size = getFlattenSize(*allocatedTy);
      } else {
	// allocate memory on stack; see more in llvm reference
	size = context->dl->GET_TYPE_SE_SIZE(allocatedTy);
      }
      Summary *s = new Summary(&instr);
      Allocation *alloca;
      if (ai->isArrayAllocation()) {
	const llvm::Value *arraySize = ai->getArraySize();
        alloca = new Allocation(ai, size, arraySize);
      } else {
	alloca = new Allocation(ai, size);
      }
      s->addConstraint(alloca, false);
      Sumbase::registerInstrSummary(&instr, s);
      break;
    }
    case llvm::Instruction::Load: {
      Summary *s = new Summary(&instr);
      const llvm::LoadInst *li = llvm::cast<const llvm::LoadInst>(&instr);
      const llvm::Value *valueP = li->getPointerOperand();
      summariseConstExpr(s, valueP);
      AccessingOperation *ao =
	new AccessingOperation(Expr::LOAD, li, valueP);
      s->addConstraint(ao, false);
      Sumbase::registerInstrSummary(&instr, s);
      break;
    }
    case llvm::Instruction::Store: {
      Summary *s = new Summary(&instr);
      const llvm::StoreInst *si = llvm::cast<const llvm::StoreInst>(&instr);
      const llvm::Value *value = si->getValueOperand();
      const llvm::Value *valueP = si->getPointerOperand();
      summariseConstExpr(s, valueP);
      AccessingOperation *ao =
	new AccessingOperation(Expr::STORE, value, valueP);
      s->addConstraint(ao, false);
      Sumbase::registerInstrSummary(&instr, s);
      break;
    }
      // missing fence etc.
    case llvm::Instruction::GetElementPtr: {
      Summary *s = new Summary(&instr);
      const llvm::Operator *op =
	llvm::cast<const llvm::Operator>(&instr);
      summariseOperator(s, op);
      Sumbase::registerInstrSummary(&instr, s);
      break;
    }

      /* Conversion operations */
    case llvm::Instruction::Trunc:
      handlingCasting(instr, Expr::TRUNC);
      break;
    case llvm::Instruction::ZExt:
      handlingCasting(instr, Expr::ZEXT);
      break;
    case llvm::Instruction::SExt:
      handlingCasting(instr, Expr::SEXT);
      break;
    case llvm::Instruction::FPTrunc:
      handlingCasting(instr, Expr::FPTRUNC);
      break;
    case llvm::Instruction::FPExt:
      handlingCasting(instr, Expr::FPEXT);
      break;
    case llvm::Instruction::FPToUI:
      handlingCasting(instr, Expr::FP_UI);
      break;
    case llvm::Instruction::FPToSI:
      handlingCasting(instr, Expr::FP_SI);
      break;
    case llvm::Instruction::UIToFP:
      handlingCasting(instr, Expr::UI_FP);
      break;
    case llvm::Instruction::SIToFP:
      handlingCasting(instr, Expr::SI_FP);
      break;
    case llvm::Instruction::PtrToInt:
      handlingCasting(instr, Expr::PTR_INT);
      break;
    case llvm::Instruction::IntToPtr:
      handlingCasting(instr, Expr::INT_PTR);
      break;
    case llvm::Instruction::BitCast: {
      Summary *s = new Summary(&instr);
      const llvm::Operator *op =
	llvm::cast<const llvm::Operator>(&instr);
      summariseOperator(s, op);
      Sumbase::registerInstrSummary(&instr, s);
      break;
    }
      // missing addrspacecase

      /* Other operations, comparison */
    case llvm::Instruction::ICmp:
    case llvm::Instruction::FCmp:
      handlingComparison(instr);
      break;

      /* Other operations, control */
    case llvm::Instruction::PHI:
      // summarised during block summarisation and execution
      break;
    case llvm::Instruction::Select: {
      const llvm::SelectInst *seli =
	static_cast<const llvm::SelectInst *>(&instr);
      Summary *s = new Summary(&instr);
      const llvm::Value *trueV = seli->getTrueValue();
      summariseConstExpr(s, trueV);
      const llvm::Value *falseV = seli->getFalseValue();
      summariseConstExpr(s, falseV);
      ITEExpr *iteExpr =
	new ITEExpr(seli, seli->getCondition(), trueV, falseV);
      s->addConstraint(iteExpr, false);
      Sumbase::registerInstrSummary(seli, s);
      break;
    }
    case llvm::Instruction::Call: {
      // call constraints are simple eq exprs between parameters and
      // arguments
      const llvm::CallInst *ci = llvm::cast<const llvm::CallInst>(&instr);
      const llvm::Function *callee = getCallee(ci);
      if (!callee) { // declaration still gets through, intended
	report("function pointers detected, unhandled", 0x0200, true);
	break;
      }
      // maybe don't have to sum if a declaration has no model
      llvm::Function::const_arg_iterator ai = callee->arg_begin(),
	aj = callee->arg_end();
      // calling convention ignored
      Summary *s = new Summary(&instr); // or use the corr version
      unsigned int numArgs = ci->getNumArgOperands();
      for (unsigned int i = 0; i < numArgs; i++, ++ai) {
	if (ai == aj) {
	  report("vararg function detected", 0x0100, true);
	  assert(callee->isVarArg() && "Abnormal function");
	  break;
	}
	const llvm::Value *param = &(*ai);
	const llvm::Value *arg = ci->getArgOperand(i);
	summariseConstExpr(s, arg);
	EqualityExpr *ee = new EqualityExpr(param, arg);
	// assuming the order doesn't matter
	s->addConstraint(ee, false);
      }
      s->push(callee, ci);
      Sumbase::registerInstrSummary(&instr, s);
      break;
    }
    case llvm::Instruction::VAArg:
      report("unhandled vaarg", 0x0206, true);
      break;

      // missing *pad

    default:
      llvm::errs() << "Unknown instruction\n";
    }
  }

  /*
    In our understanding, some (all?) kinds of instructions are operators,
    and a constant expr is always an operator.
  */
  void Summaxecutor::summariseConstExpr(Summary *s, const llvm::Value *v) {
    if (llvm::isa<const llvm::ConstantExpr>(v)) {
      if (const llvm::Operator *op = llvm::dyn_cast<const llvm::Operator>(v))
	summariseOperator(s, op);
      else
	llvm::errs() << "Shouldn't const exprs always be operators?\n";
    }
  }

  /*
    This is really annoying as it's completely the same as
    handling instructions and repeating the same code is bad.
    The fact is that operator is just a utility class
    that contains the common functionality between instruction
    and constant expr. So the handling of an instruction that is
    an operator should comes here, just like that of a constant
    expr.
  */
  void Summaxecutor::summariseOperator(Summary *s, const llvm::Operator *op) {
    // get opcode not working
    if (const llvm::GEPOperator *gepop =
	llvm::dyn_cast<const llvm::GEPOperator>(op)) {
      const llvm::Value *ptr = gepop->getPointerOperand();
      summariseConstExpr(s, ptr);
      // same as ptr->getElementType()
      llvm::Type *targetType = llvm::cast<llvm::PointerType>
	(gepop->getPointerOperandType())->getElementType();
      // these are not the only variables, the indices in gepop
      // are also used in building constraints
      // we just can't do it here
      // the indices are not const expr summarised
      AddressingOperation *ao =
	new AddressingOperation(gepop, ptr, targetType);
      // assuming it doesn't interfere with other constraints,
      // we don't have to do the summary bop
      // it shouldn't interfere, because we are either summarising
      // an individual instr, or we are summarising a constant expr
      // that doesn't have side effect (according to doc)
      // actually doesn't matter, because this function also do the
      // update we want; fix the unnecessary summary bop!
      s->addConstraint(ao, false);
    } else if (const llvm::BitCastOperator *bcop =
	       llvm::dyn_cast<const llvm::BitCastOperator>(op)) {
      const llvm::Type *type = bcop->getDestTy();
      const llvm::Value *orig = bcop->getOperand(0);
      assert(op->getNumOperands() == 1 && "Illegal casting");
      CastingExpr *ce =
	new CastingExpr(Expr::BC, bcop, type, orig);
      s->addConstraint(ce, false);
    } else {
      report("unhandled operator", 0x0207, true);
      op->dump();
    }
  }

} // end of namespace cse
