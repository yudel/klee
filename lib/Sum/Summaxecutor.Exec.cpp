#include"cse/InterprocState.h"
#include"cse/Summaxecutor.h"
#include"cse/Util.h"

#include<fstream>

#include"cse/Timer.h"
namespace cse {

  bool Summaxecutor::ready() {
    if (callStack.empty())
      return true;
    else
      return false;
  }

  void Summaxecutor::execute(llvm::Function *f) {
    friExecute(f);
  }

  // it's getting slow now..
  void Summaxecutor::friExecute(llvm::Function *f) {
    /* Get the first instr */
    const llvm::BasicBlock *bb = &(f->getEntryBlock());
    PCINSTR instr = bb->getFirstNonPHI();
    if (instr != &(bb->front()))
      llvm::outs() << "Warning: entry has phi\n";

    /* Initialise state */
    // init solver, default restore point, done
    // single state (multiple copies in stack), single solver in dfs
    InterprocState state(context);
    bool initFlag = true;

    /* Process the state stack */
    interprocWorkStack.push(state);
    while (withinTL && !interprocWorkStack.empty()) {
      // backtrack to a particular restore point
      InterprocState &backtrackState = interprocWorkStack.top();
      if (initFlag) { // only once at the beginning
	// technically should solver backtrack, but we can omit it
	state.copyFrom(backtrackState);
	initFlag = false;
      } else {
	bool res, clear;
	// solver backtrack done when visit
	state = backtrackState.visit(res, clear);
	if (!res) {
	  if (!clear) { // something wrong adding constraints
	    backtrackState.backtrack(); // must had choices
	    // going to raise warning about check to get model
	    terminate(backtrackState, true);
	    // might still continue if has more choice
	  }
	  if (backtrackState.isAllChosen())
	    interprocWorkStack.pop();
	  continue;
	}
	instr = state.getNextInstr();
      }
      if (backtrackState.isAllChosen())
	interprocWorkStack.pop();

      // execute from instr
      friExecuteFrom(state, instr);
    }
  }

  bool consumeRetInstr(InterprocState &state,
		       const llvm::ReturnInst *retInstr) {
    PCINSTR callInstr = state.getSummary()->pop(); // summary cannot be null
    if (!callInstr)
      return false;
    // not const expr summarised
    const Summary *sum = summariseEquality(retInstr, callInstr,
					   retInstr->getReturnValue());
    PCINSTR nextInstr = callInstr->getNextNode();
    if (sum) {
      state.consume(sum, nextInstr);
      delete sum;
    } else {
      state.consumeEmpty(retInstr, nextInstr);
    }
    return true;
  }

  void Summaxecutor::terminate(InterprocState &state, bool partial) {
    // there isn't a last solver check on this path, so in some
    // weird situations, the path can be infeasible!
    llvm::outs() << "Found a " << (partial ? "partial " : "") << "path\n";
    state.statsDump();
    // state.dump();
    // state.solverDump();
    state.gen();
    // pop solver to stack top, done when getting another state
    // deletion automatically done
    unsigned int numOfTests = Counter::get(1);
    if (TEST_LIMIT && numOfTests >= TEST_LIMIT) {
      report("test limit reached", 0x0020, true);
      withinTL = false;
    }
  }

  void Summaxecutor::friExecuteFrom(InterprocState &state,
				    PCINSTR instr) {
    /*
    if (V && state.getVirtualHead() && state.getVirtualTail()) {
      std::ofstream file;
      file.open("track", std::ios::app);
      file << "from " << size_t(state.getVirtualHead()) << " "
	   << state.getVirtualHead()->getOpcodeName() << " to "
	   << size_t(state.getVirtualTail()) << " "
	   << state.getVirtualTail()->getOpcodeName() << "\n";
      if (instr)
	file << "next is " << size_t(instr) << " "
	     << instr->getOpcodeName() << "\n";
      file.close();
    }
    */
    assert(state.isAllChosen() && "Illegal state");

    /* Find the next instr from the state tail */
    // instr must be null when the tail is a call instr
    if (!instr) {
      instr = state.getVirtualTail();
      if (const llvm::TerminatorInst *ti =
	  llvm::dyn_cast<const llvm::TerminatorInst>(instr)) {
	if (ti->getNumSuccessors() > 1) {
	  // conditional branch must have corr, wouldn't end up here
	  llvm::errs() << "Successor(s) unhandled\n";
	  instr = &(ti->getSuccessor(0)->front());
	} else if (ti->getNumSuccessors() > 0) {
	  instr = &(ti->getSuccessor(0)->front());
	}
	// ret instr has 0 succ
	else if (llvm::isa<const llvm::ReturnInst>(ti)) {
	  llvm::errs() << "Return instr shouldn't be here\n";
	  terminate(state); // abnormal termination
	  return;
	} else { // abnormal termination
	  llvm::errs() << "Shouldn't terminate here\n";
	  terminate(state);
	  return;
	}
      } else if (const llvm::CallInst *ci =
		 llvm::dyn_cast<const llvm::CallInst>(instr)) {
	// summary from sumbase always non-empty
	Summary *sum = state.getSummary();
	const llvm::Function *callee = sum->getLastCalledFunction(instr);
	if (callee && !callee->isDeclaration()) {
	  instr = &(callee->getEntryBlock().front());
	} else {
	  if (callee) { // look for function model
	    if (!urLookUp(callee)) {
	      llvm::outs() << "Warning once, undefined reference to "
			   << callee->getName().str() << "\n";
	    }
	    functionModels.handleFunctionDecl(state, ci, callee);
	  } else {
	    llvm::errs() << "Cannot handle function pointer in:\n";
	    instr->dump();
	  }
	  instr = instr->getNextNode();
	}
      } else {
	instr = instr->getNextNode();
	assert(instr && "Cannot find instr to execute");
      }
    }

    /* Process phi nodes */
    PCINSTR tail = state.getVirtualTail();
    while (const llvm::PHINode *phin =
	   llvm::dyn_cast<const llvm::PHINode>(instr)) {
      if (tail) { // we know its predecessor
	const llvm::BasicBlock *pred = tail->getParent();
	const llvm::Value *incoming = phin->getIncomingValueForBlock(pred);
	// not const expr summarised
	// never null
	Summary *sum = summariseEquality(instr, instr, incoming);
	state.consume(sum, NULL); // next instr is not used anyway
	delete sum;
      } else { // if we don't know the predecessor
	llvm::errs() << "Skipping phi node\n";
      }
      instr = instr->getNextNode();
    }

    /* Find the next node with summary/summaries */
    bool withinDL;
    while (true) {
      if (Sumbase::isKnown(instr)) {
	withinDL = state.readyChoicePool(instr);
	break; // the only route to the next section of this function
      } else if (const llvm::TerminatorInst *ti =
		 llvm::dyn_cast<const llvm::TerminatorInst>(instr)) {
	if (ti->getNumSuccessors() > 1) {
	  // all choices being failed could lead to this
	  // even so, something was very wrong before
	  llvm::errs() << "Error, conditional branch without summaries\n";
	  return;
	} else if (ti->getNumSuccessors() > 0) {
	  llvm::errs() << "Non-conditonal branch shouldn't be here\n";
	  instr = &(ti->getSuccessor(0)->front());
	}
	// ret instr has 0 succ
	else if (const llvm::ReturnInst *ri =
		 llvm::dyn_cast<const llvm::ReturnInst>(ti)) {
	  if (consumeRetInstr(state, ri)) {
	    instr = state.getNextInstr();
	  } else {
	    // entry function returns, could have a dummy call to verify
	    state.cover(ri); // I don't want to cover it, not by definition
	    terminate(state);
	    return;
	  }
	} else { // normal termination, could be unreachable, etc.
	  terminate(state);
	  return;
	}
      } else if (llvm::isa<const llvm::CallInst>(instr)) {
	llvm::errs() << "Call instr shouldn't be here\n";
	instr = instr->getNextNode();
      } else {
	// this instr is not known probably because it's missed out
	// by mistake (could be because of loop latch handling)
	llvm::errs() << "Missed out instr\n";
	instr = instr->getNextNode();
      }
      assert(instr && "Cannot find instr to execute");
    }

    /* Proceed according to the status of prepared choice pool */
    if (!withinDL) {
      // provide a partial solution to the path
      terminate(state, true);
      // might still be pushed later
    }
    int avail = state.getAvail();
    if (avail > 0) {
      if (state.getTotal() > 1)
	state.createRestorePoint();
      // the next instr is set by readyChoicePool
      // when visit, the virtual head and tail will be set
      interprocWorkStack.push(state);
    } else {
      // (avail == 0) => (!withinDL)
      assert(!withinDL && "Impossible");
    }
  }
  
}
