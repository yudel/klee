#include"cse/Common.h"
#include"cse/MemoryChecker.h"

#include"llvm/Support/raw_ostream.h"

#include<cassert>

namespace cse {

  // all these functions not type-checked
  void MemoryChecker::add(Z3_ast ptr, unsigned int scope, bool preOrPost) {
    if (preOrPost) {
      assert((preBPs.empty() || scope >= preBPs.back().scope)
	     && "Memory checker abnormal");
      preBPs.push_back(BoundPair(ptr, scope));
      if (MC_MONOTONE)
	edge = &(preBPs.back());
    } else {
      assert((postBPs.empty() || scope >= postBPs.back().scope)
	     && "Memory checker abnormal");
      postBPs.push_back(BoundPair(ptr, scope));
      if (MC_MONOTONE)
	edge = &(postBPs.back());
    }
  }
  void MemoryChecker::add(Z3_ast lb, Z3_ast ub, unsigned int scope,
			  bool preOrPost) {
    if (preOrPost) {
      assert((preBPs.empty() || scope >= preBPs.back().scope)
	     && "Memory checker abnormal");
      preBPs.push_back(BoundPair(lb, ub, scope));
      if (MC_MONOTONE)
	edge = &(preBPs.back());
    } else {
      assert((postBPs.empty() || scope >= postBPs.back().scope)
	     && "Memory checker abnormal");
      postBPs.push_back(BoundPair(lb, ub, scope));
      if (MC_MONOTONE)
	edge = &(postBPs.back());
    }
  }

  Z3_ast MemoryChecker::makeBoundCheck(Z3_context ctx, Z3_ast ptr,
				       bool preOrPost) {
    std::vector<BoundPair> *bps;
    if (preOrPost)
      bps = &preBPs;
    else
      bps = &postBPs;
    int n = bps->size();
    if (n == 0)
      return Z3_mk_true(ctx);
    // simplified version
    if (MC_MONOTONE) {
      Z3_ast check;
      if (edge->isSize1) {
	Z3_ast lt = Z3_mk_bvult(ctx, edge->lb, ptr);
	check = lt;
      } else {
	Z3_ast le = Z3_mk_bvule(ctx, edge->ub, ptr);
	check = le;
      }
      return check;
    }
    // mad version
    Z3_ast *checks = new Z3_ast[n];
    for (int i = 0; i < n; i++) {
      Z3_ast check;
      if ((*bps)[i].isSize1) {
	Z3_ast eq = Z3_mk_eq(ctx, (*bps)[i].lb, ptr);
	Z3_ast neq = Z3_mk_not(ctx, eq);
	check = neq;
      } else {
	Z3_ast le = Z3_mk_bvule(ctx, (*bps)[i].ub, ptr);
	if (!MC_MONOTONE) {
	  Z3_ast ge = Z3_mk_bvuge(ctx, (*bps)[i].lb, ptr);
	  Z3_ast toOr[2] = {le, ge};
	  check = Z3_mk_or(ctx, 2, toOr);
	} else {
	  check = le;
	}
      }
      checks[i] = check;
    }
    Z3_ast boundCheck = Z3_mk_and(ctx, n, checks);
    delete checks;
    return boundCheck;
  }
  Z3_ast MemoryChecker::makeBoundCheck(Z3_context ctx, Z3_ast lb, Z3_ast ub,
				       bool preOrPost) {
    std::vector<BoundPair> *bps;
    if (preOrPost)
      bps = &preBPs;
    else
      bps = &postBPs;
    int n = bps->size();
    if (n == 0)
      return Z3_mk_true(ctx);
    // simplified version
    if (MC_MONOTONE) {
      Z3_ast check;
      if (edge->isSize1) {
	Z3_ast lt = Z3_mk_bvult(ctx, edge->lb, lb);
	check = lt;
      } else {
	Z3_ast le = Z3_mk_bvule(ctx, edge->ub, lb);
	check = le;
      }
      return check;
    }
    // mad version
    Z3_ast *checks = new Z3_ast[n];
    for (int i = 0; i < n; i++) {
      Z3_ast check;
      if ((*bps)[i].isSize1) {
	Z3_ast lt = Z3_mk_bvult(ctx, (*bps)[i].lb, lb);
	if (!MC_MONOTONE) {
	  Z3_ast ge = Z3_mk_bvuge(ctx, (*bps)[i].lb, ub);
	  Z3_ast toOr[2] = {lt, ge};
	  check = Z3_mk_or(ctx, 2, toOr);
	} else {
	  check = lt;
	}
      } else {
	Z3_ast le = Z3_mk_bvule(ctx, (*bps)[i].ub, lb);
	if (!MC_MONOTONE) {
	  Z3_ast ge = Z3_mk_bvuge(ctx, (*bps)[i].lb, ub);
	  Z3_ast toOr[2] = {le, ge};
	  check = Z3_mk_or(ctx, 2, toOr);
	} else {
	  check = le;
	}
      }
      checks[i] = check;
    }
    Z3_ast boundCheck = Z3_mk_and(ctx, n, checks);
    delete checks;
    return boundCheck;
  }

  void MemoryChecker::backtrackTo(unsigned int scope) {
    // either this
    while (!preBPs.empty()) {
      if (preBPs.back().scope >= scope) {
	preBPs.pop_back();
	if (MC_MONOTONE && preBPs.empty())
	  edge = NULL;
      } else {
	if (MC_MONOTONE)
	  edge = &(preBPs.back());
	break;
      }
    }
    // or this
    while (!postBPs.empty()) {
      if (postBPs.back().scope >= scope) {
	postBPs.pop_back();
	if (MC_MONOTONE && postBPs.empty())
	  edge = NULL;
      } else {
	if (MC_MONOTONE)
	  edge = &(postBPs.back());
	break;
      }
    }
  }

  void MemoryChecker::clear() {
    preBPs.clear();
    postBPs.clear();
    edge = NULL;
  }
  
}
