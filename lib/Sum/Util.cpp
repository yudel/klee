#include"cse/Util.h"

#include"llvm/Support/raw_ostream.h"

#include<map>

namespace cse {

  struct StringRefCmp {
    bool operator()(const llvm::StringRef& sr1,
		    const llvm::StringRef& sr2) const {
      return sr1.compare(sr2) < 0;
    }
  };
  typedef std::map<llvm::StringRef, unsigned int, StringRefCmp> MSR2UI;
  MSR2UI typeSizeMap;
  unsigned int getFlattenSize(const llvm::Type &type) {
    if (!type.isAggregateType())
      return 1;
    if (type.isArrayTy()) {
      const llvm::ArrayType &arrayType =
	llvm::cast<const llvm::ArrayType>(type);
      const llvm::Type *elementType = arrayType.getElementType();
      return getFlattenSize(*elementType) * arrayType.getNumElements();
    } else if (type.isStructTy()) {
      const llvm::StructType &structType =
	llvm::cast<const llvm::StructType>(type);
      llvm::StringRef name;
      bool hasName = structType.hasName();
      if (hasName) {
	name = structType.getName();
	MSR2UI::iterator mi = typeSizeMap.find(name);
	if (mi != typeSizeMap.end())
	  return mi->second;
      }
      unsigned int size = 0;
      llvm::StructType::element_iterator ei = structType.element_begin(),
	ej = structType.element_end();
      for (; ei != ej; ++ei)
	size += getFlattenSize(**ei);
      if (hasName)
	typeSizeMap.insert(std::pair<llvm::StringRef, unsigned int>
			   (name, size));
      return size;
    } else {
      llvm::errs() << "Unknown type, couldn't get flatten size\n";
      return 1;
    }
  }
  unsigned int getElementFlattenOffset(const llvm::Type &type,
				       unsigned int index) {
    if (!type.isAggregateType()) // shouldn't happen
      return 0;
    if (type.isArrayTy()) {
      const llvm::ArrayType &arrayType =
	llvm::cast<const llvm::ArrayType>(type);
      const llvm::Type *elementType = arrayType.getElementType();
      unsigned int elementSize = getFlattenSize(*elementType);
      return elementSize * index;
    } else if (type.isStructTy()) {
      const llvm::StructType &structType =
	llvm::cast<const llvm::StructType>(type);
      unsigned int offset = 0;
      llvm::StructType::element_iterator ei = structType.element_begin(),
	ej = structType.element_end();
      for (unsigned int i = 0; ei != ej && i < index; ++ei, ++i)
	offset += getFlattenSize(**ei);
      return offset;
    } else {
      llvm::errs() << "Unknown type, couldn't get flatten offset\n";
      return 0;
    }
  }
  
  void readOperands(const llvm::Instruction &instr) {
    instr.dump();
    for (llvm::User::const_op_iterator
	   opi = instr.op_begin(), opj = instr.op_end();
	 opi != opj; opi++ ) {
      opi->get()->dump();
    }
  }

  const llvm::Function *getCallee(const llvm::CallInst *ci) {
    const llvm::Function *callee = ci->getCalledFunction();
    if (!callee) {
      const llvm::Value *ptr = ci->getCalledValue()->stripPointerCasts();
      // if it is a function
      if ((callee = llvm::dyn_cast<const llvm::Function>(ptr)))
	return callee;
      // else can't know it statically
      else
	callee = NULL; // not warning here
    }
    return callee;
  }

  // used for phi instr, ret instr, etc.
  Summary *summariseEquality(PCINSTR pos, PCINSTR instr,
			     const llvm::Value *value) {
    if (value) {
      Summary *s = new Summary(pos); // or use the corr version
      EqualityExpr *ee = new EqualityExpr(instr, value);
      s->addConstraint(ee, false);
      return s;
    } else {
      return NULL;
    }
  }

}
