#include"cse/Common.h"
#include"cse/Expr.h"
#include"cse/FunctionModels.h"
#include"cse/Util.h"

#include"llvm/IR/Constants.h"
#include"llvm/IR/Intrinsics.h"

#include<cassert>

namespace cse {

  // each va start has multiple constraints like store/loads
  // it could be expensive
  // works but returns strange messages and doesn't produce statistics
  void FunctionModels::handleVaStart(State &state,
				     const llvm::CallInst *ci,
				     const llvm::Function *f) {
    // note this is the real machine size, which shows us how the program
    // was compiled, I guess we need it to know the struct layout
    unsigned int pointerSizeInBits = context->getMachineWordSize();
    unsigned int modelPtrSize = context->getModelWordSize();
    if (pointerSizeInBits == 0) {
      llvm::errs() << "Function models not inited\n";
    } else if (pointerSizeInBits == 64) {
      // warning, the memory space allocated for llvm objects are not freed
      // this function can be called an arbitrary number of times
      // the unfreed stuff has clearly created problems, don't know how to fix
      Summary *sVaStart = new Summary(ci);

      // get the call instr that makes the vararg call
      // not the call instr that makes the va start call
      // because we've popped first
      const llvm::CallInst *vaci = llvm::cast
	<const llvm::CallInst>(state.getSummary()->getLastCallInstr());
      if (!vaci) {
	report("not enough dynamic vararg info, no model provided", 0x0181);
	return;
      }
      
      // make gp offset greater than 40, I don't know what it means
      // it's a hack, it makes things different than they actually are
      llvm::LLVMContext &llvmCtx = f->getContext();
      llvm::IntegerType *intType32 =
	llvm::IntegerType::get(llvmCtx, 32);
      llvm::ConstantInt *constInt48 = llvm::ConstantInt::get(intType32, 48);
      llvm::Value *vaListPtr = ci->getArgOperand(0);
      AccessingOperation *ao =
	new AccessingOperation(Expr::STORE, constInt48, vaListPtr);
      sVaStart->addConstraint(ao, false);

      // get the pointer to the beginning of space reserved for vas
      // i8*
      llvm::PointerType *ptrType =
	llvm::IntegerType::get(llvmCtx, 8)->getPointerTo();
      // complete hack, use an alloca instr to represent the pointer
      // i8**
      llvm::AllocaInst *vasPtr = new llvm::AllocaInst(ptrType);
      
      llvm::IntegerType *intTypePtrSize =
	llvm::IntegerType::get(llvmCtx, modelPtrSize);
      // 32 + 32 = 64, this constant is used in incrementing pointers
      llvm::ConstantInt *constInt2Or64 = MEMORY_FLATTEN ?
	llvm::ConstantInt::get(intTypePtrSize, 2) : // the thrid element
	llvm::ConstantInt::get(intTypePtrSize, 64);
      llvm::AllocaInst *constPtr2Or64 = new llvm::AllocaInst(ptrType);
      // not optimal, you can do constant propagation
      CastingExpr *ce =
	new CastingExpr(Expr::INT_PTR, constPtr2Or64, ptrType, constInt2Or64);
      sVaStart->addConstraint(ce, false);
      EquationExpr *ee =
	new EquationExpr(Expr::ADD, vasPtr, vaListPtr, constPtr2Or64);
      sVaStart->addConstraint(ee, false);

      // in case of fp vararg, we need to do this, the order is a bit strange
      if (FP_MEMORY) {
	// i32*
	llvm::AllocaInst *fpOffsetPtr = new llvm::AllocaInst(intType32);
	llvm::ConstantInt *constInt1Or32 = MEMORY_FLATTEN ?
	  llvm::ConstantInt::get(intTypePtrSize, 1) : // the second element
	  llvm::ConstantInt::get(intTypePtrSize, 32);
	llvm::AllocaInst *constPtr1Or32 = new llvm::AllocaInst(intType32);
	ce = new CastingExpr(Expr::INT_PTR, constPtr1Or32,
			     constPtr1Or32->getType(), constInt1Or32);
	sVaStart->addConstraint(ce, false);
	ee = new EquationExpr(Expr::ADD, fpOffsetPtr, vaListPtr, constPtr1Or32);
	sVaStart->addConstraint(ee, false);
	// make fp offset greater than 160, same
	llvm::ConstantInt *constInt168 = llvm::ConstantInt::get(intType32, 168);
	ao = new AccessingOperation(Expr::STORE, constInt168, fpOffsetPtr);
	sVaStart->addConstraint(ao, false);
      }
      
      // store every vararg
      // i8
      llvm::IntegerType *intType8 = llvm::IntegerType::get(llvmCtx, 8);
      // this constant is used in incrementing pointers
      llvm::ConstantInt *constInt1Or64 = MEMORY_FLATTEN ?
	llvm::ConstantInt::get(intTypePtrSize, 1) :
	llvm::ConstantInt::get(intTypePtrSize, 64);
      llvm::AllocaInst *constPtr1Or64 = new llvm::AllocaInst(ptrType);
      ce =
	new CastingExpr(Expr::INT_PTR, constPtr1Or64, ptrType, constInt1Or64);
      sVaStart->addConstraint(ce, false);
      int numArgs = vaci->getNumArgOperands();
      int numRegArgs =
	state.getSummary()->getLastCalledFunction()->arg_size(); // about right
      bool firstAddress = true;
      for (int i = numRegArgs; i < numArgs; i++) {
	// i8*
	llvm::AllocaInst *argPtr = new llvm::AllocaInst(intType8);
	// allocate an address for arg
	llvm::Value *arg = vaci->getArgOperand(i);
	llvm::Type *argType = arg->getType();
	unsigned int size = MEMORY_FLATTEN ?
	  getFlattenSize(*argType) : // should be 1
	  context->dl->GET_TYPE_SE_SIZE(argType);
	Allocation *alloca = new Allocation(argPtr, size);
	sVaStart->addConstraint(alloca, false);
	if (firstAddress) {
	  firstAddress = false;
	  // store arg's address
	  ao = new AccessingOperation(Expr::STORE, argPtr, vasPtr);
	  sVaStart->addConstraint(ao, false);
	}
	// store arg
	ao = new AccessingOperation(Expr::STORE, arg, argPtr);
	sVaStart->addConstraint(ao, false);
	// increment
	ee = new EquationExpr(Expr::ADD, argPtr, argPtr, constPtr1Or64);
	sVaStart->addConstraint(ee, false);
      }

      // consume and delete
      // this style is bad since we have previously consumed a fake summary
      state.consume(sVaStart, NULL); // don't care much about next instr
      delete sVaStart;
    } else {} // 32 unimpelemented
  }
  
  void FunctionModels::handleFunctionDecl(State &state,
					  const llvm::CallInst *ci,
					  const llvm::Function *f) {
    assert(f && f->isDeclaration() && "Function null or has a definition");
    // when there is a decl, it cannot be null
    Summary *summary = state.getSummary();
    switch (f->getIntrinsicID()) {
    case llvm::Intrinsic::ID::not_intrinsic:
      // handle normal declarations
      summary->pop();
      break;
    case llvm::Intrinsic::ID::vastart:
      // currently only care about the va start function
      report("trying to handle va start", 0x0180, true);
      // in this case we pop first, it's ugly but
      summary->pop();
      // unfortunately this one is only dynamically decidable, so no model
      // can be register to sumbase
      // nothing goes to solver, because all ecs are empty
      handleVaStart(state, ci, f);
      break;
    default:
      // silence
      summary->pop(); // as if seeing a ret
    }
    
  }


  
}
