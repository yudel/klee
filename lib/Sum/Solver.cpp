#include"cse/Solvers.h"

namespace cse {

  Solver *Solver::create(Context *context) {
    // should have other kinds of solvers
    Solver *solver = new Z3Solver();
    solver->context = context;
    solver->ptrSize = context->getModelWordSize();
    solver->init();
    return solver;
  }

  // testing only
  Solver *Solver::create() {
    Solver *solver = new Z3Solver();
    solver->context = NULL;
    solver->ptrSize = PTR_SIZE;
    solver->init();
    return solver;
  }

  unsigned int Solver::getOffsetInBits(llvm::StructType *structType,
				       const VersionedValue &vv) {
    const llvm::StructLayout *sl = context->dl->getStructLayout(structType);
    const llvm::ConstantInt *constInt =
      llvm::cast<const llvm::ConstantInt>(vv.get());
    int index = constInt->getSExtValue();
    if (index < 0) {
      llvm::outs() << "Strange index\n";
      return 0;
    } else {
      // have to be consitent with the get type size function!
      return sl->getElementOffsetInBits(unsigned(index));
    }
  }
  
}
