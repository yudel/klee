#include"cse/Expr.h"

namespace cse {

  // end
  vValueIterator::vValueIterator()
    : expr(NULL), vValue(NULL), id(VVI_INVALID) {}
    
  vValueIterator::vValueIterator(const Expr *expr)
    : expr(expr), vValue(expr->getOp0()), id(0) {}

  // no checking, careful
  vValueIterator::vValueIterator(const Expr *expr, VersionedValue *vValue)
    : expr(expr), vValue(vValue), id(1) {
    Expr::Kind kind = expr->getKind();
    assert((kind == Expr::LOAD || kind == Expr::STORE)
	   && "Wrong expr kind for iterator");
  }
    
  vValueIterator::vValueIterator(const vValueIterator &vvi)
    : expr(vvi.expr), vValue(vvi.vValue), id(vvi.id) {}
  
  vValueIterator &vValueIterator::operator++() {
    const Expr *e = this->expr;
    if (e == NULL) {
      this->pointToEnd();
      return *this;
    }
    if (++id < int(e->getNumOfVValues())) {
      Expr::Kind kind = e->getKind();
      if (kind == Expr::IS_TRUE || kind == Expr::IS_FALSE) {
	this->pointToEnd();
      } else if (kind > Expr::EQUATION_BEGIN && kind < Expr::EQUATION_END) {
	const EquationExpr *ee = static_cast<const EquationExpr *>(e);
	if (id == 1)
	  vValue = ee->getOp1();
	else if (id == 2)
	  vValue = ee->getOp2();
	else
	  this->pointToEnd();
      } else if (kind == Expr::EQ) {
	const EqualityExpr *ee = static_cast<const EqualityExpr *>(e);
	if (id == 1)
	  vValue = ee->getSrc();
	else
	  this->pointToEnd();
      } else if (kind == Expr::NEQ) {
	const InequalityExpr *ie = static_cast<const InequalityExpr *>(e);
	if (id <= int(ie->getNumOfIeqs()))
	  vValue = ie->getOps()[id - 1];
	else
	  this->pointToEnd();
      } else if (kind == Expr::ALLOCA) {
	const Allocation *alloc = static_cast<const Allocation *>(e);
	if (id == 1)
	  vValue = alloc->getArraySize();
	else
	  this->pointToEnd();
      } else if (kind == Expr::LOAD || kind == Expr::STORE) {
	const AccessingOperation *ao = static_cast
	  <const AccessingOperation *>(e);
	if (id == 2)
	  vValue = ao->getPtr();
	else
	  this->pointToEnd();
      } else if (kind == Expr::GEP) {
	const AddressingOperation *ao = static_cast
	  <const AddressingOperation *>(e);
	if (id == 1)
	  vValue = ao->getPtr();
	else if (id > 1)
	  vValue = ao->getIdx(id - 2);
	else
	  this->pointToEnd();
      } else if (kind > Expr::CASTING_BEGIN && kind < Expr::CASTING_END) {
	const CastingExpr *ce = static_cast<const CastingExpr *>(e);
	if (id == 1)
	  vValue = ce->getOrig();
	else
	  this->pointToEnd();
      } else if (kind == Expr::CMP) {
	const ComparisonExpr *ce = static_cast<const ComparisonExpr *>(e);
	if (id == 1)
	  vValue = ce->getOp1();
	else if (id == 2)
	  vValue = ce->getOp2();
	else
	  this->pointToEnd();
      } else if (kind == Expr::ITE) {
	const ITEExpr *itee = static_cast<const ITEExpr *>(e);
	if (id == 1)
	  vValue = itee->getCond();
	else if (id == 2)
	  vValue = itee->getOp1();
	else if (id == 3)
	  vValue = itee->getOp2();
	else
	  this->pointToEnd();
      } else {
	this->pointToEnd();
      }
    } else {
      this->pointToEnd();
    }
    return *this;
  }

  // this would create one additional object each time,
  // avoid using it for the good
  vValueIterator vValueIterator::operator++(int) {
    vValueIterator temp(*this);
    operator++();
    return temp;
  }

  bool vValueIterator::operator==(const vValueIterator &rhs) {
    return expr == rhs.expr && vValue == rhs.vValue && id == rhs.id;
  }
  bool vValueIterator::operator!=(const vValueIterator &rhs) {
    return !(*this == rhs);
  }

  VersionedValue &vValueIterator::operator*() const { return *vValue; }
  VersionedValue *vValueIterator::operator->() const { return vValue; }

  void vValueIterator::pointToEnd() {
    this->expr = NULL;
    this->vValue = NULL;
    this->id = VVI_INVALID;
  }

  // this memory space is not freed anywhere
  vValueIterator &Expr::getVValueIterator() const {
    return *(new vValueIterator(this));
  }

  // stupid cpp, no proper way to do copy
  Expr *Expr::copy() const {
    Kind kind = this->kind;
    if (kind == IS_TRUE || kind == IS_FALSE)
      return new UnaryExpr(*static_cast<const UnaryExpr *>(this));
    else if (kind == EQ)
      return new EqualityExpr(*static_cast<const EqualityExpr *>(this));
    else if (kind == NEQ)
      return new InequalityExpr(*static_cast<const InequalityExpr *>(this));
    else if (kind > EQUATION_BEGIN && kind < EQUATION_END)
      return new EquationExpr(*static_cast<const EquationExpr *>(this));
    else if (kind == ALLOCA)
      return new Allocation(*static_cast<const Allocation *>(this));
    else if (kind == LOAD || kind == STORE)
      return new AccessingOperation(*static_cast<const AccessingOperation *>
				    (this));
    else if (kind == GEP)
      return new AddressingOperation(*static_cast<const AddressingOperation *>
				     (this));
    else if (kind > CASTING_BEGIN && kind < CASTING_END)
      return new CastingExpr(*static_cast<const CastingExpr *>(this));
    else if (kind == CMP)
      return new ComparisonExpr(*static_cast<const ComparisonExpr *>(this));
    else if (kind == ITE)
      return new ITEExpr(*static_cast<const ITEExpr *>(this));
    else
      llvm::outs() << "Illegal expression for copy" << "\n";
    return NULL;
  }

  void Expr::dump() const {
    vValueIterator &vvi = this->getVValueIterator();
    vValueIterator &vvj = this->end();
    for (; vvi != vvj; ++vvi) {
      llvm::outs() << vvi->toString() << " ";
    }
    llvm::outs() << "\n";
    delete &vvi;
    delete &vvj;
  }

  UnaryExpr::UnaryExpr(const UnaryExpr &ue)
    : Expr(ue) {}

  void UnaryExpr::dump() const {
    if (kind == IS_TRUE)
      llvm::outs() << "IS_TRUE ";
    else if (kind == IS_FALSE)
      llvm::outs() << "IS_FLASE ";
    else
      llvm::outs() << "<op> " ;
    llvm::outs() << getOp0()->toString() << "\n";
  }

  EqualityExpr::EqualityExpr(const EqualityExpr &ee)
    : Expr(ee),
      src(new VersionedValue(ee.src)) {}
  
  void EqualityExpr::dump() const {
    vValueIterator &vvi = this->getVValueIterator();
    llvm::outs() << vvi->toString() << " == ";
    llvm::outs() << (++vvi)->toString() << "\n";
    delete &vvi;
  }

  InequalityExpr::InequalityExpr(const InequalityExpr &ie)
    : Expr(ie),
      ops(new VersionedValue *[ie.num - 1]) {
    for (unsigned int i = 0; i < ie.num - 1; i++)
      ops[i] = new VersionedValue(ie.ops[i]);
  }
  
  void InequalityExpr::dump() const {
    vValueIterator &vvi = this->getVValueIterator();
    vValueIterator &vvj = this->end();
    VersionedValue &vv = *vvi;
    ++vvi;
    bool prt = false;
    do {
      if (prt)
	llvm::outs() << " and\n";
      else
	prt = true;
      llvm::outs() << vv.toString() << " != ";
      llvm::outs() << vvi->toString();
    } while ((++vvi) != vvj);
    llvm::outs() << "\n";
    delete &vvi;
    delete &vvj;
  }

  EquationExpr::EquationExpr(const EquationExpr &ee)
    : Expr(ee),
      op1(new VersionedValue(ee.op1)),
      op2(new VersionedValue(ee.op2)) {}

  void EquationExpr::dump() const {
    vValueIterator &vvi = this->getVValueIterator();
    llvm::outs() << vvi->toString() << " == ";
    llvm::outs() << (++vvi)->toString();
    switch (kind) {
    case ADD:
    case FADD:
      llvm::outs() << " + ";
      break;
    case SUB:
    case FSUB:
      llvm::outs() << " - ";
      break;
    case MUL:
    case FMUL:
      llvm::outs() << " * ";
      break;
    case UDIV:
    case SDIV:
    case FDIV:
      llvm::outs() << " / ";
      break;
    case UREM:
    case SREM:
    case FREM:
      llvm::outs() << " % ";
      break;
    case SHL:
      llvm::outs() << " << ";
      break;
    case LSHR:
    case ASHR:
      llvm::outs() << " >> ";
      break;
    case AND:
      llvm::outs() << " and ";
      break;
    case OR:
      llvm::outs() << " or ";
      break;
    case XOR:
      llvm::outs() << " xor ";
      break;
    default:
      llvm::outs() << " <op> ";
    }
    llvm::outs() << (++vvi)->toString() << "\n";
    delete &vvi;
  }

  Allocation::Allocation(const Allocation &alloca)
    : Expr(alloca),
      arraySpecified(alloca.arraySpecified),
      size(alloca.size),
      arraySize(alloca.arraySpecified ?
		new VersionedValue(alloca.arraySize) : NULL) {}

  void Allocation::dump() const {
    vValueIterator &vvi = this->getVValueIterator();
    llvm::outs() << vvi->toString() << " == alloca";
    if (this->isArray())
      llvm::outs() << " " << (++vvi)->toString() << "\n";
    else
      llvm::outs() << "\n";
    delete &vvi;
  }

  AccessingOperation::AccessingOperation(const AccessingOperation &ao)
    : Expr(ao),
      desOrSrc(ao.kind == LOAD ?
	       this->vValue : new VersionedValue(ao.desOrSrc)),
      ptr(new VersionedValue(ao.ptr)) {}

  void AccessingOperation::dump() const {
    vValueIterator &vvi = this->getVValueIterator();
    llvm::outs() << vvi->toString() << " == *";
    llvm::outs() << (++vvi)->toString() << "\n";
    delete &vvi;
  }

  AddressingOperation::AddressingOperation(const AddressingOperation &ao)
    : Expr(ao),
      ptr(new VersionedValue(ao.ptr)),
      targetType(ao.targetType) {
    for (std::vector<VersionedValue *>::const_iterator i = ao.indices.begin(),
	   j = ao.indices.end(); i != j; i++) {
      indices.push_back(new VersionedValue(*i));
    }
  }

  void AddressingOperation::dump() const {
    vValueIterator &vvi = this->getVValueIterator();
    llvm::outs() << vvi->toString() << " == ";
    llvm::outs() << (++vvi)->toString() << " + *" << "\n";
    delete &vvi;
  }
  
  vValueIterator &AccessingOperation::getVValueIterator() const {
    return *(new vValueIterator(this, desOrSrc));
  }

  CastingExpr::CastingExpr(const CastingExpr &ce)
    : Expr(ce),
      type(ce.type),
      orig(new VersionedValue(ce.orig)) {}

  void CastingExpr::dump() const {
    vValueIterator &vvi = this->getVValueIterator();
    llvm::outs() << vvi->toString() << " == (ty) ";
    llvm::outs() << (++vvi)->toString() << "\n";
    delete &vvi;
  }

  ComparisonExpr::ComparisonExpr(const ComparisonExpr &ce)
    : Expr(ce),
      predicate(ce.predicate),
      op1(new VersionedValue(ce.op1)),
      op2(new VersionedValue(ce.op2)) {}

  void ComparisonExpr::dump() const {
    vValueIterator &vvi = this->getVValueIterator();
    llvm::outs() << vvi->toString() << " == ";
    llvm::outs() << (++vvi)->toString();
    using llvm::CmpInst;
    switch (predicate) {
    case CmpInst::FCMP_OEQ:
    case CmpInst::FCMP_UEQ: llvm::outs() << " == "; break;
    case CmpInst::FCMP_OGT:
    case CmpInst::FCMP_UGT: llvm::outs() << " > "; break;
    case CmpInst::FCMP_OGE:
    case CmpInst::FCMP_UGE: llvm::outs() << " >= "; break;
    case CmpInst::FCMP_OLT:
    case CmpInst::FCMP_ULT: llvm::outs() << " < "; break;
    case CmpInst::FCMP_OLE:
    case CmpInst::FCMP_ULE: llvm::outs() << " <= "; break;
    case CmpInst::FCMP_ONE:
    case CmpInst::FCMP_UNE: llvm::outs() << " != "; break;
    case CmpInst::ICMP_EQ: llvm::outs() << " == "; break;
    case CmpInst::ICMP_NE: llvm::outs() << " != "; break;
    case CmpInst::ICMP_UGT: llvm::outs() << " > "; break;
    case CmpInst::ICMP_UGE: llvm::outs() << " >= "; break;
    case CmpInst::ICMP_ULT: llvm::outs() << " < "; break;
    case CmpInst::ICMP_ULE: llvm::outs() << " <= "; break;
    case CmpInst::ICMP_SGT: llvm::outs() << " > "; break;
    case CmpInst::ICMP_SGE: llvm::outs() << " >= "; break;
    case CmpInst::ICMP_SLT: llvm::outs() << " < "; break;
    case CmpInst::ICMP_SLE: llvm::outs() << " <= "; break;
    default:
      llvm::outs() << " <cmp> ";
    }
    llvm::outs() << (++vvi)->toString() << "\n";
    delete &vvi;
  }
  
  ITEExpr::ITEExpr(const ITEExpr &itee)
    : Expr(itee),
      cond(new VersionedValue(itee.cond)),
      op1(new VersionedValue(itee.op1)),
      op2(new VersionedValue(itee.op2)) {}

  void ITEExpr::dump() const {
    vValueIterator &vvi = this->getVValueIterator();
    llvm::outs() << vvi->toString() << " == ";
    llvm::outs() << (++vvi)->toString() << " ? ";
    llvm::outs() << (++vvi)->toString() << " : ";
    llvm::outs() << (++vvi)->toString() << "\n";
    delete &vvi;
  }
  
  vValueIterator &Expr::end() const {
    return *(new vValueIterator());
  }
  
} // end of namespace cse
