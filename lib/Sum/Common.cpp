#include"cse/Common.h"

#include"llvm/Support/CommandLine.h"
#include"llvm/Support/raw_ostream.h"

namespace {

  llvm::cl::opt<unsigned int, true> CL_DL
  ("depth-limit", llvm::cl::desc
   ("Number of conditional branches (default=0 (off))"),
   llvm::cl::location(cse::DEPTH_LIMIT),
   llvm::cl::init(0));

  llvm::cl::opt<unsigned int, true> CL_TL
  ("test-limit", llvm::cl::desc
   ("Number of tests (default=0 (off))"),
   llvm::cl::location(cse::TEST_LIMIT),
   llvm::cl::init(0));

  llvm::cl::opt<bool, true> CL_V
  ("verbose", llvm::cl::desc
   ("Printing the progress info (default=false)"),
   llvm::cl::location(cse::V),
   llvm::cl::init(false));

  llvm::cl::opt<bool, true> CL_PT
  ("print-test", llvm::cl::desc
   ("Printing the generated tests (default=false)"),
   llvm::cl::location(cse::PRT_T),
   llvm::cl::init(false));

  llvm::cl::OptionCategory MEM_MACHINE_MODEL
  ("Memory and machine modelling", "");
  llvm::cl::opt<bool, true> CL_MC
  ("mem-check", llvm::cl::desc
   ("Memory operation correctness check (default=true)"),
   llvm::cl::location(cse::MEM_CHECK),
   llvm::cl::init(true),
   llvm::cl::cat(MEM_MACHINE_MODEL));

  llvm::cl::opt<bool, true> CL_MF
  ("mem-flatten", llvm::cl::desc
   ("Unifying alignment to simplify (default=true)"),
   llvm::cl::location(cse::MEMORY_FLATTEN),
   llvm::cl::init(true),
   llvm::cl::cat(MEM_MACHINE_MODEL));

  llvm::cl::opt<bool, true> CL_MCM
  ("mem-check-monotone", llvm::cl::desc
   ("Simple memory check (underappox) (default=true)"),
   llvm::cl::location(cse::MC_MONOTONE),
   llvm::cl::init(true),
   llvm::cl::cat(MEM_MACHINE_MODEL));
  
  llvm::cl::opt<bool, true> CL_FPM
  ("fp-memory", llvm::cl::desc
   ("Modelling memory operations on FP data (default=false)"),
   llvm::cl::location(cse::FP_MEMORY),
   llvm::cl::init(false),
   llvm::cl::cat(MEM_MACHINE_MODEL));

  llvm::cl::opt<unsigned int, true> CL_PS
  ("ptr-size", llvm::cl::desc
   ("Pointer size of memory model (default=32)"),
   llvm::cl::location(cse::PTR_SIZE),
   llvm::cl::init(32),
   llvm::cl::cat(MEM_MACHINE_MODEL));
  
  llvm::cl::opt<unsigned int, true> CL_CS
  ("cell-size", llvm::cl::desc
   ("Memory cell size (default=32, theoretically 1)"),
   llvm::cl::location(cse::CELL_SIZE),
   llvm::cl::init(32),
   llvm::cl::cat(MEM_MACHINE_MODEL));

  llvm::cl::OptionCategory SOLVER
  ("Solver", "");
  llvm::cl::opt<unsigned int, true> CL_ST
  ("solver-timeout", llvm::cl::desc
   ("Solver timeout in seconds (default=0 (off))"),
   llvm::cl::location(cse::SOLVER_TIMEOUT),
   llvm::cl::init(0),
   llvm::cl::cat(SOLVER));

  llvm::cl::opt<bool, true> CL_IS
  ("inc-solve", llvm::cl::desc
   ("Using incremental solving (default=true)"),
   llvm::cl::location(cse::INC_SOLVE),
   llvm::cl::init(true),
   llvm::cl::cat(SOLVER));

  llvm::cl::opt<bool, true> CL_SL // should read strings, dk how to do it
  ("solver-logic", llvm::cl::desc
   ("Solver logic (only one) (default=<auto detected>)"),
   llvm::cl::location(cse::L_QF_ABV),
   llvm::cl::init(false), // might not be optimal
   llvm::cl::cat(SOLVER));

  llvm::cl::opt<bool, true> CL_SUC
  ("simp-unsat-core", llvm::cl::desc
   ("Using unsat core to simplify (default=true)"),
   llvm::cl::location(cse::SIMP_UNSAT_CORE),
   llvm::cl::init(true), // might not be optimal
   llvm::cl::cat(SOLVER));

  llvm::cl::opt<unsigned int, true> CL_AL
  ("assumpt-limit", llvm::cl::desc
   ("Num of assumpts >= limit, try trimming (default=5)"),
   llvm::cl::location(cse::ASSUMPT_LIMIT),
   llvm::cl::init(5),
   llvm::cl::cat(SOLVER));

  llvm::cl::opt<bool, true> CL_QA
  ("quick-assumpt", llvm::cl::desc
   ("Using quick assumption checker (default=false)"),
   llvm::cl::location(cse::QUICK_ASSUMPT),
   llvm::cl::init(false),
   llvm::cl::cat(SOLVER));

}

namespace cse {

  /* General control */
  unsigned int W_DEPTH = 0;
  unsigned int DEPTH_LIMIT;

  unsigned int TEST_LIMIT;

  /* Memory & machine model */
  bool MEM_CHECK;
  bool MC_MONOTONE;
  unsigned int INIT_ADDRESS = 1; // assuming post
  // seems to be slower using this, guess solver doesn't like ugly value
  bool COMPACT_MEM = true;
  bool MEMORY_FLATTEN;

  bool FP_MEMORY;

  unsigned int PTR_SIZE;
  
  unsigned int CELL_SIZE;

  /* Solver */
  unsigned int SOLVER_TIMEOUT;
  
  bool INC_SOLVE;

  bool L_QF_ABV;
  // std::string SOLVER_LOGIC = L_QF_ABV ? "QF_ABV" : "";

  bool SIMP_UNSAT_CORE;
  unsigned int ASSUMPT_LIMIT;
  bool QUICK_ASSUMPT;
  
  /* Messages and errors */
  bool V;

  bool PRT_T;

  std::string h[16] = {
    "Trivial: ", // 0
    "Warning: ", // 1
    "Error: ", // 2
    "  " // 3, progress report, indented
  };
  std::string m[16] = {
    "Summarising instr", // 0
    "Summarising blocks", // 1
    "Executing", // 2
    "Summary", // 3
    "STP solver", // 4
    "Z3 solver", // 5
    "",
    "",
    "Function models", // 8
    "Test" // 9
    "",
    "",
    "",
    "",
    "",
    "", // F, leave blank
  };

  std::set<unsigned int> codes;

  void report(unsigned int code, bool once) {
    report("", code, once);
  }
  void report(std::string msg, unsigned int code, bool once) {
    code = code & 0x0FFF;
    if (!V && code == 0x03F0)
      return;
    if (once) {
      if (codes.find(code) == codes.end())
	codes.insert(code);
      else
	return;
    }
    unsigned int lc = code & 0x000F;
    code = code >> 4;
    unsigned int mc = code & 0x000F;
    code = code >> 4;
    unsigned int hc = code & 0x000F;
    (llvm::outs() << "[").write_hex(hc).write_hex(mc).write_hex(lc)
      << (once ? "-once] " : "] ")
      <<h[hc] << m[mc] << " " << msg << "\n";
  }

  /* About undefined references */
  std::set<const llvm::Function *> urs;
  
  bool urLookUp(const llvm::Function *f) {
    if (urs.find(f) == urs.end())
      urs.insert(f);
    else
      return true;
    return false;
  }
  
}
