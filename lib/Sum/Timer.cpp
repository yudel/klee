#include"cse/Timer.h"

namespace cse {

  void Timer::reset(int no) {
    trs[no] = TimeRecord();
  }
  
  void Timer::begin(int no) {
    trs[no].tp = SC::now();
    trs[no].counter++;
  }

  void Timer::pause(int no) {
    trs[no].ts += std::chrono::duration_cast<MS>(SC::now() - trs[no].tp);
  }

  void Timer::unpause(int no) {
    begin(no);
  }

  void Timer::end(int no) {
    pause(no);
  }

  void Timer::accumulate(int from, int to) {
    trs[to].ts += trs[from].ts;
    trs[to].counter++;
  }

  int Timer::get(int no) {
    return trs[no].ts.count();
  }
  float Timer::getAvg(int no) {
    return trs[no].ts.count() / float(trs[no].counter);
  }

  TimeRecord Timer::trs[N_TIMERECS];

}
