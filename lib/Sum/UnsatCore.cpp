#include"cse/UnsatCore.h"

namespace cse {

  unsigned int UnsatCore::getNumOfAssumptions() const {
    if (!current)
      return 0;
    return current->ars.size();
  }
  
  Z3_ast *UnsatCore::getAssumptions() const {
    int size = current->ars.size();
    Z3_ast *assumptions = new Z3_ast[size];
    int i = 0;
    for (MUI2AR::const_iterator ci = current->ars.begin(),
	   cj = current->ars.end(); ci != cj; ++ci)
      assumptions[i++] = (*ci).second.assumption;
    return assumptions; // not deleted, to be done
  }

  void UnsatCore::addAssumption(unsigned int hash, Z3_ast assumption,
				PCINSTR branch, PCINSTR choice) {
    current->asserted = false;
    MUI2AR &ars = current->ars;
    assert(ars.find(hash) == ars.end() && "Adding the same assumption again");
    ars.insert(std::pair<unsigned int, AssumptionRecord>
	       (hash, AssumptionRecord(assumption, branch, choice)));
  }

  void UnsatCore::addUnsatCore(unsigned int hash) {
    MUI2AR::const_iterator ci = current->ars.find(hash);
    if (ci == current->ars.end()) {
      llvm::errs() << "Cannot find assumption record\n";
      return;
    }
    current->ucs.insert(std::pair<PCINSTR, PCINSTR>
			(ci->second.branch, ci->second.choice));
  }

  void UnsatCore::assertAssumptions(Z3_context ctx, Z3_solver solver) {
    assert(current);
    if (current->asserted)
      return;
    for (MUI2AR::const_iterator ci = arsBegin(), cj = arsEnd(); ci != cj; ++ci)
      Z3_solver_assert(ctx, solver, ci->second.assumption);
    current->asserted = true;
  }
  
}
