#include"cse/Counter.h"
#include"cse/Statistics.h"
#include"cse/Timer.h"

namespace cse {

  void Statistics::init(const Context *context) {
    Statistics::context = context;
    fStats.open(context->getName().append(".stats"));
  }

  void Statistics::categoriseTime(int ms) {
    if (ms < 0)
      return;
    else if (ms < 10)
      Counter::inc(8);
    else if (ms < 100)
      Counter::inc(9);
    else
      Counter::inc(10);
  }
  
  void Statistics::cover(PCINSTR instr) {
    if (covered.find(instr) == covered.end())
      covered.insert(instr);
  }

  void Statistics::printCounter() {
    // from block summarising states
    fStats << "Constraint solver calls (sum): "
	   << cse::Counter::get(2) << "\n";
    // from all states
    fStats << "Constraint solver calls (state): "
	   << cse::Counter::get() << "\n";
    // all
    fStats << "Constraint solver calls (all): "
	   << cse::Counter::get(3) << "\n";
    // from assumption checking
    fStats << "Constraint solver calls (extra): "
	   << cse::Counter::get(6) << "\n";
    // from getting model or concretising
    fStats << "Constraint solver calls (misc): "
	   << cse::Counter::get(5) << "\n";
    // ending in undef results (most should be timeout)
    fStats << "Coustraint solver calls (undef): "
	   << cse::Counter::get(7) << "\n";
    fStats << "Number of tests: " << cse::Counter::get(1) << "\n";
    fStats << "Number of abandoned choices: " << W_DEPTH << "\n";
  }
  
  void Statistics::printTimer() {
    fStats << "Time elapsed (solver): " << cse::Timer::get(1) << " ms\n";
    fStats << "               (succ): " << cse::Timer::get(4) << " ms\n";
    fStats << "             (others): " << cse::Timer::get(3) << " ms\n";
    fStats << "       0 ~ 10 ms: " << cse::Counter::get(8) << "\n";
    fStats << "     10 ~ 100 ms: " << cse::Counter::get(9) << "\n";
    fStats << "100 ms ~ timeout: " << cse::Counter::get(10) << "\n";
    fStats << "Avg solver time: " << cse::Timer::getAvg(1) << " ms\n";
    fStats << "           succ: " << cse::Timer::getAvg(4) << " ms\n";
    fStats << "         others: " << cse::Timer::getAvg(3) << " ms\n";
    fStats << "Time elapsed (smx): " << cse::Timer::get() << " ms\n";
  }
  
  void Statistics::printCoverage() {
    const llvm::Module *module = context->module;
    fStats << module->getName().str() << "\n\n";
    for (llvm::Module::const_iterator mci = module->begin(),
	   mcj = module->end(); mci != mcj; ++mci) {
      const llvm::Function *function = &(*mci);
      fStats << function->getName().str() << "{\n";
      for (llvm::Function::const_iterator fci = function->begin(),
	     fcj = function->end(); fci != fcj; ++fci) {
	const llvm::BasicBlock *bb = &(*fci);
	fStats << "  " << bb->getName().str() << ":\n";
	for (llvm::BasicBlock::const_iterator bbci = bb->begin(),
	       bbcj = bb->end(); bbci != bbcj; ++bbci) {
	  PCINSTR instr = &(*bbci);
	  bool isCovered = false;
	  if (covered.find(instr) != covered.end())
	    isCovered = true;
	  fStats << "  " << (isCovered ? "c " : "u ")
		 << size_t(&(*bbci)) << " "
		 << (*bbci).getOpcodeName() << "\n";
	}
	fStats << "\n";
      }
      fStats << "}\n";
    }
  }

  const Context *Statistics::context;

  std::ofstream Statistics::fStats;
  
  std::set<PCINSTR> Statistics::covered;

}
