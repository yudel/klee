#include"cse/Statistics.h"
#include"cse/Summary.h"
#include"cse/VersionMap.h"

namespace cse {

  Summary::Summary(const Summary *s)
    : length(s->length),
      head(s->head), tail(s->tail),
      correspondent(s->correspondent),
      covered(s->covered),
      eCCount(s->eCCount), pcCount(s->pcCount),
      versionMap(s->versionMap),
      callStack(s->callStack) {
    for (Constraints::const_iterator ci = s->entryCond.begin(),
	   cj = s->entryCond.end(); ci != cj; ci++) {
      this->entryCond.push_back((*ci)->copy());
    }
    for (Constraints::const_iterator ci = s->postcond.begin(),
	   cj = s->postcond.end(); ci != cj; ci++) {
      this->postcond.push_back((*ci)->copy());
    }
  }

  Summary::Summary(const Summary *s, PCINSTR newHead, PCINSTR newTail)
    : length(s->length),
      head(newHead), tail(newTail),
      correspondent(s->correspondent),
      covered(s->covered),
      eCCount(s->eCCount), pcCount(s->pcCount),
      versionMap(s->versionMap),
      callStack(s->callStack) {
    for (Constraints::const_iterator ci = s->entryCond.begin(),
	   cj = s->entryCond.end(); ci != cj; ci++) {
      this->entryCond.push_back((*ci)->copy());
    }
    for (Constraints::const_iterator ci = s->postcond.begin(),
	   cj = s->postcond.end(); ci != cj; ci++) {
      this->postcond.push_back((*ci)->copy());
    }
  }

  Summary::~Summary() {
    for (Constraints::iterator ci = entryCond.begin(), cj = entryCond.end();
	 ci != cj; ci++) {
      delete (*ci);
    }
    for (Constraints::iterator ci = postcond.begin(), cj = postcond.end();
	 ci != cj; ci++) {
      delete (*ci);
    }
    // ec, pc vectors, the map and the stack should be automatically deleted
  }

  void Summary::cover(const Summary *op1, const Summary *op2) {
    for (std::vector<PCINSTR>::const_iterator ci = op1->covered.cbegin(),
	   cj = op1->covered.cend(); ci != cj; ++ci)
      this->covered.push_back(*ci);
    for (std::vector<PCINSTR>::const_iterator ci = op2->covered.cbegin(),
	   cj = op2->covered.cend(); ci != cj; ++ci)
      this->covered.push_back(*ci);
  }
  
  std::string Summary::prepareAssumption(PCINSTR instr, PCINSTR &choice) const {
    for (std::vector<PCINSTR>::const_iterator ci = covered.cbegin(),
	   cj = covered.cend(); ci != cj; ++ci) {
      if ((*ci) == instr) {
	PCINSTR nextInstr = *(++ci);
	std::stringstream ss;
	ss << "0x" << std::hex << size_t(instr)
	   << "-0x" << std::hex << size_t(nextInstr);
	choice = nextInstr;
	return ss.str();
      } else {
	continue;
      }
    }
    llvm::errs() << "Unable to find the asked pattern in summary\n";
    choice = NULL;
    return std::string("");
  }

  const Constraints Summary::getAdditional(unsigned int from) const {
    // note that although the vector is new, the exprs are not new
    // it's odd but fine, as long as no one modifies/deletes anything
    // in or pointed to by this vector, except for deleting the whole
    // vector
    if (from < eCCount)
      return Constraints(entryCond.begin() + from, entryCond.end());
    else
      return Constraints();
  }
  
  // constraint shouldn't be null
  void Summary::addConstraint(Constraint *c, bool preOrPost) {
    updateVMap(c, preOrPost);
    postcond.push_back(c);
    pcCount++;
  }

  void Summary::addConstraintBoth(Constraint *c, bool preOrPost) {
    updateVMap(c, preOrPost);
    entryCond.push_back(c);
    postcond.push_back(c->copy());
    eCCount++;
    pcCount++;
  }

  void Summary::addConstraint(Constraint *c) {
    updateVMapValuePreserving(c);
    postcond.push_back(c);
    pcCount++;
  }

  void Summary::addConstraintBoth(Constraint *c) {
    updateVMapValuePreserving(c);
    entryCond.push_back(c);
    postcond.push_back(c->copy());
    eCCount++;
    pcCount++;
  }

  bool Summary::isEmpty() const {
    bool isEmpty = postcond.empty();
    assert(!isEmpty || entryCond.empty());
    return isEmpty && callStack.empty();
  }
  // ec or pc is immediately true if it is empty
  bool Summary::isECImdTrue() const {
    return entryCond.empty();
  }
  bool Summary::isPcImdTrue() const {
    return postcond.empty();
  }

  unsigned int Summary::getECLength() const {
    return entryCond.size();
  }

  // lack of simplification
  Summary *Summary::SUM_BOP(const Summary *op1, const Summary *op2,
			    bool preOrPost) {
    const Constraints *cs;
    Constraint *c;
    // preOrPost???
    Summary *res = new Summary(op1->getLength() + op2->getLength(),
			       op1->getHead(), op2->getTail(), op2->getCorr());
    res->cover(op1, op2);
    res->updateCallStack(op1);
    res->updateCallStack(op2);
    // the direction matters in that so that incremental solving
    // can be implemented, alternatives (pointer values as variable names)?
    if (!preOrPost) {
      // res->ec = copy(op1->ec or op1->pc)
      // res->pc = copy(op1->pc)
      if (!op1->isPcImdTrue()) {
	if (op2->isECImdTrue()) { // don't know if it's better
	  // res->pc = copy(op1->pc)
	  cs = op1->getPc();
	  for (Constraints::const_iterator ci = cs->begin(), cj = cs->end();
	       ci != cj; ci++) {
	    c = (*ci)->copy();
	    res->addConstraint(c);
	  }
	  // res->ec = copy(op1->ec)
	  if (!op1->isECImdTrue()) {
	    cs = op1->getEC();
	    for (Constraints::const_iterator ci = cs->begin(), cj = cs->end();
		 ci != cj; ci++) {
	      c = (*ci)->copy();
	      res->addConstrECNoUpdateAtAll(c);
	    }
	  }
	}
	// res->ec = res->pc = copy(op1->pc)
	else {
	  cs = op1->getPc();
	  for (Constraints::const_iterator ci = cs->begin(), cj = cs->end();
	       ci != cj; ci++) {
	    c = (*ci)->copy();
	    res->addConstraintBoth(c);
	  }
	}
      }
      // res->vmap = copy(op1->vmap)
      res->updateVMapMemoryPreserving(op1->versionMap);

      const VersionMap *vMap = op2->getVMap();
      // res->ec += copy(op2->ec)
      if (!op2->isECImdTrue()) {
	cs = op2->getEC();
	for (Constraints::const_iterator ci = cs->begin(), cj = cs->end();
	     ci != cj; ci++) {
	  c = (*ci)->copy();
	  // preOrPost = false
	  res->addConstrECNoUpdate(c, false, *vMap);
	}
      }
      // res->pc += copy(op2->pc)
      if (!op2->isPcImdTrue()) {
	cs = op2->getPc();
	for (Constraints::const_iterator ci = cs->begin(), cj = cs->end();
	     ci != cj; ci++) {
	  c = (*ci)->copy();
	  // preOrPost = false
	  res->addConstrPcNoUpdate(c, false, *vMap);
	}
      }
      // res->vmap += copy(op2->vmap)
      res->versionMap.setPost(*vMap);
    } else {} // unimplemented
    return res;
  }

  Summary *Summary::SUM_BOP_REVERSE(const Summary *op1, bool preOrPost) {
    Summary *res = NULL;
    // unimplemented
    return res;
  }

  void Summary::statsDump() const {
    for (std::vector<PCINSTR>::const_iterator ci = covered.cbegin(),
	   cj = covered.cend(); ci != cj; ++ci)
      Statistics::cover(*ci);
  }
  
  void Summary::dump() const {
    llvm::outs() << "Covered:\n";
    for (std::vector<PCINSTR>::const_iterator ci = covered.begin(),
	   cj = covered.end(); ci != cj; ++ci) {
      (*ci)->dump();
    }
    llvm::outs() << "Constraints:\n";
    for (Constraints::const_iterator ci = postcond.begin(), cj = postcond.end();
	 ci != cj; ci++) {
      (*ci)->dump();
    }
  }

  // the name..
  void Summary::addConstrECNoUpdateAtAll(Constraint *c) {
    entryCond.push_back(c);
    eCCount++;
  }
  
  void Summary::addConstrECNoUpdate(Constraint *c, bool preOrPost,
				    const VersionMap &another) {
    // no updating the vmap yet, but still need to update the vv
    updateVVOnly(c, preOrPost, another);
    entryCond.push_back(c);
    eCCount++;
  }

  void Summary::addConstrPcNoUpdate(Constraint *c, bool preOrPost,
				    const VersionMap &another) {
    updateVVOnly(c, preOrPost, another);
    postcond.push_back(c);
    pcCount++;
  }

  void Summary::updateVVOnly(Constraint *c, bool preOrPost,
			     const VersionMap &another) {
    vValueIterator &vvi = c->getVValueIterator();
    vValueIterator &vvj = c->end();
    for (; vvi != vvj; ++vvi) {
      if (preOrPost) {
	int difference = another.getHighest(vvi->get()) - vvi->getVersion();
	assert(difference >= 0);
	int lowest = versionMap.getLowest(vvi->get());
	vvi->setVersion(lowest - difference);
	difference = another.getMemoryHighest() - vvi->getMemoryVersion();
	assert(difference >= 0);
	lowest = versionMap.getMemoryLowest();
	vvi->setMemoryVersion(lowest - difference);
      } else {
	int difference = vvi->getVersion() - another.getLowest(vvi->get());
	assert(difference >= 0);
	int highest = versionMap.getHighest(vvi->get());
	vvi->setVersion(highest + difference);
	difference = vvi->getMemoryVersion() - another.getMemoryLowest();
	assert(difference >= 0);
	highest = versionMap.getMemoryHighest();
	vvi->setMemoryVersion(highest + difference);
      }
    }
    delete &vvi;
    delete &vvj;
  }
  
  // just like copying from another map step by step
  void Summary::updateVMapValuePreserving(const Constraint *c) {
    vValueIterator &vvi = c->getVValueIterator();
    vValueIterator &vvj = c->end();
    for (; vvi != vvj; ++vvi)
      vvi->toVMapPreserving(versionMap);
    // memory will be updated separately
    delete &vvi;
    delete &vvj;
  }

  void Summary::updateVMapMemoryPreserving(const VersionMap &vMap) {
    versionMap.setMemoryPreserving(vMap.getMemoryHighest(),
				   vMap.getMemoryLowest());
  }

  // although the name says "update vmap", the constraint
  // (its variable versions) also gets modified
  // this function combines all of the other update functions,
  // but can only be used for summarising individual instrs
  void Summary::updateVMap(Constraint *c, bool preOrPost) {
    // process memory first for pre
    if (preOrPost && c->isMemoryLifted())
      versionMap.setMemoryPre();
    // process vvs
    vValueIterator &vvi = c->getVValueIterator();
    vValueIterator &vvj = c->end();
    VersionedValue *vvFirst = &(*vvi);
    bool skipHead = !preOrPost && vvFirst->isLifted();
    if (skipHead)
      ++vvi;
    for (; vvi != vvj; ++vvi) {
      if (preOrPost)
	vvi->toVMapPre(versionMap);
      else
	vvi->toVMapPost(versionMap);
    }
    if (skipHead)
      vvFirst->toVMapPost(versionMap);
    delete &vvi;
    delete &vvj;
    // process memory for post
    if (!preOrPost && c->isMemoryLifted())
      versionMap.setMemoryPost();
  }

  std::stack<CIStackFrame> &Summary::getCallStack() {
    return callStack;
  }
  PCINSTR Summary::getLastCallInstr() const {
    // no checking
    return callStack.top().getCallInstr();
  }
  llvm::Function *Summary::getLastCalledFunction() const {
    // no checking
    return callStack.top().getFunction();
  }
  llvm::Function *Summary::getLastCalledFunction(PCINSTR callInstr) const {
    if (callStack.empty()) {
      // this means either it's not a call instr summary
      // or we have a function pointer
      return NULL;
    } else {
      // this returns function declaration or definition
      const CIStackFrame &cisf = callStack.top();
      if (cisf.getCallInstr() == callInstr) {
	// making sure the top stack frame is refering to the current call
	// instr; although, the same call instr can appear in the stack
	// frame multiple times, but as long as we know it appear once, we
	// know we have the right function, because for a call instr, it
	// either appear always, or not at all, and the function is always
	// the same for a call instr
	return cisf.getFunction();
      } else {
	// in the wrong stack frame
	// could be stack being disordered, or the same as the first case
	return NULL;
      }
    }
  }
  void Summary::push(const llvm::Function *callee, PCINSTR callInstr) {
    callStack.push(CIStackFrame(callee, callInstr));
    // deepen(); // optional
  }
  PCINSTR Summary::pop() {
    if (callStack.empty())
      return NULL; // consider as top level return
    PCINSTR callInstr = callStack.top().getCallInstr();
    callStack.pop();
    return callInstr;
  }
  
  void Summary::updateCallStack(const Summary *other) {
    // warning, stupid
    std::stack<CIStackFrame> temp = other->callStack;
    std::stack<CIStackFrame> tempReverse;
    while (!temp.empty()) {
      tempReverse.push(temp.top());
      temp.pop();
    }
    while (!tempReverse.empty()) {
      this->callStack.push(tempReverse.top());
      tempReverse.pop();
    }
  }
  
}
