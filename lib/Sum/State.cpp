#include"cse/State.h"
#include"cse/Statistics.h"
#include"cse/Sumbase.h"

namespace cse {

  bool Choice::operator==(const Choice &other) const {
    return this->target == other.target && this->summary == other.summary;
  }

  bool Choice::operator<(const Choice &other) const {
    return this->target < other.target || (this->target == other.target
					   && this->summary < other.summary);
  }

  bool Choice::operator>(const Choice &other) const {
    return this->target > other.target || (this->target == other.target
					   && this->summary > other.summary);
  }

  void Choice::setSlvRes(SolverResult slvRes) const {
    this->slvRes = slvRes;
  }

  PCINSTR State::getVirtualHead() {
    return virtualHead;
  }
  PCINSTR State::getVirtualTail() {
    return virtualTail;
  }
  PCINSTR State::getHead() {
    if (summary)
      return summary->getHead();
    else
      return NULL;
  }
  PCINSTR State::getTail() {
    if (summary)
      return summary->getTail();
    else
      return NULL;
  }

  void State::cover(PCINSTR instr) {
    if (summary)
      summary->cover(instr);
    else
      covered.push_back(instr);
  }

  bool State::isAllChosen() {
    if (availChoices > 0)
      return false;
    else
      return true;
  }
  void State::addChoice(PCINSTR via, PCINSTR target) {
    Choice c(via, target);
    // not checking if existed
    choicePool.insert(c); // does it change the iterator end???
    numOfChoices++;
    availChoices++;
  }
  void State::addChoice(PCINSTR via, PCINSTR target,
			const Summary *summary) {
    Choice c(via, target, summary);
    choicePool.insert(c);
    numOfChoices++;
    availChoices++;
  }

  bool State::hasOnlyChoice() {
    return (availChoices == 1 && allChosenFailed);
  }
  const Choice *State::visitChoice() {
    if (availChoices > 0)
      availChoices--;
    else
      return NULL;
    // naive search strategy
    SI si = (lastChoice == choicePool.end()) ?
      choicePool.begin() : ++lastChoice;
    for (SI sj = choicePool.end(); si != sj; si++) {
      if (!si->visited) {
	si->visited = true;
	lastChoice = si;
	return &(*si);
      }
    }
    // something's wrong
    llvm::errs() << "Choice pool error\n";
    return NULL;
  }
  void State::setSlvRes(const Choice *c, SolverResult slvRes) {
    if (slvRes == SLV_FALSE) {
      c->setSlvRes(slvRes);
    } else {
      c->setSlvRes(slvRes);
      allChosenFailed = false;
    }
  }
  State State::visit(bool &res, bool &clear) {
    res = false;
    bool mustSat = hasOnlyChoice();
    const Choice *c = this->visitChoice();
    Solver *nullSolver = NULL;
    State newState(this->solver->context, nullSolver);
    newState.copyFrom(*this); // the search backtracks
    if (numOfChoices > 1)
      newState.backtrack(); // the solver backtracks
    if (c) {
      const Summary *selectedSummary =
	c->summary ? c->summary : Sumbase::getInstrSum(c->via, c->target);
      if (selectedSummary) {
	Summary *newSummary = newState.getNewSummary(selectedSummary);
	if (selectedSummary->getECCount() > 0) {
	  assert(numOfChoices > 1);
	  clear = newState.toSolver(newSummary); // add condition to solver
	  assert(clear && "Error adding constraint");
	  if (mustSat) {
	    // even we know that the condition can be proven,
	    // we still add the condition to the solver, maybe it helps
	    res = true;
	    setSlvRes(c, SLV_TRUE);
	  } else {
	    Counter::inc(); // solver count (state)
	    Counter::inc(2); // solver count (sum)
	    SolverResult slvRes = newState.solver->check();
	    if (slvRes == SLV_TRUE) {
	      res = true;
	    } else if (slvRes == SLV_FALSE) {
	      res = false;
	    } else { // undef
	      assert(virtualHead);
	      Sumbase::registerIncomplete(virtualHead);
	      res = false; // abandom
	    }
	    setSlvRes(c, slvRes);
	  }
	} else {
	  assert(numOfChoices == 1);
	  res = true;
	  setSlvRes(c, SLV_TRUE);
	}
	if (res) {
	  newState.update(newSummary, c->target);
	  // conditional branch, regardless of feasibility,
	  // if and only if the choices are added
	  // calls also increase length (optionally)
	  if (numOfChoices > 1)
	    newState.deepen();
	} else {
	  // if fail abandon choice, manual or automatic deletion
	  delete newSummary;
	  // backtrack to previous restore point, not done here
	}
      } else {
	res = true;
	setSlvRes(c, SLV_TRUE);
	newState.consumeEmpty(c->via, c->target);
      }
      return newState;
    } else {
      // this could never happen
      llvm::errs() << "Visiting choice failed\n";
      return newState;
    }
  }
  const Choice *State::visitChoice(PCINSTR target) {
    Choice c(target);
    SI si = choicePool.find(c);
    if (si == choicePool.end())
      return NULL;
    else if (si->visited)
      return NULL;
    else {
      si->visited = true;
      availChoices--;
      return &(*si);
    }
  }

  Summary *State::getSummary() {
    return summary;
  }

  void State::consumeEmpty(PCINSTR instr, PCINSTR nextInstr) {
    if (summary)
      summary->setCorr(NULL);
    if (!virtualHead)
      virtualHead = instr;
    virtualTail = instr;
    setNextInstr(nextInstr);
    cover(instr);
  }

  /**
   * This block of code is messy, read together
   */
  Summary *State::getNewSummary(const Summary *next) {
    // summary bop
    if (this->summary)
      return Summary::SUM_BOP(this->summary, next, false);
    Summary *newSummary = new Summary(next);
    // update the coverage info
    if (!covered.empty())
      newSummary->cover(covered.begin(), covered.end());
    covered.clear(); // we now have a summary instead to store this info
    return newSummary;
  }
  bool State::toSolver(const Summary *newSummary) {
    genned = false; // shouldn't set for must sat
    unsigned int from;
    if (this->summary)
      from = this->summary->getECLength();
    else
      from = 0;
    // remember to push beforehand
    return this->solver->add(newSummary->getAdditional(from));
  }
  void State::updateSummary(Summary *newSummary) {
    Summary *temp = NULL;
    if (this->summary && this->summary != newSummary)
      temp = this->summary;
    this->summary = newSummary;
    delete temp;
    // not touching virtual head and tail
    // not touching nextInstr
  }
  void State::update(Summary *newSummary, PCINSTR nextInstr) {
    updateSummary(newSummary);
    if (!virtualHead)
      virtualHead = newSummary->getHead();
    virtualTail = newSummary->getTail();
    this->nextInstr = nextInstr;
  }
  void State::consume(const Summary *next, PCINSTR nextInstr, bool preOrPost) {
    if (preOrPost) {
      // unimplemented
    } else {
      Summary *newSummary = getNewSummary(next);
      if (next->getECCount() > 0) {
	bool clear = toSolver(newSummary);
	assert(clear && "Error adding constraint");
      }
      update(newSummary, nextInstr);
    }
  }
  /* End of messy code */

  void State::createRestorePoint() {
    restorePoint = solver->createRestorePoint();
  }
  void State::backtrack() {
    solver->backtrackTo(restorePoint);
  }
  void State::gen() {
    if (summary->isECImdTrue()) {
      if (PRT_T)
	llvm::outs() << "No constraint no model\n";
      return;
    }
    if (genned)
      return;
    genned = true;
    solver->getModel();
    if (PRT_T)
      solver->display();
  }

  void State::solverDump() {
    solver->dump();
    // debug
    assert(solver->check() == SLV_TRUE); // comment it
  }

  void State::registerSummary() {
    if (summary) {
      assert(virtualHead && virtualTail && "State info incomplete");
      assert(covered.empty() && "Covered instr unrecorded");
      Summary *toReg = new Summary(summary, virtualHead, virtualTail);
      Sumbase::registerSummary(virtualHead, virtualTail, toReg);
    } else {
      report("tried to register empty summary", 0x0031, true);
      if (virtualHead && virtualTail) {
	// instrs included are not summariesed for various of reasons,
	// it doesn't have constraints; it's not supported by the model; etc.
	// all the conditions of use apply confirmed
	Summary *emptyToReg = new Summary(virtualHead, virtualTail, covered);
	Sumbase::registerSummary(virtualHead, virtualTail, emptyToReg);
      }
    }
  }

  void State::statsDump() {
    Counter::inc(1); // num of tests
    if (!summary) {
      for (std::vector<PCINSTR>::iterator vi = covered.begin(),
	     vj = covered.end(); vi != vj; ++vi)
	Statistics::cover(*vi);
    } else {
      assert(covered.empty() && "Covered instr unrecorded");
      summary->statsDump();
    }
  }

  void State::dump() {
    llvm::outs() << "From: ";
    virtualHead->dump();
    summary->dump();
    llvm::outs() << "  To: ";
    virtualTail->dump();
  }
  
}
