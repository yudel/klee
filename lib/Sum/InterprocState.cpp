#include"cse/InterprocState.h"
#include"cse/Sumbase.h"
#include"cse/UnsatCore.h"

namespace cse {

  void InterprocState::clearChoicePool() {
    // no warning if pool weren't empty
    choicePool.clear();
    numOfChoices = 0;
    availChoices = 0;
    lastChoice = choicePool.end();
  }
  bool InterprocState::readyChoicePool(PCINSTR instr) {
    bool withinDL = true;
    clearChoicePool();
    setNextInstr(instr); // must
    // single instr sums don't matter now
    Summaries *sums = Sumbase::getSums(instr);
    if (Sumbase::isIncomplete(instr))
      allChosenFailed = false;
    for (Summaries::VCI vi = sums->begin(), vj = sums->end(); vi != vj; ++vi) {
      const Summary *sum = *vi;
      if (DEPTH_LIMIT &&
	  DEPTH_LIMIT < (summary ?
			 summary->getLength() + sum->getLength() :
			 sum->getLength())) {
	// some choices are directly abandoned. we know it's not going
	// to finish before the depth is too large. we will provide a
	// "half solution" if there is such path	
	if (!(W_DEPTH++))
	  llvm::outs() << "Warning once, depth limit exceeded\n";
	withinDL = false;
	continue;
      }
      // corr not always available
      // directly add sum, not newing
      addChoice(instr, sum->getCorr(), sum);
    }
    if (numOfChoices != int(sums->size()))
      allChosenFailed = false;
    // since there is depth limit, we need to set the total number separately
    numOfChoices = sums->size();
    return withinDL;
  }

  InterprocState InterprocState::visit(bool &res, bool &clear) {
    res = false;
    clear = true;
    bool mustSat = hasOnlyChoice();
    const Choice *c = this->visitChoice();
    Solver *nullSolver = NULL;
    InterprocState newState(this->solver->context, nullSolver);
    newState.copyFrom(*this); // the search backtracks
    if (numOfChoices > 1)
      newState.backtrack(); // the solver backtracks
    if (c) {
      const Summary *selectedSummary = c->summary;
      if (selectedSummary) {
	Summary *newSummary = newState.getNewSummary(selectedSummary);
	bool condExists = selectedSummary->getECCount() > 0;
	if (condExists) {
	  assert(numOfChoices > 1); // 1 violation found, not fixed
	  if (mustSat) {
	    // even we know that the condition can be proven,
	    // we still add the condition to the solver, maybe it helps
	    // add condition to solver
	    clear = newState.toSolver(newSummary);
	    if (clear) {
	      res = true;
	      setSlvRes(c, SLV_TRUE);
	    } else {
	      res = false;
	      setSlvRes(c, SLV_UNDEF);
	    }
	  } else {
	    newState.solver->prepare(selectedSummary);
	    clear = newState.toSolver(newSummary); // add condition to solver
	    if (clear) {
	      Counter::inc(); // solver count (state)
	      SolverResult slvRes = newState.solver->check();
	      if (slvRes == SLV_TRUE) {
		newState.solver->afterCheck(true);
		res = true;
	      } else if (slvRes == SLV_FALSE) {
		// note it is the backtrack state that is trimming
		this->trimChoicePool();
		newState.solver->afterCheck(false); // doesn't matter
		res = false;
	      } else { // undef
		// not trimming
		newState.solver->afterCheck(false); // doesn't matter
		res = false; // abandom
	      }
	      setSlvRes(c, slvRes);
	    } else {
	      newState.solver->afterCheck(false); // doesn't matter
	      res = false;
	      setSlvRes(c, SLV_UNDEF);
	    }
	  }
	} else {
	  assert(numOfChoices == 1);
	  res = true;
	  setSlvRes(c, SLV_TRUE);
	}
	if (res) {
	  // target could be null, handled later
	  newState.update(newSummary, c->target);
	} else {
	  // if fail abandon choice, manual or automatic deletion
	  delete newSummary;
	  // backtrack to previous restore point, not done here
	}
      } else {
	// never
	llvm::errs() << "Summary not found\n";
      }
      return newState;
    } else {
      // this could never happen
      llvm::errs() << "Visiting choice failed\n";
      return newState;
    }
  }

  void InterprocState::pushEntryFunc(const llvm::Function *entryFunc) {
    if (summary)
      summary->push(entryFunc, NULL);
    else
      llvm::outs() << "Summary not ready for entry func\n";
  }
  bool InterprocState::finished() {
    if (summary)
      return summary->getCallStack().empty();
    else
      return true;
  }

  bool InterprocState::checkAgainstUnsatCore(const Summary *sum) {
    assert(sum);
    SPPP &ucs = solver->getUnsatCore().getUnsatCores();
    if (ucs.empty()) {
      llvm::errs() << "No unsat core\n";
      return false;
    }
    SPPP::const_iterator sci, scj;
    for (sci = ucs.cbegin(), scj = ucs.cend(); sci != scj; ++sci) {
      bool match = false;
      for (std::vector<PCINSTR>::const_iterator
	     vpci = sum->getCovered().cbegin(),
	     vpcj = sum->getCovered().cend(),
	     vpck = vpci + 1;
	   vpck != vpcj; ++vpci, ++vpck) {
	PCINSTR branch = *vpci;
	PCINSTR choice = *vpck;
	if (sci->first == branch && sci->second == choice) {
	  match = true;
	  break;
	}
      }
      if (!match)
	return false;
    }
    return true;
    /*
    for (std::vector<PCINSTR>::const_iterator vpci = sum->getCovered().cbegin(),
	   vpcj = sum->getCovered().cend(), vpck = vpci + 1;
	 vpck != vpcj; ++vpci, ++vpck) {
      PCINSTR branch = *vpci;
      PCINSTR choice = *vpck;
      SPPP::const_iterator sci, scj;
      for (sci = ucs.cbegin(), scj = ucs.cend(); sci != scj; ++sci) {
	if (sci->first == branch && sci->second == choice)
	  continue;
	else
	  break;
      }
      if (sci == scj)
	return true;
    }
    return false;
    */
  }
  // use before "after check" (unsat core gets cleared)
  void InterprocState::trimChoicePool() {
    if (!INC_SOLVE || !SIMP_UNSAT_CORE)
      return;
    if (availChoices == 0)
      return;
    // warning, the backtrack state's solver should be
    // the same as the new state's
    if (solver->getUnsatCore().getUnsatCores().empty()) {
#ifdef FULL_ASSUMPT
      llvm::errs() << "No unsat core, result must be undecided\n";
#endif
      return;
    }
    for (SI si = choicePool.begin(), sj = choicePool.end(); si != sj; ++si) {
      if (!si->visited) {
	const Summary *sum = si->summary;
	if (checkAgainstUnsatCore(sum)) {
	  si->visited = true;
	  availChoices--;
	}
      }
    }
  }
  
}
