#include"cse/Summaxecutor.h"
#include"cse/Util.h"

#include"llvm/Analysis/CFG.h"
#include"llvm/IR/Dominators.h"

namespace cse {

  // const is cancer
  
  bool Summaxecutor::summarise(llvm::Function *f) {
    // killed recursive calls, not well implemented, worry about that later
    if (!f || f->isDeclaration() || Sumbase::isKnown(f))
      return false;
    report("Summarising function " + f->getName().str(), 0x03F0);
    const llvm::Function::BasicBlockListType &bBList = f->getBasicBlockList();
    for (llvm::ilist_iterator<const llvm::BasicBlock>
	   i = bBList.begin(), j = bBList.end();
	 i != j; i++) {
      const llvm::BasicBlock::InstListType &iList = i->getInstList();
      for (llvm::ilist_iterator<const llvm::Instruction>
	     m = iList.begin(), n = iList.end();
	   m != n; m++) {
	summariseInstr(*m);
      }
    }
    report("Finished summarisation", 0x03F0);
    Sumbase::addToKnownFuncs(f);
    // Sumbase::dump();
    summariseBlock(f);
    return true;
  }

  void Summaxecutor::summariseBlock(llvm::Function *f) {
    forwardReasoningImpl(f);
  }
  
  void Summaxecutor::forwardReasoningImpl(llvm::Function *f) {
    /* Prepare the loop information */
    llvm::DominatorTree *dt = new llvm::DominatorTree();
    dt->recalculate(*f);
    llvm::LoopInfoBase<llvm::BasicBlock, llvm::Loop> *loopInfoBase =
      new llvm::LoopInfoBase<llvm::BasicBlock, llvm::Loop>();
    loopInfoBase->releaseMemory();
    // this function is different in the old version llvm
    loopInfoBase->analyze(*dt);

    /* Get the first block */
    const llvm::BasicBlock *parentBB = &(f->getEntryBlock());
    
    /* Iteratively summarise */
    push();
    report("Reasoning about function " + f->getName().str(), 0x03F0);
    setLoopInfo(loopInfoBase);
    friProcessUnit(parentBB);
    report("Finished reasoning", 0x03F0);
    pop();

    delete dt;
  }

  void Summaxecutor::push() {
    callStack.push(StackFrame());
  }
  void Summaxecutor::pop() {
    callStack.pop();
  }

  std::stack<State> *Summaxecutor::workStack() {
    if (callStack.empty())
      // this should never happen
      return NULL;
    else
      return callStack.top().getWorkStack();
  }

  void Summaxecutor::setLoopInfo(llvm::LoopInfoBase<llvm::BasicBlock,
				 llvm::Loop> *lib) {
    assert(!callStack.empty() && "Call stack can never be null");
    callStack.top().setLoopInfo(lib);
  }
  llvm::LoopInfoBase<llvm::BasicBlock, llvm::Loop> *Summaxecutor::loopInfo() {
    if (callStack.empty())
      // this should never happen
      return NULL;
    else
      return callStack.top().getLoopInfo();
  }

  bool Summaxecutor::isLoopHeader(const llvm::BasicBlock *const bb) {
    return loopInfo()->isLoopHeader(bb);
  }
  bool Summaxecutor::contains(const llvm::BasicBlock *bb) {
    for (llvm::LoopInfoBase<llvm::BasicBlock, llvm::Loop>::iterator
	   libi = loopInfo()->begin(), libj = loopInfo()->end();
	 libi != libj; libi++) {
      if ((*libi)->contains(bb))
	return true;
    }
    return false;
  }

  PCINSTR findHead(PCINSTR instr) {
    // getNextNode searches in the parent bb
    while (instr && llvm::isa<llvm::PHINode>(instr))
      instr = instr->getNextNode();
    if (!instr) {
      llvm::outs() << "Warning: instr ptr overflow\n";
      return NULL;
    } else {
      return instr;
    }
  }

  void Summaxecutor::init(State &state) {
    workStack()->push(state);
    // init solver, done when creating new state
  }
  void Summaxecutor::init(State &state, PCINSTR &instr) {
    instr = findHead(instr);
    if (Sumbase::isKnown(instr)) {
      // warning, this might not prevent all repeat summarisation
      // there might be states that start from instr but not registered yet!
      instr = NULL;
      return;
    }
    state.setNextInstr(instr);
    init(state);
  }
  void regNPop(State &state) {
    state.registerSummary();
    // pop the solver to the previous restore point
    // done when getting another state
    // delete state, automatically done
  }

  bool vectorContains(llvm::SmallVectorImpl<llvm::BasicBlock *> &v,
		      const llvm::BasicBlock *bb) {
    // and this is how convenient and easy to use llvm is l & g
    for (llvm::SmallVectorImpl<llvm::BasicBlock *>::iterator
	   vi = v.begin(), vj = v.end(); vi != vj; vi++) {
      if ((*vi) == bb)
	return true;
    }
    return false;
  }
  void printVector(llvm::SmallVectorImpl<llvm::BasicBlock *> &v) {
    llvm::outs() << "Printing vector\n";
    for (llvm::SmallVectorImpl<llvm::BasicBlock *>::iterator
	   vi = v.begin(), vj = v.end(); vi != vj; vi++)
      (*vi)->dump();
  }

  void getLoopLatches(llvm::Loop *loop,
		      llvm::SmallVectorImpl<llvm::BasicBlock *> &v) {
    llvm::SmallVector<llvm::BasicBlock *, 2> loopLatches;
    loop->getLoopLatches(loopLatches);
    for (llvm::SmallVectorImpl<llvm::BasicBlock *>::iterator lli =
	   loopLatches.begin(), llj = loopLatches.end(); lli != llj; ++lli) {
      if (!vectorContains(v, (*lli)))
	v.push_back(*lli);
    }
  }
  void getExitBlocks(llvm::Loop *loop,
		     llvm::SmallVectorImpl<llvm::BasicBlock *> &v) {
    llvm::SmallVector<llvm::BasicBlock *, 1> exitBlocks;
    loop->getExitBlocks(exitBlocks);
    for (llvm::SmallVectorImpl<llvm::BasicBlock *>::iterator ebi =
	   exitBlocks.begin(), ebj = exitBlocks.end(); ebi != ebj; ++ebi) {
      if (!vectorContains(v, (*ebi)))
	v.push_back(*ebi);
    }
  }

  // always increments pc
  bool consumeInstr(State &state, PCINSTR &instr) {
    bool hasSum;
    const Summary *next = Sumbase::getInstrSum(instr);
    // next instr doesn't really matter, not using it anywhere
    PCINSTR nextInstr = instr->getNextNode();
    if (next) {
      // when consuming call instr, next instr is meaningless, so we don't care
      state.consume(next, nextInstr);
      hasSum = true;
    } else {
      report("skipping instruction (probably a call)", 0x0110, true);
      state.consumeEmpty(instr, nextInstr);
      hasSum = false;
    }
    instr = nextInstr;
    return hasSum;
  }

  // ??? random behaviour detected
  void Summaxecutor::friProcessUnit(const llvm::BasicBlock *const first) {
    int itemInScopeCount = 0;
    
    /* Get the first instr */
    // non-phi or phi? for forward reasoning, it should be non-phi
    const llvm::Instruction *instr = first->getFirstNonPHI();
    if (!instr)
      return;

    /* Prepare some loop info */
    bool firstIsLoopHeader = isLoopHeader(first);
    llvm::Loop *currentLoop = NULL;
    llvm::SmallVector<llvm::BasicBlock *, 2> loopLatches;
    llvm::SmallVector<llvm::BasicBlock *, 1> loopExits; // a little redundant
    if (firstIsLoopHeader) {
      currentLoop = loopInfo()->getLoopFor(first);
      // stupid llvm doesn't know the duplicates
      getLoopLatches(currentLoop, loopLatches);
      getExitBlocks(currentLoop, loopExits);
    }
#define NOT_FIRST(P) P!=first

    /* Initialise a state */
    State initState(context);
    init(initState); /* PUSH 1, choice clear */
    itemInScopeCount++;

    /* Work through the states */
    while (itemInScopeCount > 0 && !workStack()->empty()) {
      /* Prepare for another path */
      // we need to directly visit its restore point, unimplemented!
      // don't want assignment to happen
      State &backtrackState = workStack()->top();
      Solver *solver = NULL; // gets solver from the backtrack state
      State state(context, solver);
      bool choiceUnclear = !instr && backtrackState.getAvail() > 0;
      if (choiceUnclear) {
	bool res, useless;
	// I don't like so many copies
	state = backtrackState.visit(res, useless);
	// solver failed
	if (!res) {
	  if (backtrackState.isAllChosen()) {
	    // pop solver, done when getting another state
	    workStack()->pop();
	    assert(itemInScopeCount > 0 && "Visited work item of other scope");
	    itemInScopeCount--;
	  }
	  continue;
	}
	instr = state.getNextInstr();
      } else { // only used by the beginning states (that is, just inited)
	// technically should solver backtrack, but we can omit it
	state.copyFrom(backtrackState);
	// an incomplete check
	assert(state.empty() && "Workstack exception");
	// next instr stored in state should match the current instr
	// or be undefined
	PCINSTR nextInstr = state.getNextInstr();
	if (!instr)
	  instr = nextInstr;
	else if (nextInstr)
	  assert(instr == nextInstr && "Instr not matching");
	assert(instr && "No instruction to proceed");
      }
      if (backtrackState.isAllChosen()) {
	workStack()->pop();
	assert(itemInScopeCount > 0 && "Visited work item of other scope");
	itemInScopeCount--;
      }

      /* The situation when we see a loop */
      const llvm::BasicBlock *parentBB = instr->getParent();
      // when we are at the start of a basic block
      if (instr == &(parentBB->front())
	  || instr == parentBB->getFirstNonPHI()) {
	// see a new loop
	if (isLoopHeader(parentBB) && NOT_FIRST(parentBB)) {
	  regNPop(state);
	  llvm::Loop *loop = loopInfo()->getLoopFor(parentBB);
	  friProcessUnit(parentBB);
	  llvm::SmallVector<llvm::BasicBlock *, 1> exitBlocks;
	  getExitBlocks(loop, exitBlocks);
	  for (llvm::SmallVector<llvm::BasicBlock *, 1>::iterator vi =
		 exitBlocks.begin(), vj = exitBlocks.end(); vi != vj; vi++) {
	    State tempState(context);
	    instr = (*vi)->getFirstNonPHI();
	    assert(instr && "Block don't have a non-phi???");
	    init(tempState, instr); /* PUSH 2, choice clear */
	    itemInScopeCount++;
	  }
	  instr = NULL;
	  continue;
	}
	// or a loop exit
	else if (firstIsLoopHeader && vectorContains(loopExits, parentBB)) {
	  regNPop(state);
	  instr = NULL;
	  continue;
	}
      }

      /* Process all subsequent instrs until */
      PCINSTR tail = state.getVirtualTail();
      bool phiSequenceEnds = false; // phi nodes are always the first in bb
      while (instr && !instr->isTerminator() &&
	     !llvm::isa<const llvm::CallInst>(instr)) {
	// there is a phi node
	const llvm::PHINode *phin;
	if (!phiSequenceEnds && (phin = llvm::dyn_cast
				 <const llvm::PHINode>(instr))) {
	  if (tail) { // and we know its predecessor
	    const llvm::BasicBlock *pred = tail->getParent();
	    const llvm::Value *incoming = phin->getIncomingValueForBlock(pred);
	    // not const expr summarised
	    // never null
	    Summary *sum = summariseEquality(instr, instr, incoming);
	    state.consume(sum, NULL); // next instr is not used anyway
	    delete sum;
	  }
	  // if we don't know the predecessor, we do nothing
	  // leave it to the execution part
	  instr = instr->getNextNode();
	} else {
	  phiSequenceEnds = true;
	  consumeInstr(state, instr); // instr = instr->getNextNode() now
	}
      }

      /* until we see a strange instr */
      if (!instr) {
	regNPop(state);
	instr = NULL;
      } else if (instr->isTerminator()) {
	if (const llvm::BranchInst *bi =
	    llvm::dyn_cast<const llvm::BranchInst>(instr)) {
	  // should equal to the num of sums
	  unsigned int numSucc = bi->getNumSuccessors();
	  if (numSucc == 0) {
	    regNPop(state);
	    instr = NULL;
	    continue;
	  }
	  for (unsigned int i = 0; i < numSucc; i++) {
	    state.addChoice(instr, &(bi->getSuccessor(i)->front()));
	  }
	  
	  // may or may not solve
	  
	  if (numSucc > 1)
	    state.createRestorePoint();
	  // loop latch detector
	  if (firstIsLoopHeader
	      && vectorContains(loopLatches, instr->getParent())) {
	    // assuming that every one of its successor(s) is either
	    // a header or an exit, otherwise we will miss out
	    // summarisation on some code
	    for (unsigned int i = 0; i < numSucc; i++) {
	      bool res, useless;
	      State newState = state.visit(res, useless);
	      // solver failed
	      if (!res)
		// pop solver, done when visit
		continue;
	      regNPop(newState);
	    }
	    // pop solver again, done when getting another state
	  } else {
	    workStack()->push(state); /* PUSH 3, choice unclear */
	    itemInScopeCount++;
	  }
	  instr = NULL;
	} else if (llvm::isa<const llvm::SwitchInst>(instr)) {
	  // we do it differently from br because we are afraid
	  // that corr is not enough to distinguish different paths
	  // sometimes different cases can direct to the same successor
	  Summaries *sums = Sumbase::getInstrSums(instr);
	  // must not be empty
	  for (Summaries::VCI vi = sums->begin(), vj = sums->end();
	       vi != vj; ++vi) {
	    const Summary *sum = *vi;
	    // corr always available
	    // directly add sum, not newing
	    state.addChoice(instr, sum->getCorr(), sum);
	  }
	  
	  // may or may not solve

	  if (sums->size() > 1)
	    state.createRestorePoint();
	  // not detecting loops, assuming handled by br
	  workStack()->push(state); /* PUSH 4, choice unclear */
	  itemInScopeCount++;
	  instr = NULL;
	} else { // unhandled or don't need to do anything about
	  const llvm::TerminatorInst *ti =
	    llvm::cast<const llvm::TerminatorInst>(instr);
	  if (ti->getNumSuccessors() > 0) {
	    // not properly handled
	    // since it's not handled, there should be no constraint
	    state.addChoice(instr, &(ti->getSuccessor(0)->front()));
	    
	    // may or may not solve
	    
	    workStack()->push(state); /* PUSH 5, choice unclear */
	    itemInScopeCount++;
	  } else {
	    regNPop(state);
	  }
	  instr = NULL;
	}
      } else { // instr is a call instr
	PCINSTR callInstr = instr;
	llvm::Function *callee;
	// finish this state
	if (consumeInstr(state, instr)) // instr = instr->getNextNode() now
	  callee = state.getSummary()->getLastCalledFunction(callInstr);
	else
	  callee = NULL;
	regNPop(state);
	// iteratively summarise
	summarise(callee);
	// init a state after the call instr
	if (instr) {
	  state.reset(); // state = State(context)
	  /* PUSH 6, choice clear */
	  init(state, instr); // actually don't have to find head
	  itemInScopeCount++;
	}
      }
    }
  }

}
