#include"cse/VersionedValue.h"

#include"llvm/IR/Value.h"

#include<cmath>

namespace cse {

  // private use
#define INVALID_ID 0
#define ID_START 1000 // have some reserved numbers

  // unique int, but it could change from time to time
  // finalised after the vv is added to a solver
  unsigned int VersionedValue::toInt() const {
    unsigned int id = registerId();
    // map to positive
    unsigned int pv = (version >= 0) ? (version * 2) : (-version * 2 - 1);
    return (id + pv) * (id + pv + 1) / 2 + pv; // Cantor pairing function
  }
  // the inverse of the above function
  bool VersionedValue::getVersion(unsigned int intName,
				  std::pair<unsigned int, int> &res) {
    unsigned int w = unsigned(std::sqrt(8 * intName + 1) - 1) / 2;
    unsigned int t = unsigned(pow(w, 2) + w) / 2;
    unsigned int pv = intName - t;
    unsigned int id = w - pv;
    int v = (pv % 2) ? -((pv + 1) / 2) : pv / 2;
    res.first = id;
    res.second = v;
    // warning, still possible to be an unknown int name
    // because we cannot check the version is known or not
    if (id >= ID_START && id < unique)
      return true;
    else
      return false;
  }
  // unique string, but it could change from time to time
  // finalised after the vv is added to a solver
  std::string VersionedValue::toString() const {
    std::stringstream ss;
    ss << "0x" << std::hex << size_t(value)
       << "_" << std::dec << version;
    // << "@" << memoryVersion; // remove when creating unique name for solvers
    return ss.str();
  }

  unsigned int VersionedValue::unique = ID_START;
  std::map<const llvm::Value *, unsigned int> VersionedValue::ids;

  unsigned int VersionedValue::getId() const {
    std::map<const llvm::Value *, unsigned int>::iterator
      found = ids.find(value);
    if (found == ids.end())
      return INVALID_ID;
    return found->second;
  }
  
  unsigned int VersionedValue::registerId() const {
    std::map<const llvm::Value *, unsigned int>::iterator
      found = ids.find(value);
    if (found == ids.end()) {
      unsigned int id = unique++;
      ids.insert(std::pair<const llvm::Value *, unsigned int>(value, id));
      return id;
    }
    return found->second;
  }

}
