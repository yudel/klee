#include"cse/Common.h"
#include"cse/Solvers.h"

#include<cassert>

namespace cse {

  void STPSolver::init() {
    vc = vc_createValidityChecker();
    // vc_setInterfaceFlags(vc, EXPRDELETE, 0); // maybe
    initConsts();
    // feels weird
    vc_push(vc); // otherwise there will be a "pop empty" error
  }

  void STPSolver::initConsts() {
    falseConst = vc_falseExpr(vc);
  }

  unsigned int STPSolver::createRestorePoint() {
    vc_push(vc);
    return ++scopeLevel;
  }
  void STPSolver::backtrackTo(unsigned int scopeLevel) {
    if (this->scopeLevel < scopeLevel) {
      report("scope level exception", 0x0240);
      return;
    }
    while (this->scopeLevel > scopeLevel) {
      vc_pop(vc);
      this->scopeLevel--;
    }
    // feels weird, additional effort
    vc_pop(vc);
    vc_push(vc);
  }
  
  bool STPSolver::add(const Constraints &ccs) {
    // should we simplify???
    ::Expr expr = getSTPExpr(ccs);
    vc_assertFormula(vc, expr);
    return true;
  }

  /* STP solver doesn't allow the use of multiple instances. More specifically,
     a vc will use some global variables during the checking, destroying, etc.
     This creates segmentation fault/assertion failure when multiple vcs are
     used interleaved. Util STP is fixed, we cannot use this class. Thus this
     class is not working, not even debugged (although they looks fine,
     including the pushs/pops. */
  SolverResult STPSolver::check() {
    // in stp logic, the vc_query function returns context=>expr (implication)
    // you have to prove that !expr is impossible (vc_query(!expr)=0) under
    // the current context, so that you know there must be a solution for expr
    // you can then get the solution as a counter example

    // here we are saying context=>!true, which is equivalent to finding out
    // whether context has a solution
    // note, false=>false returns true, no counter example
    int result = 0; // vc_query(vc, falseConst); // debugging
    switch (result) {
    case 0: break;
    case 1: break;
    case 2:
    case 3:
    default: report("unexpected result", 0x0241);
    }
    bool hasSolution = !result;
    if (hasSolution)
      return SLV_TRUE;
    return SLV_FALSE;
  }
  void STPSolver::getModel() {
    vc_printCounterExample(vc);
  }
  
  void STPSolver::destroy() {
    vc_Destroy(vc);
  }

  void STPSolver::test() {
    VC vc = vc_createValidityChecker();
    VC vc1 = vc_createValidityChecker();

    vc_Destroy(vc);
    vc_Destroy(vc1);

    // vcs are not independent on each other!

    /*
    ::Expr x = vc_varExpr(vc, "x", vc_bvType(vc, 8));
    ::Expr const0 = vc_bvConstExprFromInt(vc, 8, 0);
    ::Expr constMinus10 = vc_bvUMinusExpr(vc, vc_bvConstExprFromInt(vc, 8, 10));
    // ::Expr constMinus1 = vc_bvUMinusExpr(vc, vc_bvConstExprFromInt(vc, 8, 9))nn;
    // ::Expr const1 = vc_bvConstExprFromInt(vc, 8, 1);
    ::Expr gt = vc_sbvGtExpr(vc, x, constMinus10);
    ::Expr lt = vc_sbvLtExpr(vc, x, const0);
    // vc_assertFormula(vc, gt);
    vc_assertFormula(vc, lt);
    vc_assertFormula(vc, gt);

    // ::Expr falseExpr = vc_falseExpr(vc);

    // vc_assertFormula(vc, falseExpr);

    vc_printAsserts(vc, 0);
    int result = vc_query(vc, vc_falseExpr(vc));
    vc_printQuery(vc);

    vc_printCounterExample(vc);
    
    ::Expr xCE = vc_getCounterExample(vc, x);
    llvm::outs() << "ce " << getBVInt(xCE) << "\n";

    llvm::outs() << "result " << result << "\n";
    */
  }

  ::Expr STPSolver::getSTPExpr(const Constraints &ccs) {
    return vc_trueExpr(vc);
  }
  
}
