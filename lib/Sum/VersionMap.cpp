#include"cse/VersionMap.h"

namespace cse {

  // understand before using it
  void VersionMap::setPre(const llvm::Value *value,
			  int &vvVersion, bool lifted,
			  int &vvMemoryVersion) {
    int l = lifted ? -1 : 0;
    MI i = vMap.find(value);
    if (i == vMap.end()) {
      Version *version = new Version(0, l);
      vMap.insert(std::pair<const llvm::Value *, Version>(value, *version));
      vvVersion = 0;
    } else {
      int oldLowest = i->second.lowest;
      i->second.lowest = oldLowest + l;
      vvVersion = oldLowest;
    }
    vvMemoryVersion += memoryVersion.lowest;
  }
  void VersionMap::setPost(const llvm::Value *value,
			   int &vvVersion, bool lifted,
			   int &vvMemoryVersion) {
    int l = lifted ? 1 : 0;
    MI i = vMap.find(value);
    if (i == vMap.end()) {
      Version *version = new Version(l, 0);
      vMap.insert(std::pair<const llvm::Value *, Version>(value, *version));
      vvVersion = l;
    } else {
      int highest = i->second.highest + l;
      i->second.highest = highest;
      vvVersion = highest;
    }
    vvMemoryVersion += memoryVersion.highest;
  }
  
  void VersionMap::setPre(const VersionMap &another) {
    MI end = vMap.end();
    for (MCI mi = another.begin(), mj = another.end(); mi != mj; mi++) {
      int difference = mi->second.highest - mi->second.lowest;
      MI i = vMap.find(mi->first);
      if (i == end) {
	Version *version = new Version(0, -difference);
	vMap.insert(std::pair<const llvm::Value *, Version>
		    (mi->first, *version));
      } else {
	i->second.lowest -= difference;
      }
    }
    memoryVersion.lowest -= another.getMemoryDifference();
  }
  void VersionMap::setPost(const VersionMap &another) {
    MI end = vMap.end();
    for (MCI mi = another.begin(), mj = another.end(); mi != mj; mi++) {
      int difference = mi->second.highest - mi->second.lowest;
      MI i = vMap.find(mi->first);
      if (i == end) {
	Version *version = new Version(difference, 0);
	vMap.insert(std::pair<const llvm::Value *, Version>
		    (mi->first, *version));
      } else {
	i->second.highest += difference;
      }
    }
    memoryVersion.highest += another.getMemoryDifference();
  }
  
  void VersionMap::setPreserving(const llvm::Value *value,
				 int vvVersion, bool lifted,
				 int vvMemoryVersion) {
    int l = vvVersion + (lifted ? -1 : 0);
    MI i = vMap.find(value);
    if (i == vMap.end()) {
      Version *version = new Version(vvVersion, l);
      vMap.insert(std::pair<const llvm::Value *, Version>(value, *version));
    } else {
      if (l < i->second.lowest)
	i->second.lowest = l;
      if (vvVersion > i->second.highest)
	i->second.highest = vvVersion;
    }
    if (vvMemoryVersion < memoryVersion.lowest)
      memoryVersion.lowest = vvMemoryVersion;
    if (vvMemoryVersion > memoryVersion.highest)
      memoryVersion.highest = vvMemoryVersion;
  }
  
}
