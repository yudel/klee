#include"cse/Counter.h"

namespace cse {

  void Counter::reset(int no) {
    rs[no] = 0;
  }
  
  void Counter::inc(int no) {
    rs[no]++;
  }

  void Counter::dec(int no) {
    if (rs[no]) {
      rs[no]--;
    }
  }

  unsigned int Counter::get(int no) {
    return rs[no];
  }

  unsigned int Counter::rs[N_RECS] = {}; // init to 0

}
