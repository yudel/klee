#include"cse/Common.h"
#include"cse/Counter.h"
#include"cse/Solvers.h"
#include"cse/Statistics.h"
#include"cse/Timer.h"
#include"cse/Util.h"

#include"llvm/ADT/SmallVector.h"

#include<fstream>

namespace cse {

  // adding the solver interface slightly but noticably increase
  // the time consumption even without real constraints, just
  // pushing and popping "true"
  // didn't notice this in stp
  
  void Z3Solver::init() {
    Z3_config cfg;
    cfg = Z3_mk_config();
    // config not set
    Z3_set_param_value(cfg, "model", "true");
    // Z3_set_param_value(cfg, "proof", "true");
    if (SIMP_UNSAT_CORE)
      Z3_set_param_value(cfg, "unsat_core", "true"); // could be slow
    // check out other parameters
    if (SOLVER_TIMEOUT) {
      std::stringstream ss;
      ss << (SOLVER_TIMEOUT * 1000); // in seconds
      Z3_set_param_value(cfg, "timeout", ss.str().c_str());
    }
    ctx = Z3_mk_context(cfg);
    Z3_del_config(cfg);
    // This is not how you use solver tactics.
    bool SOLVER_TACTIC = !INC_SOLVE; // hack, inc solve shouldn't be here
    if (SOLVER_TACTIC) {
      // tactics are not compatible with inc
      // these are not good, try better tactics
      /*
      llvm::outs() << "\nAvailable tactics:\n";
      unsigned int num = Z3_get_num_tactics(ctx);
      for (unsigned int i = 0; i < num; i++)
	llvm::outs() << Z3_get_tactic_name(ctx, i) << "\n";
      */
      Z3_tactic simplify = Z3_mk_tactic(ctx, "simplify");
      Z3_tactic_inc_ref(ctx, simplify);
      Z3_tactic smt = Z3_mk_tactic(ctx, "smt");
      Z3_tactic_inc_ref(ctx, smt);
      Z3_tactic then = Z3_tactic_and_then(ctx, simplify, smt);
      Z3_tactic_inc_ref(ctx, then);
      solver = Z3_mk_solver_from_tactic(ctx, then); // seems to be non-inc
      Z3_tactic_dec_ref(ctx, simplify); // delete now?
      Z3_tactic_dec_ref(ctx, smt);
      Z3_tactic_dec_ref(ctx, then);
    } else if (!L_QF_ABV /* SOLVER_LOGIC.empty() */) {
      // According to the discussion online, mk solver will have a
      // preprocessing procedure that decides what kind of solver
      // will be used (in my understanding, decides the logics and hence
      // tactics that constitute the solver). And the mk simple solver
      // will directly use the incremental solver (smt tactic) without
      // preprocessing. If there are push/pops etc., the two functions are
      // almost "exactly" alike. In my experiment, they are significantly
      // different.
      // This option decides if no tatic or logic provided, what solver
      // do we use, ideally it should switch between inc and non-inc solvers,
      // but I don't know how to ensure Z3 to use non inc solver, so I hack
      // the solver solver tactics, see above.
      // In non-inc mode, push and pops are only means of managing, no
      // learning should be expected.
      if (INC_SOLVE)
	solver = Z3_mk_simple_solver(ctx); // incremental solver
      else
	solver = Z3_mk_solver(ctx);
    } else {
      Z3_symbol logic = Z3_mk_string_symbol(ctx, "QF_ABV");
      // Z3_symbol logic = Z3_mk_string_symbol(ctx, SOLVER_LOGIC.c_str());
      solver = Z3_mk_solver_for_logic(ctx, logic); // could be slightly faster
    }
    Z3_solver_inc_ref(ctx, solver);
    modelAvail = false;
    createRestorePoint(); // a bit redundant

    useAssumption = false;
    sum = NULL;
  }

  unsigned int Z3Solver::createRestorePoint() {
    modelAvail = false;

    Z3_solver_push(ctx, solver);
    // default 0, coincide with the inited 0
    scopeLevel = Z3_solver_get_num_scopes(ctx, solver);
    // 1 after first push
    return scopeLevel;
  }
  void Z3Solver::backtrackTo(unsigned int scopeLevel) {
    modelAvail = false;

    if (scopeLevel == 0)
      report("backtracking to scope 0, shouldn't have happened", 0x0152, true);
    int diff = this->scopeLevel - scopeLevel;
    if (diff < 0) {
      report("scope level exception", 0x0250);
      return;
    }
    diff++;
    Z3_solver_pop(ctx, solver, diff);
    Z3_solver_push(ctx, solver);
    this->scopeLevel = Z3_solver_get_num_scopes(ctx, solver);
    if (MEM_CHECK)
      mc.backtrackTo(scopeLevel);
  }

  void Z3Solver::prepare(const Summary *sum) {
    // indirectly control the use of assumptions/unsat cores
    if (!INC_SOLVE || !SIMP_UNSAT_CORE)
      return;
    
    useAssumption = true;
    this->sum = sum;
    uc.addKnowledge(sum);
  }
  bool Z3Solver::add(const Constraints &ccs) {
    modelAvail = false; // ccs could be empty, but still set false by def
    for (Constraints::const_iterator ccsci = ccs.begin(), ccscj = ccs.end();
	 ccsci != ccscj; ++ccsci) {
      if (!add(**ccsci))
	return false;
    }
    return true;
  }

  // quick assumption checker, turns out this is not quick
  Z3_lbool Z3Solver::checkAssumptions() {
    bool headOrTail = true;
    for (MUI2AR::const_iterator ci = uc.arsBegin(), cj = uc.arsEnd();
	 ci != cj;) {
      MUI2AR::const_iterator current = headOrTail ? ci : cj;
      // must be in inc mode already
      Z3_solver_push(ctx, solver);
      modelAvail = false; // unnecessary
      Z3_ast assumption = current->second.assumption;
      Z3_solver_assert(ctx, solver, assumption);
      Counter::inc(3); // solver count (all)
      Counter::inc(6); // solver count (extra)
      Timer::begin(1); // solver timer
      Timer::reset(2);
      Timer::begin(2); // categorised solver timer
      Z3_lbool result = Z3_solver_check(ctx, solver);
      Timer::end(2);
      Statistics::categoriseTime(Timer::get(2));
      Timer::pause(1);
      Z3_solver_pop(ctx, solver, 1);
      modelAvail = false; // unnecessary
      switch (result) {
      case Z3_L_FALSE: {
	// structure-wise uniqueness, guess should be right for us
	unsigned int hash = Z3_get_ast_id(ctx, assumption);
	uc.addUnsatCore(hash);
	Timer::accumulate(2, 3);
	return result;
      }
      case Z3_L_UNDEF:
	Counter::inc(7); // check result undef count
	Timer::accumulate(2, 3);
	break;
      case Z3_L_TRUE:
	Timer::accumulate(2, 4);
	if (headOrTail)
	  --cj;
	else
	  ++ci;
	headOrTail = !headOrTail;
	continue;
      default:
	Timer::accumulate(2, 3);
	break;
      }
    }
    Timer::reset(2);
    return Z3_L_UNDEF; // this stands for "I quit"
  }
  Z3_lbool Z3Solver::check(bool assumptionEnabled) {
    /*
    std::ofstream file;
    file.open("solver-to-recheck", std::ios::app);
    file << Z3_solver_to_string(ctx, solver) << "\n" // to string..
	 << "(check-sat)\n"
	 << "(reset)\n";
    file.close();
    */
    Counter::inc(3); // solver count (all)
    Timer::begin(1); // solver timer
    Timer::reset(2);
    Timer::begin(2); // categorised solver timer
    Z3_lbool result;
    if (assumptionEnabled) {
      Counter::inc(6); // solver count (extra)
      result = Z3_solver_check_assumptions
	(ctx, solver, uc.getNumOfAssumptions(), uc.getAssumptions());
    } else {
      result = Z3_solver_check(ctx, solver);
    }
    Timer::end(2);
    Statistics::categoriseTime(Timer::get(2));
    Timer::pause(1);
    return result;
  }
  SolverResult Z3Solver::check() {
    modelAvail = false;
    Z3_lbool result;
    bool assumptionEnabled = useAssumption && uc.hasAssumption();
    if (assumptionEnabled) {
#ifdef FULL_ASSUMPT
      // unless we are doing exp or one day the solvers are good enough,
      // we will never use this, it's just slow
      result = check(true);
#else
      // must be in inc mode already
      Z3_solver_push(ctx, solver);
      modelAvail = false; // unnecessary
      uc.assertAssumptions(ctx, solver);
      result = check(false);
      switch (result) {
      case Z3_L_FALSE:
	Z3_solver_pop(ctx, solver, 1);
	modelAvail = false; // unnecessary
	Timer::accumulate(2, 3);
	// magic number
	if (uc.getNumOfAssumptions() < ASSUMPT_LIMIT)
	  return SLV_FALSE;
	result = QUICK_ASSUMPT ? checkAssumptions() : check(true);
	assert((result == Z3_L_UNDEF || result == Z3_L_FALSE)
	       && "Unexpected solver result");
	if (result == Z3_L_UNDEF) {
	  Counter::inc(7); // check result undef count
	  Timer::accumulate(2, 3);
	  return SLV_FALSE;
	}
	break;
      case Z3_L_UNDEF:
      case Z3_L_TRUE:
      default:
	// all handled later
	break;
      }
#endif
    } else {
      result = check(false);
    }
    switch (result) {
    case Z3_L_FALSE: {
      if (assumptionEnabled && !QUICK_ASSUMPT) {
	// seems not always the best cores, bug or?
	Z3_ast_vector unsatCores = Z3_solver_get_unsat_core(ctx, solver);
	Z3_ast_vector_inc_ref(ctx, unsatCores);
	unsigned int num = Z3_ast_vector_size(ctx, unsatCores);
	assert(num && "Cannot produce unsat core"); // 1 bug not fixed
	for (unsigned int i = 0; i < num; i++) {
	  Z3_ast unsatCore = Z3_ast_vector_get(ctx, unsatCores, i);
	  // structure-wise uniqueness, guess should be right for us
	  unsigned int hash = Z3_get_ast_id(ctx, unsatCore);
	  uc.addUnsatCore(hash);
	}
	// Z3_ast_vector_dec_ref(ctx, unsatCores); wtf?
      }
      Timer::accumulate(2, 3);
      return SLV_FALSE;
    }
    case Z3_L_UNDEF:
      /*
       * Many interpretations:
       * 1. If there are quantifiers in the formulas,
       *    it means there are potential solutions
       * 2. Real arithmetics, nonlinear constraints, etc. They are
       *    undecidable? This could be an undefined behaviour, which
       *    means if you run it again or run by commandline,
       *    it may disappear. Shouldn't have happended
       * 3. Solver time out
       * 4. Something we don't care happened
       */
      llvm::errs() << "Solver cannot decide:\n  "
		   << Z3_solver_get_reason_unknown(ctx, solver) << "\n";
      Counter::inc(7); // check result undef count
      Timer::accumulate(2, 3);
      return SLV_UNDEF;
    case Z3_L_TRUE:
      Timer::accumulate(2, 4);
      modelAvail = true;
      return SLV_TRUE;
    default:
      report("unexpected result", 0x0251);
      Timer::accumulate(2, 3);
      return SLV_UNDEF;
    }
  }
  void Z3Solver::afterCheck(bool asserting) {
    if (!INC_SOLVE || !SIMP_UNSAT_CORE)
      return;
    
    if (asserting)
      uc.assertAssumptions(ctx, solver);
    useAssumption = false;
    this->sum = NULL;
    uc.clear();
  }
  void Z3Solver::getModel() {
    if (model) { // the old model
      Z3_model_dec_ref(ctx, model);
      model = 0;
    }
    if (modelAvail) {
      model = Z3_solver_get_model(ctx, solver);
      Z3_model_inc_ref(ctx, model);
    } else {
      Counter::inc(5); // solver count (misc)
      // changes to assertions after last check
      if (check() == SLV_TRUE) {
	// this case is expected, a little ugly
	report("checked to get model", 0x0050);
	model = Z3_solver_get_model(ctx, solver);
	Z3_model_inc_ref(ctx, model);
      } else {
	report("model not available", 0x025b);
	model = 0;
      }
    }
  }

  // slightly noticably time consuming
  // watch out for z3 to string problems
  void Z3Solver::display() {
    if (!model) {
      llvm::errs() << "Model doesn't exist\n";
      return;
    }
    llvm::outs() << "Inputs:\n";
    inputModelFuncs = new std::set<std::string>();
    int numConsts = Z3_model_get_num_consts(ctx, model);
    // display constants
    // assuming all our inputs are represented by constants, works fine for now
    for (int i = 0; i < numConsts; i++) {
      Z3_func_decl constDecl = Z3_model_get_const_decl(ctx, model, i);
      Z3_symbol name = Z3_get_decl_name(ctx, constDecl);
      Z3_symbol_kind kind = Z3_get_symbol_kind(ctx, name);
      Z3_string nameStr = Z3_get_symbol_string(ctx, name);
      bool isLowest;
      // warning, assuming 0 is the lowest version, works fine for now
      if (kind == Z3_INT_SYMBOL) {
	int nameInt = Z3_get_symbol_int(ctx, name);
	std::pair<unsigned int, int> res;
	if (VersionedValue::getVersion(nameInt, res))
	  isLowest = (res.second == 0);
	else
	  isLowest = false; // may be const added by Z3
      } else {
	int len = std::strlen(nameStr);
	isLowest = !std::strcmp(&(nameStr[len-2]), "_0");
      }
      if (isLowest) {
	llvm::outs() << nameStr << "=";
	Z3_ast constant = Z3_model_get_const_interp(ctx, model, constDecl);
	display(constant);
	llvm::outs() << "\n";
      }
    }
    // then functions
    // flat functions are essentially the same as constants
    // but are they in the same format? are constants in here?
    int numFuncs = Z3_model_get_num_funcs(ctx, model);
    for (int i = 0; i < numFuncs; i++) {
      Z3_func_decl funcDecl = Z3_model_get_func_decl(ctx, model, i);
      Z3_symbol name = Z3_get_decl_name(ctx, funcDecl);
      Z3_string nameStr = Z3_get_symbol_string(ctx, name);
      if (inputModelFuncs->find(std::string(nameStr)) !=
	  inputModelFuncs->end()) { // might not have displayed all needed
	llvm::outs() << nameStr << "=";
	display(funcDecl);
	llvm::outs() << "\n";
      }
    }
    delete inputModelFuncs;
    llvm::outs() << "End of inputs\n\n";
  }

  void Z3Solver::dump() {
    llvm::outs() << "Solver:\n"
		 << Z3_solver_to_string(ctx, solver)
		 << "\nEnd of solver\n"; // don't use it!
  }

  /*
   * Convenient functions for making z3 objects
   */
#define REAL_STR "real"
#define BV_STR "bv"
  std::string sort_to_string(Z3_context ctx, Z3_sort sort) {
    switch (Z3_get_sort_kind(ctx, sort)) {
    case Z3_REAL_SORT:
      return std::string(REAL_STR);
    case Z3_BV_SORT:
      return std::string(BV_STR);
    default:
      report("unknown sort to string", 0x0259);
      return std::string("");
    }
  }
  // in case z3 won't copy it, we make a copy
  // actually z3 will copy it, so you don't have to do this
  // (don't use it)
  // edit: i doubt that, i still need to use it
  // once a string obj is deleted, its char * goes to hell with it
  const char *mk_name(const std::string &str) {
    unsigned int len = str.length();
    char *name = new char[len + 1];
    const char *fromStr = str.c_str();
    std::copy(fromStr, &fromStr[len + 1], name);
    return name;
  }
  // they call a const associated with a symbol (as its name) and a type
  // (also called "sort"), a "variable"
  // so in constraint solving, a variable is essentially a constant
  // or a function not yet assigned a model (after you solve you can
  // model)
  // and a constant is essentially a function without an argument, which
  // makes it the same value everywhere
  Z3_ast mk_var(Z3_context ctx, int intName, Z3_sort ty) {
    Z3_symbol s = Z3_mk_int_symbol(ctx, intName);
    return Z3_mk_const(ctx, s, ty);
  }
  Z3_ast mk_var(Z3_context ctx, const char *name, Z3_sort ty) {
    Z3_symbol s = Z3_mk_string_symbol(ctx, name);
    return Z3_mk_const(ctx, s, ty);
  }
  Z3_ast mk_bool_var(Z3_context ctx, int intName) {
    // in llvm bool is bv with 1 bit, we don't need this for that
    Z3_sort ty = Z3_mk_bool_sort(ctx);
    return mk_var(ctx, intName, ty);
  }
  Z3_ast mk_bool_var(Z3_context ctx, const char *name) {
    // in llvm bool is bv with 1 bit, we don't need this for that
    Z3_sort ty = Z3_mk_bool_sort(ctx);
    return mk_var(ctx, name, ty);
  }
  Z3_ast mk_int_var(Z3_context ctx, int name /* const char *name */) {
    // this int sort doesn't have a length, so we don't like this one
    Z3_sort ty = Z3_mk_int_sort(ctx);
    return mk_var(ctx, name, ty);
  }
  Z3_ast mk_bv_var(Z3_context ctx, int name /* const char *name */,
		   unsigned int bitWidth) {
    Z3_sort ty = Z3_mk_bv_sort(ctx, bitWidth);
    return mk_var(ctx, name, ty);
  }
  Z3_ast mk_real_var(Z3_context ctx, int name /* const char *name */) {
    Z3_sort ty = Z3_mk_real_sort(ctx);
    return mk_var(ctx, name, ty);
  }
  // phisically, different types of data share the same piece of memory,
  // however, an array in Z3 is strictly typed and to avoid conversion
  // constraints (they are having problems btw, according to Z3 doc),
  // we use different memory arrays for different types of data
  // floating point not implemented
  // this make a important but invisible (for most of the time)
  // difference between the model and the reality
  Z3_sort mk_memory_array_sort(Z3_context ctx,
			       unsigned int ptrSize,
			       // intended to hold all kinds of data if bv
			       // should be big enough like 64
			       Z3_sort cellTy) {
    Z3_sort ptrTy = Z3_mk_bv_sort(ctx, ptrSize);
    // theoretically every memory cell should be 1 bit
    // now suppose you store a 4-bit bv to ptr, this is what happens:
    // [your stuff] [nothing] [nothing] [nothing] [your other stuff] ...
    //  ^            ^---------^---------^         ^
    // ptr             empty, never used         ptr+4
    // while theoretically:
    // [your stuff bit 1] [bit 2] [bit 3] [bit 4, your stuff ends] [other] ...
    //  ^                                                           ^
    // ptr                                                        ptr+4
    return Z3_mk_array_sort(ctx, ptrTy, cellTy);
  }
  Z3_ast mk_memory_array_var(Z3_context ctx, const char *name,
			     unsigned int ptrSize, Z3_sort cellTy) {
    Z3_sort ty = mk_memory_array_sort(ctx, ptrSize, cellTy);
    return mk_var(ctx, name, ty);
  }

  // representing constant, here "constant" means the constant in the
  // program, not the z3 constant
  // our constant' value is immediately clear without getting model
  Z3_ast mk_int(Z3_context ctx, int v) {
    Z3_sort ty = Z3_mk_int_sort(ctx);
    return Z3_mk_int(ctx, v, ty);
  }
  Z3_ast mk_bv(Z3_context ctx, int64_t v, unsigned int bitWidth) {
    Z3_sort ty = Z3_mk_bv_sort(ctx, bitWidth);
    // my guess is that the bitWidth is the bv's logical width,
    // and the int64_t value is just a container to hold a value to
    // create this bv
    return Z3_mk_int64(ctx, v, ty);
  }
  // this specialised type of bv is used to handle pointer arithmetics
  Z3_ast mk_ptr_bv(Z3_context ctx, uint64_t v, unsigned int ptrSize) {
    Z3_sort ty = Z3_mk_bv_sort(ctx, ptrSize);
    return Z3_mk_unsigned_int64(ctx, v, ty);
  }
  // make real number using string representation
  Z3_ast mk_real(Z3_context ctx, const char *numeral) {
    Z3_sort ty = Z3_mk_real_sort(ctx);
    return Z3_mk_numeral(ctx, numeral, ty);
  }

  // for address computation (unsigned of course)
  // it cannot overflow, otherwise out of memory?
  Z3_ast mk_bvadd_no_overflow(Z3_context ctx, Z3_solver solver,
			      Z3_ast op1, Z3_ast op2) {
    Z3_ast noOf = Z3_mk_bvadd_no_overflow(ctx, op1, op2, false);
    Z3_solver_assert(ctx, solver, noOf);
    return Z3_mk_bvadd(ctx, op1, op2);
  }
  Z3_ast mk_bvmul_no_overflow(Z3_context ctx, Z3_solver solver,
			      Z3_ast op1, Z3_ast op2) {
    Z3_ast noOf = Z3_mk_bvmul_no_overflow(ctx, op1, op2, false);
    Z3_solver_assert(ctx, solver, noOf);
    return Z3_mk_bvmul(ctx, op1, op2);
  }
  /* Convenients end */

  // static
  void Z3Solver::test() {
    /*
    Z3Solver *theSolver = (Z3Solver *) Solver::create();
    Z3_context ctx = theSolver->ctx;
    Z3_solver solver = theSolver->solver;

    Z3_ast pa = mk_int_var(ctx, "pa");
    Z3_ast paeq0 = Z3_mk_eq(ctx, pa, mk_int(ctx, 0));
    Z3_ast pb = mk_int_var(ctx, "pb");
    Z3_ast pbeqpa = Z3_mk_eq(ctx, pb, pa);
    Z3_ast pbeq1 = Z3_mk_eq(ctx, pb, mk_int(ctx, 1));
    Z3_ast p1 = mk_bool_var(ctx, "P1");
    // Z3_ast c1[2] = { p1, paeq0 };
    Z3_solver_assert(ctx, solver, Z3_mk_eq(ctx, p1, paeq0));
    Z3_ast p2 = mk_bool_var(ctx, "P2");
    // Z3_ast c2[2] = { p2, pbeqpa };
    Z3_solver_assert(ctx, solver, Z3_mk_eq(ctx, p2, pbeqpa));
    Z3_ast p3 = mk_bool_var(ctx, "P3");
    // Z3_ast c3[2] = { p3, pbeq1 };
    Z3_solver_assert(ctx, solver, Z3_mk_eq(ctx, p3, pbeq1));
    Z3_ast assumptions[3] = { p1, p2, p3 };
    
    Z3_lbool result;
    Z3_ast proof;
    Z3_ast_vector core;

    result = Z3_solver_check_assumptions(ctx, solver, 3, assumptions);

    switch (result) {
    case Z3_L_FALSE:
      core = Z3_solver_get_unsat_core(ctx, solver);
      proof = Z3_solver_get_proof(ctx, solver);
      llvm::outs() << "proof:\n" << Z3_ast_to_string(ctx, proof) << "\n";
      llvm::outs() << "unsat core:\n";
      for (unsigned int i = 0; i < Z3_ast_vector_size(ctx, core); ++i)
	llvm::outs() << Z3_ast_to_string(ctx, Z3_ast_vector_get(ctx, core, i));
      llvm::outs() << "\n";
      break;
    case Z3_L_UNDEF:
      break;
    case Z3_L_TRUE:
      break;
    default:
      ;
    }

    delete theSolver;
    */
  }

  int getIntName(const VersionedValue &vv) {
    return int(vv.toInt());
  }
  // never ever use char * again! stupid language, almost costs me an afternoon
  const char *getName(const VersionedValue &vv) {
    return mk_name(vv.toString());
  }
  const char *getMAName(std::string sortStr, int memoryVersion) {
    std::stringstream ss;
    ss << "mem_" << sortStr << "_" << memoryVersion;
    return mk_name(ss.str());
  }
  const char *getMAName(const VersionedValue &vv,
			std::string sortStr, int mod) {
    return getMAName(sortStr, vv.getMemoryVersion() + mod);
  }

  unsigned int getBVWidth(const llvm::Value *v) {
    const llvm::Type *type = v->getType();
    if (const llvm::IntegerType *intType =
	llvm::dyn_cast<const llvm::IntegerType>(type))
      return intType->getBitWidth();
    else
      report("bv type check failed", 0x0254);
    return 0;
  }
  void checkBVWidth(const llvm::Value *v, unsigned int bitWidth) {
    assert(v->getType()->isIntegerTy(bitWidth));
  }

  Z3_ast evaluateBool(Z3_context ctx,
		      const VersionedValue &vv) {
    const llvm::Value *v = vv.get();
    checkBVWidth(v, 1);
    if (const llvm::ConstantInt *constBool =
	llvm::dyn_cast<const llvm::ConstantInt>(v)) {
      if (constBool->isZero())
	return mk_bv(ctx, 0, 1);
      else
	return mk_bv(ctx, 1, 1);
    } else
      return mk_bv_var(ctx, getIntName(vv), 1);
  }
  Z3_ast evaluateBV(Z3_context ctx,
		    const VersionedValue &vv, unsigned int bitWidth) {
    // no check width
    if (const llvm::ConstantInt *constInt =
	llvm::dyn_cast<const llvm::ConstantInt>(vv.get()))
      // gives constant int as int64_t
      // we don't have enough info to decide whether it should be
      // signed or unsigned, but it shouldn't matter, as we only care
      // about the bits under bitWidth
      return mk_bv(ctx, constInt->getSExtValue(), bitWidth);
    else
      return mk_bv_var(ctx, getIntName(vv), bitWidth);
  }
  // this specialised type of bv is used to handle pointer arithmetics
  Z3_ast evaluatePtrBV(Z3_context ctx, bool &isConstZero,
		       const VersionedValue &vv, unsigned int ptrSize) {
    isConstZero = false;
    const llvm::Value *v = vv.get();
    unsigned int bitWidth = getBVWidth(v);
    if (const llvm::ConstantInt *constInt =
	llvm::dyn_cast<const llvm::ConstantInt>(v)) {
      int64_t cize = constInt->getZExtValue();
      if (!cize)
	isConstZero = true;
      return mk_ptr_bv(ctx, cize, ptrSize);
    } else {
      int toZExt = ptrSize - bitWidth;
      Z3_ast bv = mk_bv_var(ctx, getIntName(vv), bitWidth);
      if (toZExt > 0)
	return Z3_mk_zero_ext(ctx, toZExt, bv);
      else if (toZExt < 0)
	return Z3_mk_extract(ctx, ptrSize - 1, 0, bv);
      else
	return bv;
    }
  }
#define FP_NUMERAL_LEN 10 // it's actually usually pretty long like 20
  Z3_ast evaluateFP(Z3_context ctx, const VersionedValue &vv) {
    const llvm::Value *v = vv.get();
    assert(v->getType()->isFloatingPointTy());
    if (const llvm::ConstantFP *constFP =
	llvm::dyn_cast<const llvm::ConstantFP>(v)) {
      llvm::SmallVector<char, FP_NUMERAL_LEN> charVector;
      constFP->getValueAPF().toString(charVector);
      // feel so ancient... string not deleted
      std::unique_ptr<char[]> numeral(new char[charVector.size() + 1]);
      int i = 0;
      for (llvm::SmallVector<char, FP_NUMERAL_LEN>::iterator
	     vi = charVector.begin(), vj = charVector.end(); vi != vj; ++vi) {
	numeral[i++] = *vi;
      }
      numeral[i] = '\0';
      return mk_real(ctx, numeral.get());
    } else {
      return mk_real_var(ctx, getIntName(vv));
    }
  }
  // throughout the whole model, we don't care what is the type of data
  // a pointer points to, we just want to know it's a pointer
  Z3_ast evaluatePtr(Z3_context ctx,
		     const VersionedValue &vv, unsigned int ptrSize) {
    const llvm::Value *v = vv.get();
    assert(v->getType()->isPointerTy());
    // pointer cannot be constant, otherwise null
    if (llvm::isa<const llvm::ConstantPointerNull>(v))
      return mk_ptr_bv(ctx, 0, ptrSize);
    else
      return mk_bv_var(ctx, getIntName(vv), ptrSize);
  }
  Z3_ast evaluateUnknown(Z3_context ctx,
			 const VersionedValue &vv, unsigned int ptrSize) {
    const llvm::Value *v = vv.get();
    llvm::Type *type = v->getType();
    // note in load operation this shouldn't be const, who cares
    if (type->isIntegerTy()) {
      return evaluateBV(ctx, vv, getBVWidth(v));
    } else if (type->isFloatingPointTy()) {
      return evaluateFP(ctx, vv);
    } else if (type->isPointerTy()) {
      return evaluatePtr(ctx, vv, ptrSize);
    } else if (type->isMetadataTy()) { // we don't care about this
      report("unhandled metadata type", 0x0151, true);
      return mk_int(ctx, 0);
    } else {
      report("unhandled type", 0x0256); // should aggregated type be allowed?
      // return whatever
      return mk_int(ctx, 0);
    }
  }
  // convert bool to bv 1
  Z3_ast convert(Z3_context ctx, Z3_ast ofBool) {
    Z3_ast trueBV = mk_bv(ctx, 1, 1);
    Z3_ast falseBV = mk_bv(ctx, 0, 1);
    return Z3_mk_ite(ctx, ofBool, trueBV, falseBV);
  }
  
  Z3_ast convert(Z3_context ctx, Z3_ast from, Z3_sort toSort,
		 bool warn = false) {
    Z3_sort fromSort = Z3_get_sort(ctx, from);
    if (Z3_get_sort_kind(ctx, fromSort) == Z3_BV_SORT) {
      if (Z3_get_sort_kind(ctx, toSort) == Z3_BV_SORT) {
	unsigned int oldSize = Z3_get_bv_sort_size(ctx, fromSort);
	unsigned int newSize = Z3_get_bv_sort_size(ctx, toSort);
	// using zext
	// conversion in/out of mem array, check
	// ptr/int or int/ptr cast, check
	// bitcasting, size the same, check
	// be careful when doing other kind of conversion
	if (newSize > oldSize) {
	  return Z3_mk_zero_ext(ctx, newSize - oldSize, from);
	} else if (newSize < oldSize) {
	  if (warn)
	    report("bv downsized", 0x0150);
	  return Z3_mk_extract(ctx, newSize - 1, 0, from);
	} else {
	  return from; // warning, return self
	}
      }
    } else if (Z3_get_sort_kind(ctx, fromSort) == Z3_REAL_SORT) {
      if (Z3_get_sort_kind(ctx, toSort) == Z3_REAL_SORT) {
	return from; // warning, return self
      }
    } else if (Z3_get_sort_kind(ctx, fromSort) == Z3_INT_SORT) {
      if (Z3_get_sort_kind(ctx, toSort) == Z3_INT_SORT) {
	return from; // warning, return self
      }
    }
    llvm::outs() << "from sort " << Z3_sort_to_string(ctx, fromSort)
		 << " to sort " << Z3_sort_to_string(ctx, toSort) << "\n";
    report("conversion failed", 0x0257);
    return from;
  }

  /* other kinds of constants unhandled, e.g., constant expr */
  /* what about global values? how are they constant? funny hierarchy */

  void Z3Solver::prt(Z3_ast ast) {
    // don't use ast to string too much. the documentation says something
    // about when the context is destroyed, the buff will be invalidated.
    // since we do that destruction a lot, maybe avoid using ast to string.
    // z3 sort to string is known (by me) to have problems as well.
    // there may be more.
    llvm::outs() << Z3_ast_to_string(ctx, ast) << "\n";
  }
  void Z3Solver::prtStats() {
    Z3_stats stats = Z3_solver_get_statistics(ctx, solver);
    Z3_stats_inc_ref(ctx, stats);
    llvm::outs() << "Solver stats:\n";
    unsigned int statsSize = Z3_stats_size(ctx, stats);
    for (unsigned int i = 0; i < statsSize; i++) {
      llvm::outs() << Z3_stats_get_key(ctx, stats, i) << ": ";
      if (Z3_stats_is_uint(ctx, stats, i))
	llvm::outs() << Z3_stats_get_uint_value(ctx, stats, i) << "\n";
      else if (Z3_stats_is_double(ctx, stats, i))
	llvm::outs() << Z3_stats_get_double_value(ctx, stats, i) << "\n";
      else
	llvm::outs() << "\n";
    }
    Z3_stats_dec_ref(ctx, stats);
  }

  // extracting assumption
  void Z3Solver::handleConditional(PCINSTR instr, std::string id,
				   Z3_ast assertion) {
    PCINSTR choice;
    // must be unique within solver, id is used specifically for that
    std::string uniqueName = id + "-" + sum->prepareAssumption(instr, choice);
    Z3_ast toggle = mk_bool_var(ctx, mk_name(uniqueName));
    Z3_ast relax[] = { assertion, toggle }; // wtf the name
    Z3_solver_assert(ctx, solver, Z3_mk_or(ctx, 2, relax));
    Z3_ast assumption = Z3_mk_not(ctx, toggle);
    uc.addAssumption(Z3_get_ast_id(ctx, assumption), assumption,
		     instr, choice);
  }

  bool Z3Solver::add(const Constraint &cc) {
    bool clear = true;
    typedef const llvm::Value *PCV;

    vValueIterator &vvi = cc.getVValueIterator();
    vValueIterator &vvj = cc.end();
    switch (cc.kind) {
      // we especially care about constraints from conditional instrs,
      // because we will create an assumption at that point each time
      // note conditional instrs never update any variables, dk if important
      // currently a little overkilling, e.g., switch can have only default,
      // even conditional br can be always true/false, making them unconditional
      // better more useless assumptions than too many un-undoable assertions
    case Expr::IS_TRUE: {
      Z3_ast op0 = evaluateBool(ctx, *vvi);
      Z3_ast constT = mk_bv(ctx, 1, 1);
      Z3_ast eq = Z3_mk_eq(ctx, op0, constT);
      PCINSTR instr = cc.getInstr();
      // must be a conditional br
      if (useAssumption && instr && llvm::isa<llvm::BranchInst>(instr))
	handleConditional(instr, vvi->toString(), eq);
      else
	Z3_solver_assert(ctx, solver, eq);
      break;
    }
    case Expr::IS_FALSE: {
      Z3_ast op0 = evaluateBool(ctx, *vvi);
      Z3_ast constF = mk_bv(ctx, 0, 1);
      Z3_ast eq = Z3_mk_eq(ctx, op0, constF);
      PCINSTR instr = cc.getInstr();
      // must be a conditional br
      if (useAssumption && instr && llvm::isa<llvm::BranchInst>(instr))
	handleConditional(instr, vvi->toString(), eq);
      else
	Z3_solver_assert(ctx, solver, eq);
      break;
    }
    case Expr::EQ: {
      const VersionedValue &op0VV = *vvi;
      Z3_ast op0 = evaluateUnknown(ctx, *vvi, ptrSize);
      ++vvi;
      Z3_ast op1 = evaluateUnknown(ctx, *vvi, ptrSize);
      // the two operands should have the same type, not asserted
      Z3_ast eq = Z3_mk_eq(ctx, op0, op1);
      PCINSTR instr = cc.getInstr();
      // must be a switch
      if (useAssumption && instr && llvm::isa<llvm::SwitchInst>(instr))
	handleConditional(instr, op0VV.toString(), eq);
      else
	Z3_solver_assert(ctx, solver, eq);
      break;
    }
    case Expr::NEQ: {
      const InequalityExpr &ie = static_cast<const InequalityExpr &>(cc);
      unsigned int n = ie.getNumOfIeqs();
      Z3_ast *ieqs = new Z3_ast[n]; // don't know if should delete
      const VersionedValue &op0VV = *vvi;
      unsigned int size = getBVWidth((*vvi).get());
      Z3_ast op0 = evaluateBV(ctx, *vvi, size);
      VersionedValue **opVVs = ie.getOps();
      for (unsigned int i = 0; i < n; i++) {
	VersionedValue *opVV = opVVs[i];
	// should have same size
	Z3_ast op = evaluateBV(ctx, *opVV, size);
	Z3_ast eq = Z3_mk_eq(ctx, op0, op);
	Z3_ast ieq = Z3_mk_not(ctx, eq);
	ieqs[i] = ieq;
      }
      Z3_ast andIeqs = Z3_mk_and(ctx, n, ieqs);
      PCINSTR instr = cc.getInstr();
      // must be a switch
      if (useAssumption && instr && llvm::isa<llvm::SwitchInst>(instr))
	handleConditional(instr, op0VV.toString(), andIeqs);
      else
	Z3_solver_assert(ctx, solver, andIeqs);
      delete ieqs;
      break;
    }
    case Expr::ADD:
    case Expr::SUB:
    case Expr::MUL:
    case Expr::UDIV:
    case Expr::SDIV:
    case Expr::UREM:
    case Expr::SREM:
    case Expr::AND:
    case Expr::OR:
    case Expr::XOR: {
      PCV op0V = (*vvi).get();
      unsigned int bitWidth;
      Z3_ast op0, op1, op2;
      if (op0V->getType()->isPointerTy()) {
	// handling pointer arithmetics, acutally only add kind should exist
	bitWidth = ptrSize;
	// should all be pointers
	op0 = evaluatePtr(ctx, *vvi, ptrSize);
	++vvi;
	op1 = evaluatePtr(ctx, *vvi, ptrSize);
	++vvi;
	op2 = evaluatePtr(ctx, *vvi, ptrSize);
      } else {
	bitWidth = getBVWidth(op0V);
	// shouldn't be const, but who knows; same for the others
	op0 = evaluateBV(ctx, *vvi, bitWidth);
	++vvi;
	PCV op1V = (*vvi).get();
	checkBVWidth(op1V, bitWidth);
	op1 = evaluateBV(ctx, *vvi, bitWidth);
	++vvi;
	PCV op2V = (*vvi).get();
	checkBVWidth(op2V, bitWidth);
	op2 = evaluateBV(ctx, *vvi, bitWidth);
      }
      Z3_ast op;
      switch (cc.kind) {
      case Expr::ADD: op = Z3_mk_bvadd(ctx, op1, op2); break;
      case Expr::SUB: op = Z3_mk_bvsub(ctx, op1, op2); break;
      case Expr::MUL: op = Z3_mk_bvmul(ctx, op1, op2); break;
      case Expr::UDIV: op = Z3_mk_bvudiv(ctx, op1, op2); break;
      case Expr::SDIV: op = Z3_mk_bvsdiv(ctx, op1, op2); break;
      case Expr::UREM: op = Z3_mk_bvurem(ctx, op1, op2); break;
      case Expr::SREM: op = Z3_mk_bvsrem(ctx, op1, op2); break;
      case Expr::AND: op = Z3_mk_bvand(ctx, op1, op2); break;
      case Expr::OR: op = Z3_mk_bvor(ctx, op1, op2); break;
      case Expr::XOR: op = Z3_mk_bvxor(ctx, op1, op2); break;
      default:
	report("unknown constraint kind", 0x0252);
	op = mk_bv(ctx, 0, bitWidth); // only to suppress the warning
      }
      Z3_ast eq = Z3_mk_eq(ctx, op0, op);
      Z3_solver_assert(ctx, solver, eq);
      break;
    }
    case Expr::FADD:
    case Expr::FSUB:
    case Expr::FMUL:
    case Expr::FDIV:
    case Expr::FREM: {
      Z3_ast op0 = evaluateFP(ctx, *vvi);
      ++vvi;
      Z3_ast op1 = evaluateFP(ctx, *vvi);
      ++vvi;
      Z3_ast op2 = evaluateFP(ctx, *vvi);
      Z3_ast ops[2] = {op1, op2};
      Z3_ast op;
      switch (cc.kind) {
      case Expr::FADD: op = Z3_mk_add(ctx, 2, ops); break;
      case Expr::FSUB: op = Z3_mk_sub(ctx, 2, ops); break;
      case Expr::FMUL: op = Z3_mk_mul(ctx, 2, ops); break;
      case Expr::FDIV: op = Z3_mk_div(ctx, op1, op2); break;
      case Expr::FREM: op = Z3_mk_rem(ctx, op1, op2); break;
      default:
	report("unknown constraint kind", 0x0252);
	op = mk_real(ctx, "0.0"); // only to suppress the warning
      }
      Z3_ast eq = Z3_mk_eq(ctx, op0, op);
      Z3_solver_assert(ctx, solver, eq);
      break;
    }
    case Expr::SHL:
    case Expr::LSHR:
    case Expr::ASHR: {
      PCV op0V = (*vvi).get();
      unsigned int bitWidth = getBVWidth(op0V);
      Z3_ast op0 = evaluateBV(ctx, *vvi, bitWidth);
      ++vvi;
      PCV op1V = (*vvi).get();
      checkBVWidth(op1V, bitWidth);
      Z3_ast op1 = evaluateBV(ctx, *vvi, bitWidth);
      ++vvi;
      PCV op2V = (*vvi).get();
      checkBVWidth(op2V, bitWidth);
      // "the second argument is treated as an unsigned", says llvm
      // don't know if z3 does the same
      // z3 warns about not necessarily capturing the semantics of the
      // modeled language
      Z3_ast op2 = evaluateBV(ctx, *vvi, bitWidth);
      Z3_ast op;
      switch (cc.kind) {
      case Expr::SHL: op = Z3_mk_bvshl(ctx, op1, op2); break;
      case Expr::LSHR: op = Z3_mk_bvlshr(ctx, op1, op2); break;
      case Expr::ASHR: op = Z3_mk_bvashr(ctx, op1, op2); break;
      default:
	report("unknown constraint kind", 0x0252);
	op = mk_bv(ctx, 0, bitWidth); // only to suppress the warning
      }
      Z3_ast eq = Z3_mk_eq(ctx, op0, op);
      Z3_solver_assert(ctx, solver, eq);
      break;
    }
    case Expr::ALLOCA: {
      if (!MEM_CHECK)
	break;
      /* This is an important one. The constraints mean, the allocated space
	 shouldn't be allocated with anything else previously, while there
	 being two modes. If the constraints are pushed in a forward manner,
	 (corresponding to "post"), then we need to record all previously
	 allocated objects' bounds, so that when we see another allocation,
	 we can make assertions to make sure the results don't collide with
	 the previous allocation. The "pre" case is the opposite. This
	 creates a lot of comparison constraints and is pretty time
	 consuming. */
      // memory visiting check, undone
      // need to deallocate at return, undone
      // also consider null-pointer constraints, undone
      const VersionedValue &op0VV = *vvi;
      // if memory not flatten (see configuraton), it should be in bits
      unsigned int allocatedSize = ((const Allocation &) cc).getSize();

      // check if it's an array
      ++vvi;
      if (vvi != vvj) {
	PCV op1V = (*vvi).get();
	if (const llvm::ConstantInt *constInt =
	    llvm::dyn_cast<const llvm::ConstantInt>(op1V)) {
	  allocatedSize *= constInt->getZExtValue();
	} else {
	  // stack allocation having dynamic size? does it happen?
	  // unhandled yet
	  report("missing bounds on memory validity", 0x0253, true);
	  break;
	}
      }
      bool ubSame = (allocatedSize == 1);

      // make bound check
      Z3_ast op0 = evaluatePtr(ctx, op0VV, ptrSize);
      Z3_ast size = mk_ptr_bv(ctx, allocatedSize, ptrSize);
      bool preOrPost = false; // not implemented
      Z3_ast ub, check;
      if (!mc.needToCheck()) { // quick path
	if (!ubSame)
	  ub = mk_ptr_bv(ctx, INIT_ADDRESS + allocatedSize, ptrSize);
	Z3_ast initAddress = mk_ptr_bv(ctx, INIT_ADDRESS, ptrSize);
	check = Z3_mk_eq(ctx, op0, initAddress);
	Z3_solver_assert(ctx, solver, check);
	if (ubSame)
	  mc.add(initAddress, scopeLevel, preOrPost);
	else
	  mc.add(initAddress, ub, scopeLevel, preOrPost);
	break;
      }
      Z3_solver_push(ctx, solver);
      modelAvail = false;
      if (MC_MONOTONE && COMPACT_MEM) {
	// assuming post
	BoundPair *bp = mc.getEdge();
	bool isSize1 = bp->isSize1;
	Z3_ast edge = isSize1 ? bp->lb : bp->ub;
	Z3_ast eq;
	if (isSize1)
	  eq = Z3_mk_eq(ctx, mk_bvadd_no_overflow
			(ctx, solver, edge, mk_ptr_bv(ctx, 1, ptrSize)), op0);
	else
	  eq = Z3_mk_eq(ctx, edge, op0);
	// no need to check, this is ultimately simplified
	if (!ubSame)
	  ub = mk_bvadd_no_overflow(ctx, solver, op0, size);
	check = eq;
      } else {
	if (ubSame) {
	  check = mc.makeBoundCheck(ctx, op0, preOrPost);
	} else {
	  ub = mk_bvadd_no_overflow(ctx, solver, op0, size);
	  check = mc.makeBoundCheck(ctx, op0, ub, preOrPost);
	}
      }
      Z3_solver_assert(ctx, solver, check);
      Z3_ast concretes[2];
      bool concretised;
      if (ubSame) {
	Z3_ast toConcretise[1] = {op0};
	concretised = concretise(1, toConcretise, concretes);
      } else {
	Z3_ast toConcretise[2] = {op0, ub};
	concretised = concretise(2, toConcretise, concretes);
      }
      Z3_solver_pop(ctx, solver, 1);
      modelAvail = false;
      // if we can get concretes back from Z3 that would be nice
      if (concretised) {
	if (ubSame) {
	  Z3_ast eq = Z3_mk_eq(ctx, op0, concretes[0]);
	  Z3_solver_assert(ctx, solver, eq);
	  mc.add(concretes[0], scopeLevel, preOrPost);
	} else {
	  Z3_ast eq = Z3_mk_eq(ctx, op0, concretes[0]);
	  Z3_solver_assert(ctx, solver, eq);
	  mc.add(concretes[0], concretes[1], scopeLevel, preOrPost);
	}
      } else {
	report("concretising failed", 0x025c);
	clear = false;
      }
      break;
    }
      // load and store successful, unsuccessful case unhandled
      // performance-significant
    case Expr::LOAD: {
      // des
      Z3_ast op0 = evaluateUnknown(ctx, *vvi, ptrSize);
      Z3_sort opSort = Z3_get_sort(ctx, op0);
      ++vvi;

      // index
      Z3_ast op1 = evaluatePtr(ctx, *vvi, ptrSize);

      // assignment
      Z3_sort sort;
      if (Z3_get_sort_kind(ctx, opSort) == Z3_BV_SORT) {
	sort = Z3_mk_bv_sort(ctx, CELL_SIZE);
      } else if (FP_MEMORY && Z3_get_sort_kind(ctx, opSort) == Z3_REAL_SORT) {
	sort = Z3_mk_real_sort(ctx);
      } else {
	report("unknown type of data for memory", 0x0258);
	break;
      }
      std::string sortStr = sort_to_string(ctx, sort);
      const char *name = getMAName(*vvi, sortStr, 0);
      Z3_ast memArray = mk_memory_array_var(ctx, name, ptrSize, sort);
      Z3_ast load = Z3_mk_select(ctx, memArray, op1);
      Z3_ast converted = convert(ctx, load, opSort);
      Z3_ast eq = Z3_mk_eq(ctx, op0, converted);
      Z3_solver_assert(ctx, solver, eq);
      break;
    }
    case Expr::STORE: {
      // src
      Z3_ast op0 = evaluateUnknown(ctx, *vvi, ptrSize);
      Z3_sort opSort = Z3_get_sort(ctx, op0);
      const VersionedValue &op0VV = *vvi;
      ++vvi;

      // index
      Z3_ast op1 = evaluatePtr(ctx, *vvi, ptrSize);

      // assignment
      Z3_sort sort, theOtherSort;
      if (Z3_get_sort_kind(ctx, opSort) == Z3_BV_SORT) {
	sort = Z3_mk_bv_sort(ctx, CELL_SIZE);
	theOtherSort = Z3_mk_real_sort(ctx);
      } else if (FP_MEMORY && Z3_get_sort_kind(ctx, opSort) == Z3_REAL_SORT) {
	sort = Z3_mk_real_sort(ctx);
	theOtherSort = Z3_mk_bv_sort(ctx, CELL_SIZE);
      } else {
	report("unknown type of data for memory", 0x0258);
	break;
      }
      std::string sortStr = sort_to_string(ctx, sort);
      const char *name = getMAName(op0VV, sortStr, 0);
      Z3_ast memArray = mk_memory_array_var(ctx, name, ptrSize, sort);
      name = getMAName(*vvi, sortStr, 0);
      Z3_ast newMA = mk_memory_array_var(ctx, name, ptrSize, sort);
      Z3_ast converted = convert(ctx, op0, sort, true);
      Z3_ast store = Z3_mk_store(ctx, memArray, op1, converted);
      Z3_ast mAEq = Z3_mk_eq(ctx, newMA, store);
      Z3_solver_assert(ctx, solver, mAEq);

      if (FP_MEMORY) {
	// mem array of the other type(s) unchanged
	sortStr = sort_to_string(ctx, theOtherSort);
	name = mk_name(getMAName(op0VV, sortStr, 0));
	memArray = mk_memory_array_var(ctx, name, ptrSize, theOtherSort);
	name = mk_name(getMAName(*vvi, sortStr, 0));
	newMA = mk_memory_array_var(ctx, name, ptrSize, theOtherSort);
	mAEq = Z3_mk_eq(ctx, newMA, memArray);
	Z3_solver_assert(ctx, solver, mAEq);
      }
      break;
    }
    case Expr::GEP: {
      // get the two operand pointers
      Z3_ast op0 = evaluatePtr(ctx, *vvi, ptrSize);
      ++vvi;
      Z3_ast op1 = evaluatePtr(ctx, *vvi, ptrSize);
      ++vvi;

      // process the first index
      Z3_ast tempPtr = op1;
      // don't understand why llvm doesn't allow const
      // target type won't be changed
      llvm::Type *targetType = const_cast<llvm::Type *>
	(((const AddressingOperation &) cc).getTargetType());
      if (vvi != vvj) {
	bool isConstZero;
	Z3_ast index = evaluatePtrBV(ctx, isConstZero, *vvi, ptrSize);
	if (!isConstZero) {
	  unsigned int size;
	  if (MEMORY_FLATTEN)
	    size = getFlattenSize(*targetType);
	  else
	    size = context->dl->GET_TYPE_SE_SIZE(targetType);
	  Z3_ast sizeBV = mk_ptr_bv(ctx, size, ptrSize);
	  Z3_ast mul = mk_bvmul_no_overflow(ctx, solver, index, sizeBV);
	  Z3_ast shift = mk_bvadd_no_overflow(ctx, solver, tempPtr, mul);
	  tempPtr = shift;
	}
	++vvi;
      }

      // process the rest of the indices
      llvm::Type *type = targetType;
      for (int i = 0; vvi != vvj; ++vvi, i++) {
	if (type->isStructTy()) {
	  // llvm requires that all indices are constant i32 in this case
	  llvm::StructType *structType =
	    llvm::cast<llvm::StructType>(type);
	  unsigned int offset;
	  if (MEMORY_FLATTEN) {
	    const llvm::ConstantInt *constInt =
	      llvm::cast<const llvm::ConstantInt>(vvi->get());
	    // if we are sure there is no negative index, make zero ext
	    // not checked negative
	    offset = getElementFlattenOffset
	      (*structType, unsigned(constInt->getSExtValue()));
	  } else {
	    offset = getOffsetInBits(structType, *vvi);
	  }
	  if (offset) {
	    Z3_ast offsetBV = mk_ptr_bv(ctx, offset, ptrSize);
	    Z3_ast shift = mk_bvadd_no_overflow(ctx, solver, tempPtr, offsetBV);
	    tempPtr = shift;
	  }
	  type = structType->getElementType(i);
	} else {
	  // can be variables in this case
	  // must be array since they are the only two aggregated types
	  llvm::ArrayType *arrayType =
	    llvm::cast<llvm::ArrayType>(type);
	  llvm::Type *elementType = arrayType->getElementType();
	  bool isConstZero;
	  Z3_ast index = evaluatePtrBV(ctx, isConstZero, *vvi, ptrSize);
	  if (!isConstZero) {
	    unsigned int elementSize;
	    if (MEMORY_FLATTEN)
	      elementSize = getFlattenSize(*elementType);
	    else
	      elementSize = context->dl->GET_TYPE_SE_SIZE(elementType);
	    Z3_ast elementSizeBV = mk_ptr_bv(ctx, elementSize, ptrSize);
	    Z3_ast mul =
	      mk_bvmul_no_overflow(ctx, solver, index, elementSizeBV);
	    Z3_ast shift = mk_bvadd_no_overflow(ctx, solver, tempPtr, mul);
	    tempPtr = shift;
	  }
	  type = elementType;
	}
      }
      Z3_ast eq = Z3_mk_eq(ctx, op0, tempPtr);
      Z3_solver_assert(ctx, solver, eq);
      break;
    }
      // from here, we are more likely to have untested code, because
      // programs rarely use these operations
    case Expr::TRUNC: {
      PCV op0V = (*vvi).get();
      unsigned int bitWidth0 = getBVWidth(op0V);
      Z3_ast op0 = evaluateBV(ctx, *vvi, bitWidth0);
      ++vvi;
      PCV op1V = (*vvi).get();
      unsigned int bitWidth1 = getBVWidth(op1V);
      Z3_ast op1 = evaluateBV(ctx, *vvi, bitWidth1);
      assert(bitWidth1 > bitWidth0 && "Illegal truncation");
      Z3_ast extract = Z3_mk_extract(ctx, bitWidth0 - 1, 0, op1);
      Z3_ast eq = Z3_mk_eq(ctx, op0, extract);
      Z3_solver_assert(ctx, solver, eq);
      break;
    }
    case Expr::ZEXT: {
      PCV op0V = (*vvi).get();
      unsigned int bitWidth0 = getBVWidth(op0V);
      Z3_ast op0 = evaluateBV(ctx, *vvi, bitWidth0);
      ++vvi;
      PCV op1V = (*vvi).get();
      unsigned int bitWidth1 = getBVWidth(op1V);
      Z3_ast op1 = evaluateBV(ctx, *vvi, bitWidth1);
      assert(bitWidth1 < bitWidth0 && "Illegal extension");
      Z3_ast zext = Z3_mk_zero_ext(ctx, bitWidth0 - bitWidth1, op1);
      Z3_ast eq = Z3_mk_eq(ctx, op0, zext);
      Z3_solver_assert(ctx, solver, eq);
      break;
    }
    case Expr::SEXT: {
      PCV op0V = (*vvi).get();
      unsigned int bitWidth0 = getBVWidth(op0V);
      Z3_ast op0 = evaluateBV(ctx, *vvi, bitWidth0);
      ++vvi;
      PCV op1V = (*vvi).get();
      unsigned int bitWidth1 = getBVWidth(op1V);
      Z3_ast op1 = evaluateBV(ctx, *vvi, bitWidth1);
      assert(bitWidth1 < bitWidth0 && "Illegal extension");
      Z3_ast sext = Z3_mk_sign_ext(ctx, bitWidth0 - bitWidth1, op1);
      Z3_ast eq = Z3_mk_eq(ctx, op0, sext);
      Z3_solver_assert(ctx, solver, eq);
      break;
    }
      // we used real to represent fp so we don't have this logic
    case Expr::FPTRUNC:
    case Expr::FPEXT: {
      Z3_ast op0 = evaluateFP(ctx, *vvi);
      ++vvi;
      Z3_ast op1 = evaluateFP(ctx, *vvi);
      Z3_ast eq = Z3_mk_eq(ctx, op0, op1);
      Z3_solver_assert(ctx, solver, eq);
      break;
    }
    case Expr::FP_UI:
    case Expr::FP_SI: {
      PCV op0V = (*vvi).get();
      unsigned int bitWidth = getBVWidth(op0V);
      Z3_ast op0 = evaluateBV(ctx, *vvi, bitWidth);
      ++vvi;
      Z3_ast op1 = evaluateFP(ctx, *vvi);
      // there is no real2bv in Z3, so we can only do this
      // from experience, it is not good
      Z3_ast real2int = Z3_mk_real2int(ctx, op1);
      Z3_ast int2bv = Z3_mk_int2bv(ctx, bitWidth, real2int);
      Z3_ast eq = Z3_mk_eq(ctx, op0, int2bv);
      Z3_solver_assert(ctx, solver, eq);
      break;
    }
    case Expr::UI_FP:
    case Expr::SI_FP: {
      Z3_ast op0 = evaluateFP(ctx, *vvi);
      ++vvi;
      PCV op1V = (*vvi).get();
      unsigned int bitWidth = getBVWidth(op1V);
      Z3_ast op1 = evaluateBV(ctx, *vvi, bitWidth);
      // same as above
      Z3_ast bv2int = Z3_mk_bv2int(ctx, op1, cc.kind == Expr::SI_FP);
      Z3_ast int2real = Z3_mk_int2real(ctx, bv2int);
      Z3_ast eq = Z3_mk_eq(ctx, op0, int2real);
      Z3_solver_assert(ctx, solver, eq);
      break;
    }
    case Expr::PTR_INT: {
      PCV op0V = (*vvi).get();
      unsigned int bitWidth = getBVWidth(op0V);
      Z3_ast op0 = evaluateBV(ctx, *vvi, bitWidth);
      ++vvi;
      Z3_ast op1 = evaluatePtr(ctx, *vvi, ptrSize);
      Z3_ast converted = convert(ctx, op1, Z3_get_sort(ctx, op0), false);
      Z3_ast eq = Z3_mk_eq(ctx, op0, converted);
      Z3_solver_assert(ctx, solver, eq);
      break;
    }
    case Expr::INT_PTR: {
      Z3_ast op0 = evaluatePtr(ctx, *vvi, ptrSize);
      ++vvi;
      PCV op1V = (*vvi).get();
      unsigned int bitWidth = getBVWidth(op1V);
      Z3_ast op1 = evaluateBV(ctx, *vvi, bitWidth);
      Z3_ast converted = convert(ctx, op1, Z3_get_sort(ctx, op0), false);
      Z3_ast eq = Z3_mk_eq(ctx, op0, converted);
      Z3_solver_assert(ctx, solver, eq);
      break;
    }
    case Expr::BC: {
      // warning, unsupported types may cause the solver to abort, too soon
      Z3_ast op0 = evaluateUnknown(ctx, *vvi, ptrSize);
      ++vvi;
      Z3_ast op1 = evaluateUnknown(ctx, *vvi, ptrSize);
      // size should be the same, warning means error
      Z3_ast converted = convert(ctx, op1, Z3_get_sort(ctx, op0), true);
      Z3_ast eq = Z3_mk_eq(ctx, op0, converted);
      Z3_solver_assert(ctx, solver, eq);
      break;
    }
    case Expr::CMP: {
      Z3_ast op0 = evaluateBool(ctx, *vvi);
      // the two operands should have the according types, not asserted
      ++vvi;
      Z3_ast op1 = evaluateUnknown(ctx, *vvi, ptrSize);
      ++vvi;
      Z3_ast op2 = evaluateUnknown(ctx, *vvi, ptrSize);
      using llvm::CmpInst;
      const CmpInst::Predicate predicate =
	((const ComparisonExpr &) cc).getPredicate();
      Z3_ast cmp;
      switch (predicate) {
      case CmpInst::FCMP_FALSE: cmp = Z3_mk_false(ctx); break;
	// fp comparisons can be divided into two categories,
	// one that reveal true when there is a qnan and one does not,
	// thus llvm has got no exception or undefined behaviour
	// in fp comparisons. it seems in z3, it's always the unordered
	// case, i.e., when there is an exception, z3 gives true.
	// there is the divergence.
	// fp api?
      case CmpInst::FCMP_OEQ:
      case CmpInst::FCMP_UEQ: cmp = Z3_mk_eq(ctx, op1, op2); break;
      case CmpInst::FCMP_OGT:
      case CmpInst::FCMP_UGT: cmp = Z3_mk_gt(ctx, op1, op2); break;
      case CmpInst::FCMP_OGE:
      case CmpInst::FCMP_UGE: cmp = Z3_mk_ge(ctx, op1, op2); break;
      case CmpInst::FCMP_OLT:
      case CmpInst::FCMP_ULT: cmp = Z3_mk_lt(ctx, op1, op2); break;
      case CmpInst::FCMP_OLE:
      case CmpInst::FCMP_ULE: cmp = Z3_mk_le(ctx, op1, op2); break;
      case CmpInst::FCMP_ONE:
      case CmpInst::FCMP_UNE:
	cmp = Z3_mk_not(ctx, Z3_mk_eq(ctx, op1, op2)); break;
      case CmpInst::FCMP_ORD: cmp = Z3_mk_true(ctx); break; // inaccurate
      case CmpInst::FCMP_UNO: cmp = Z3_mk_false(ctx); break; // inaccurate
      case CmpInst::FCMP_TRUE: cmp = Z3_mk_true(ctx); break;
      case CmpInst::ICMP_EQ: cmp = Z3_mk_eq(ctx, op1, op2); break;
      case CmpInst::ICMP_NE:
	cmp = Z3_mk_not(ctx, Z3_mk_eq(ctx, op1, op2)); break;
      case CmpInst::ICMP_UGT: cmp = Z3_mk_bvugt(ctx, op1, op2); break;
      case CmpInst::ICMP_UGE: cmp = Z3_mk_bvuge(ctx, op1, op2); break;
      case CmpInst::ICMP_ULT: cmp = Z3_mk_bvult(ctx, op1, op2); break;
      case CmpInst::ICMP_ULE: cmp = Z3_mk_bvule(ctx, op1, op2); break;
      case CmpInst::ICMP_SGT: cmp = Z3_mk_bvsgt(ctx, op1, op2); break;
      case CmpInst::ICMP_SGE: cmp = Z3_mk_bvsge(ctx, op1, op2); break;
      case CmpInst::ICMP_SLT: cmp = Z3_mk_bvslt(ctx, op1, op2); break;
      case CmpInst::ICMP_SLE: cmp = Z3_mk_bvsle(ctx, op1, op2); break;
      default:
	report("unknown comparison predicate", 0x025a);
	cmp = Z3_mk_false(ctx);
      }
      Z3_ast eq = Z3_mk_eq(ctx, op0, convert(ctx, cmp));
      Z3_solver_assert(ctx, solver, eq);
      break;
    }
    case Expr::ITE: {
      Z3_ast op0 = evaluateUnknown(ctx, *vvi, ptrSize);
      ++vvi;
      Z3_ast cond = evaluateBool(ctx, *vvi);
      Z3_ast toBool = Z3_mk_eq(ctx, cond, mk_bv(ctx, 1, 1));
      ++vvi;
      Z3_ast op1 = evaluateUnknown(ctx, *vvi, ptrSize);
      ++vvi;
      Z3_ast op2 = evaluateUnknown(ctx, *vvi, ptrSize);
      // the res and two operands should have the same type, not asserted
      Z3_ast ite = Z3_mk_ite(ctx, toBool, op1, op2);
      Z3_ast eq = Z3_mk_eq(ctx, op0, ite);
      Z3_solver_assert(ctx, solver, eq);
      break;
    }
    default:
      report("unknown constraint kind", 0x0252);
    }
    delete &vvi;
    delete &vvj;

    return clear;
  }

  // I don't understand Z3, but sometimes it doesn't concretise value
  // for us, being lazy?
  bool Z3Solver::concretise(unsigned int n,
			    Z3_ast *asts, Z3_ast *concretes) {
    if (!n) {
      llvm::errs() << "Don't do this to me\n";
      return false;
    }
    Counter::inc(5); // solver count (misc)
    if (!(check() == SLV_TRUE)) {
      llvm::errs() << "Not ready for concretising\n";
      return false;
    }
    getModel();
    if (model) {
      for (unsigned int i = 0; i < n; i++) {
	// not cost much from exp
	Z3_bool res = Z3_model_eval(ctx, model, asts[i], 0, &(concretes[i]));
	switch (res) {
	case Z3_L_FALSE:
	  return false;
	case Z3_L_UNDEF:
	  llvm::errs() << "Cannot decide in concretising\n";
	  return false;
	case Z3_L_TRUE:
	  continue;
	default:
	  report("unexpected result", 0x0251);
	  return false;
	}
      }
      return true;
    } else {
      llvm::errs() << "Shouldn't reach here\n";
      return false;
    }
  }
  
  // displaying ast without expanding any function, no visit to the model
  void Z3Solver::display(Z3_ast ast) {
    switch (Z3_get_ast_kind(ctx, ast)) {
    case Z3_NUMERAL_AST:
      llvm::outs() << Z3_get_numeral_string(ctx, ast); // found error once
      break;
    case Z3_APP_AST: {
      Z3_app app = Z3_to_app(ctx, ast);
      if (Z3_is_as_array(ctx, ast)) {
	Z3_func_decl arrayFuncDecl = Z3_get_as_array_func_decl(ctx, ast);
	Z3_symbol name = Z3_get_decl_name(ctx, arrayFuncDecl);
	Z3_string nameStr = Z3_get_symbol_string(ctx, name);
	llvm::outs() << "(as array " << nameStr << ")";
	inputModelFuncs->insert(std::string(nameStr));
      } else {
	Z3_func_decl funcDecl = Z3_get_app_decl(ctx, app);
	Z3_symbol name = Z3_get_decl_name(ctx, funcDecl);
	Z3_string nameStr = Z3_get_symbol_string(ctx, name);
	llvm::outs() << nameStr << "[";
	int n = Z3_get_app_num_args(ctx, app);
	for (int i = 0; i < n; i++) {
	  if (i > 0)
	    llvm::outs() << ",";
	  display(Z3_get_app_arg(ctx, app, i));
	}
	llvm::outs() << "]";
      }
      break;
    }
    case Z3_VAR_AST:
    case Z3_QUANTIFIER_AST:
    case Z3_SORT_AST:
    case Z3_FUNC_DECL_AST:
    case Z3_UNKNOWN_AST:
    default:
      llvm::outs() << "(unknown ast)\n";
    }
  }
  // displaying a function in the model
  void Z3Solver::display(Z3_func_decl funcDecl) {
    if (!Z3_model_has_interp(ctx, model, funcDecl)) {
      llvm::outs() << "(unknown func)";
      return;
    }
    Z3_func_interp function = Z3_model_get_func_interp(ctx, model, funcDecl);
    Z3_func_interp_inc_ref(ctx, function);
    int numEntries = Z3_func_interp_get_num_entries(ctx, function);
    llvm::outs() << "{\n";
    for (int i = 0; i < numEntries; i++) {
      Z3_func_entry entry = Z3_func_interp_get_entry(ctx, function, i);
      Z3_func_entry_inc_ref(ctx, entry);
      int numArgs = Z3_func_entry_get_num_args(ctx, entry);
      if (numArgs > 1)
	llvm::outs() << "(";
      for (int j = 0; j < numArgs; j++) {
	if (j > 0)
	  llvm::outs() << ",";
	display(Z3_func_entry_get_arg(ctx, entry, j));
      }
      if (numArgs > 1)
	llvm::outs() << ")";
      llvm::outs() << "->";
      display(Z3_func_entry_get_value(ctx, entry));
      llvm::outs() << "\n";
      Z3_func_entry_dec_ref(ctx, entry);
    }
    llvm::outs() << "else->";
    display(Z3_func_interp_get_else(ctx, function));
    llvm::outs() << "\n}";
    Z3_func_interp_dec_ref(ctx, function);
  }

}
