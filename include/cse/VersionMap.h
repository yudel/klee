#ifndef CSE_VERSIONMAP_H
#define CSE_VERSIONMAP_H

#include"klee/Config/Version.h"

#if LLVM_VERSION_CODE > LLVM_VERSION(3, 2)
#include"llvm/IR/Value.h"
#else
#include"llvm/Value.h"
#endif

#include<map>

namespace cse {

  // rename this to VersionRange
  struct Version {
    int highest;
    int lowest;

  Version() : highest(0), lowest(0) {}

  Version(int highest, int lowest)
  : highest(highest), lowest(lowest) {}
  Version(const Version &version)
  : highest(version.highest), lowest(version.lowest) {}
  };

  class VersionMap {
  public:
    // F****** S***** cpp!**
    typedef std::map<const llvm::Value *, Version>::iterator MI;
    typedef std::map<const llvm::Value *, Version>::const_iterator MCI;

    VersionMap() {}
  VersionMap(const VersionMap &versionMap)
    : vMap(versionMap.vMap),
      memoryVersion(versionMap.memoryVersion) {}
    
    // the default destructor will do all the job
    
    // bad?
    MCI begin() const { return vMap.begin(); }
    MCI end() const { return vMap.end(); }
    
    bool isChanged(const llvm::Value *value) const {
      MCI i = vMap.find(value);
      if (i == vMap.end())
	return false;
      else
	return i->second.highest == i->second.lowest;
    }

    int getHighest(const llvm::Value *value) const {
      MCI i = vMap.find(value);
      if (i == vMap.end())
	return 0;
      else
	return i->second.highest;
    }

    int getLowest(const llvm::Value *value) const {
      MCI i = vMap.find(value);
      if (i == vMap.end())
	return 0;
      else
	return i->second.lowest;
    }

    // convenient function of the below
    // also, these two will modify the calling vv
    void setPre(const llvm::Value *value,
		int &vvVersion, bool lifted,
		int &vvMemoryVersion);
    void setPost(const llvm::Value *value,
		 int &vvVersion, bool lifted,
		 int &vvMemoryVersion);
    
    void setPre(const VersionMap &another);
    void setPost(const VersionMap &another);

    // preserving the vv's version
    void setPreserving(const llvm::Value *value,
		       int vvVersion, bool lifted,
		       int vvMemoryVersion);

    bool isMemoryChanged() const {
      return memoryVersion.highest == memoryVersion.lowest;
    }
    
    int getMemoryHighest() const {
      return memoryVersion.highest;
    }

    int getMemoryLowest() const {
      return memoryVersion.lowest;
    }

    int getMemoryDifference() const {
      return memoryVersion.highest - memoryVersion.lowest;
    }

    int setMemoryPre() {
      return --memoryVersion.lowest;
    }
    int setMemoryPre(bool mLifted) {
      return (memoryVersion.lowest = memoryVersion.lowest + (mLifted ? -1 : 0));
    }

    int setMemoryPost() {
      return ++memoryVersion.highest;
    }
    int setMemoryPost(bool mLifted) {
      return (memoryVersion.highest = memoryVersion.highest +
	      (mLifted ? 1 : 0));
    }

    void setMemoryPreserving(int highest, int lowest) {
      memoryVersion.highest = highest;
      memoryVersion.lowest = lowest;
    }
    
  private:
    // warning, storing objects in map
    // don't like the naming
    std::map<const llvm::Value *, Version> vMap;
    Version memoryVersion;
  };
  
}

#endif
