#ifndef CSE_CISTACKFRAME_H
#define CSE_CISTACKFRAME_H

#include"llvm/IR/Function.h"

#include"cse/Common.h"

namespace cse {

  // ci stack frames are used by interproc states
  // alright, I don't know what to call it
  class CIStackFrame {
  public:
  CIStackFrame(const llvm::Function *f, PCINSTR callInstr)
    : f(f), callInstr(callInstr) {}

    // I'm so fed up with the const specifier, fix when you feel like to
    llvm::Function *getFunction() const {
      return const_cast<llvm::Function *>(f);
    }

    PCINSTR getCallInstr() const { return callInstr; }
    
  private:
    const llvm::Function *f;
    PCINSTR callInstr;
  };
  
}

#endif
