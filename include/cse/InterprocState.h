#ifndef CSE_INTERPROCSTATE_H
#define CSE_INTERPROCSTATE_H

#include"cse/Common.h"
#include"cse/State.h"

namespace cse {

  class InterprocState : public State {
  public:
  InterprocState(Context *context)
    : State(context) {}
    
  InterprocState(Context *context, Solver *solver)
    : State(context, solver) {}

  InterprocState(const InterprocState &state)
    : State(state) {}

    ~InterprocState() {}

    InterprocState &operator=(const InterprocState &other) {
      State::operator=(other);
      return *this;
    }

    // no overriding, just be precise
    // actually no difference
    void copyFrom(const InterprocState &other) {
      State::copyFrom(other);
    }

    void pushEntryFunc(const llvm::Function *entryFunc); // debug only
    bool finished();
    
    // count the number of times a summary is used, to be done
    void clearChoicePool();
    bool readyChoicePool(PCINSTR instr);

    // no overriding
    InterprocState visit(bool &res, bool &clear);

    void registerSummary() = delete;

  private:
    // one of the differences between interproc states and states is that
    // the former go through the entire call stack while the latter only
    // live in one frame (at most one call at the end if it's a call instr)
    // but the call stack is recorded in the summary, so syntactically the
    // two kinds of states are similar
    // the other difference is that this kind of states reuse summaries
    // and aside from the many differences this creates, we also want to
    // mention that we put summaries into the solver in a different way

    bool checkAgainstUnsatCore(const Summary *sum);
    void trimChoicePool();
  };
  
}

#endif
