#ifndef CSE_UNSATCORE_H
#define CSE_UNSATCORE_H

#include"cse/Summary.h"

#include<z3.h>

#include<map>
#include<set>
#include<string>

namespace cse {

  struct AssumptionRecord {
    Z3_ast assumption;
    PCINSTR branch;
    PCINSTR choice;

  AssumptionRecord(Z3_ast assumption, PCINSTR branch, PCINSTR choice)
  : assumption(assumption),
      branch(branch),
      choice(choice) {}

  };

  typedef std::map<unsigned int, AssumptionRecord> MUI2AR;
  typedef std::set<std::pair<PCINSTR, PCINSTR>> SPPP;
  
  struct Knowledge {
    const Summary *sum;
    mutable bool asserted;
    mutable MUI2AR ars;
    // there are some problems with the naming, the ucs is actually a
    // SINGLE unsat core, consisting of several asts
    mutable SPPP ucs;

  Knowledge(const Summary *sum)
  : sum(sum), asserted(true) {}

    bool operator==(const Knowledge &other) const {
      return this->sum == other.sum;
    }
    bool operator<(const Knowledge &other) const {
      return this->sum < other.sum;
    }
    bool operator>(const Knowledge &other) const {
      return this->sum > other.sum;
    }

    bool hasAssumption() const { return !ars.empty(); }
  };
  
  class UnsatCore {
  public:
    void addKnowledge(const Summary *sum) {
      current = &(*(knowledges.insert(Knowledge(sum)).first));
    }
    void clear() {
      knowledges.clear();
      current = NULL;
    }

    bool hasAssumption() const { return current && current->hasAssumption(); }
    unsigned int getNumOfAssumptions() const;
    MUI2AR::const_iterator arsBegin() const {
      assert(current);
      return current->ars.begin();
    }
    MUI2AR::const_iterator arsEnd() const {
      assert(current);
      return current->ars.end();
    }
    Z3_ast *getAssumptions() const;
    void addAssumption(unsigned int hash, Z3_ast assumption,
		       PCINSTR branch, PCINSTR choice);

    bool hasUnsatCore() const { return current && !current->ucs.empty(); }
    SPPP &getUnsatCores() {
      assert(current);
      return current->ucs;
    }
    void addUnsatCore(unsigned int hash);

    void assertAssumptions(Z3_context ctx, Z3_solver solver);
    
  private:
    const Knowledge *current;
    std::set<Knowledge> knowledges; // pretty useless, cause we only need one
  };
  
}

#endif
