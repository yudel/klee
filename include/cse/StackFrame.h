#ifndef CSE_STACKFRAME_H
#define CSE_STACKFRAME_H

#include"cse/State.h"

#include"llvm/Analysis/LoopInfo.h"

#include<stack>

namespace cse {

  // this kind of stack frame is used by the summaxecutor
  // the name is too general
  class StackFrame {
  public:
  StackFrame()
    : loopInfo(NULL) {}

    ~StackFrame() {
      delete loopInfo;
    }
    
    std::stack<State> *getWorkStack() { return &workStack; }

    void setLoopInfo(llvm::LoopInfoBase<llvm::BasicBlock, llvm::Loop> *lib) {
      loopInfo = lib;
    }
    llvm::LoopInfoBase<llvm::BasicBlock, llvm::Loop> *getLoopInfo() {
      return loopInfo;
    }

  private:
    // could use a function name and a resuming instr here
    // work items of current function
    std::stack<State> workStack;
    // loop info of current function
    llvm::LoopInfoBase<llvm::BasicBlock, llvm::Loop> *loopInfo;
  };
  
}

#endif
