#ifndef CSE_UTIL_H
#define CSE_UTIL_H

#include"cse/Summary.h"

#include"llvm/IR/Instructions.h"

namespace cse {

  unsigned int getFlattenSize(const llvm::Type &type);
  unsigned int getElementFlattenOffset(const llvm::Type &type,
				       unsigned int index);

  void readOperands(const llvm::Instruction &instr);
  
  const llvm::Function *getCallee(const llvm::CallInst *ci);

  Summary *summariseEquality(PCINSTR pos, PCINSTR instr,
			     const llvm::Value *value);

}

#endif
