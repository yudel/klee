#ifndef CSE_COMMON_H
#define CSE_COMMON_H

#include"llvm/IR/Function.h"
#include"llvm/IR/Instruction.h"

#include<set>
#include<string>

namespace cse {

  /* This means "pointer to a constant instruction" */
  typedef const llvm::Instruction *PCINSTR;

  /* General control */
  extern unsigned int W_DEPTH;
  extern unsigned int DEPTH_LIMIT;

  extern unsigned int TEST_LIMIT;

  /* Memory & machine model */
  // time consuming
  extern bool MEM_CHECK;
  extern bool MC_MONOTONE;
  extern unsigned int INIT_ADDRESS;
  extern bool COMPACT_MEM;
  extern bool MEMORY_FLATTEN;
  
  // we need a separate fp memory model for fp data,
  // otherwise we cannot handled memory operations on fp values
  // a little time consuming, and not clean
  // for the future consider using Z3's floating-point API?
  extern bool FP_MEMORY;

  // how do we measure size of a type in the symbolic execution
  // virtual machine. llvm provides size/store size/allocation size.
  // I don't understand the differences (more than alignment),
  // but so long as we keep consistency in our own machine
  // (it matters when we do pointer arithmetics), it's fine.
#define GET_TYPE_SE_SIZE getTypeAllocSizeInBits
  
  // this is the imaginary ptr size, i.e., how the tool models the memory
  // this number greatly affects the solver time consumption (not much now)
  // use an artificial, smaller number doesn't necessarily invalidates
  // the results
  // default 32
  extern unsigned int PTR_SIZE; // in bits

  // default memory cell size. ideally memory cell is 1 bit. for some
  // reason we want it big, like 64. see solver impl.
  // cell size shouldn't be smaller than ptr size, otherwise the memory
  // will have problem containing ptrs etc.
  // drastically affecting speed
  extern unsigned int CELL_SIZE; // in bits

  /* Solver */
  extern unsigned int SOLVER_TIMEOUT;
  
  // if not using incremental solving, implmentation should be
  // much simpler, e.g., only one solver is needed
  // currently we are not "disabling it enough", debug only
  extern bool INC_SOLVE;

  extern bool L_QF_ABV; // better use a string
  // extern std::string SOLVER_LOGIC;

#define FULL_ASSUMPT_OFF
  extern bool SIMP_UNSAT_CORE;
  extern unsigned int ASSUMPT_LIMIT;
  extern bool QUICK_ASSUMPT;
  
  /* Messages and errors */
  extern bool V;
  
  extern bool PRT_T;
  
  void report(unsigned int code, bool once = false);
  void report(std::string msg, unsigned int code, bool once = false);
  
  /* About undefined references */
  bool urLookUp(const llvm::Function *f);

}

#endif
