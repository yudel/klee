#ifndef CSE_MEMORYCHECKER_H
#define CSE_MEMORYCHECKER_H

#include<z3.h>

#include<vector>

namespace cse {

  // allocation as [lb, ub), or [lb, lb] when size is 1;
  struct BoundPair {
    Z3_ast lb;
    Z3_ast ub;
    bool isSize1;
    unsigned int scope;

    // used when only allocating 1 unit of memory space
  BoundPair(Z3_ast ptr, unsigned int scope)
  // ub no longer meaningful
  : lb(ptr), ub(ptr), isSize1(true), scope(scope) {}
  BoundPair(Z3_ast lb, Z3_ast ub, unsigned int scope)
  : lb(lb), ub(ub), isSize1(false), scope(scope) {}
  };

  class MemoryChecker {
  public:
  MemoryChecker()
    : edge(NULL) {}

    bool needToCheck(bool preOrPost = false) {
      return preOrPost ? !preBPs.empty() : !postBPs.empty();
    }
    std::vector<BoundPair> &getBPs(bool preOrPost = false) {
      return preOrPost ? preBPs : postBPs;
    }
    BoundPair *getEdge() { return edge; }
    
    void add(Z3_ast ptr, unsigned int scope, bool preOrPost = false);
    void add(Z3_ast lb, Z3_ast ub, unsigned int scope,
	     bool preOrPost = false);

    Z3_ast makeBoundCheck(Z3_context ctx, Z3_ast ptr, bool preOrPost = false);
    Z3_ast makeBoundCheck(Z3_context ctx, Z3_ast lb, Z3_ast ub,
			  bool preOrPost = false);

    void backtrackTo(unsigned int scope);

    void clear();

  private:
    // they shouldn't both exist I think, inc solve can't do both the same time
    // for pre
    std::vector<BoundPair> preBPs;
    // for post
    std::vector<BoundPair> postBPs;
    // useful in monotone mode
    BoundPair *edge;
  };
  
}

#endif
