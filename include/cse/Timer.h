#ifndef CSE_TIMER_H
#define CSE_TIMER_H

#include<chrono>
#include<ratio>

#define N_TIMERECS 10

namespace cse {

  typedef std::chrono::steady_clock SC;
  typedef std::chrono::duration<int, std::ratio<1, 1000>> MS;

  struct TimeRecord {
    SC::time_point tp;
    MS ts;
    // for average
    unsigned int counter;

  TimeRecord()
  : tp(SC::now()),
      ts(std::chrono::duration_cast<MS>(tp - tp)),
      counter(0) {}
  };
  
  class Timer {
  public:
    static void reset(int no = 0);
    
    static void begin(int no = 0);
    static void pause(int no = 0);
    static void unpause(int no = 0);
    static void end(int no = 0);

    static void accumulate(int from, int no);

    static int get(int no = 0);
    static float getAvg(int no = 0);
    
  private:
    static TimeRecord trs[N_TIMERECS];
  };

}

#endif
