#ifndef CSE_FUNCTIONMODELS_H
#define CSE_FUNCTIONMODELS_H

#include"cse/Context.h"
#include"cse/State.h"

#include"llvm/IR/Function.h"
#include"llvm/IR/Instructions.h"

namespace cse {

  class FunctionModels {
  public:
    // default constructor
    
    void init(Context *context) {
      this->context = context;
    }

    void handleFunctionDecl(State &state,
			    const llvm::CallInst *ci,
			    const llvm::Function *f);

  private:
    Context *context;

    void handleVaStart(State &state,
		       const llvm::CallInst *ci,
		       const llvm::Function *f);
  };
  
}

#endif
