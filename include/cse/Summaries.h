#ifndef CSE_SUMMARIES_H
#define CSE_SUMMARIES_H

#if LLVM_VERSION_CODE > LLVM_VERSION(3, 2)
#include"llvm/IR/Instruction.h"
#else
#include"llvm/Instruction.h"
#endif

#include<vector>

namespace cse {

  struct Summaries {
    typedef std::vector<const Summary *>::iterator VI;
    typedef std::vector<const Summary *>::const_iterator VCI;
    
    bool forSingleInstr;
    int numOfSums;
    PCINSTR head;
    PCINSTR tail;
    std::vector<const Summary *> vs;

  Summaries(PCINSTR instr)
  : forSingleInstr(true),
      numOfSums(0),
      head(instr),
      tail(instr) {}

  Summaries(PCINSTR head, PCINSTR tail, bool fsiDetect = true)
  : forSingleInstr(fsiDetect ? head == tail : false),
      numOfSums(0),
      head(head),
      tail(tail) {}
    
    ~Summaries() {
      for (VCI vci = vs.begin(), vcj = vs.end(); vci != vcj; vci++) {
	delete (*vci);
      }
    }

    // bad?
    VCI begin() const { return vs.begin(); }
    VCI end() const { return vs.end(); }

    unsigned int size() const { return vs.size(); }

    // this is for fri, tail could be completely useless and inconsistent
    void push_back(const Summary *s) {
      bool match = s && s->getHead() == head && s->getTail() == tail;
      if (forSingleInstr && !match) {
	llvm::errs() << "Summary not matching\n";
	return;
      }
      if (!match)
	report("head/tail not matching", 0x0030, true);
      vs.push_back(s);
      numOfSums++;
    }
  };
  
}

#endif
