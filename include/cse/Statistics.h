#ifndef CSE_STATISTICS_H
#define CSE_STATISTICS_H

#include"Common.h"
#include"Context.h"

#include<fstream>
#include<set>

namespace cse {

  class Statistics {
  public:
    ~Statistics() {
      fStats.close();
    }
    
    static void init(const Context *context);

    // better has a reset function

    static void categoriseTime(int ms);
    
    static void cover(PCINSTR instr);

    static void printCounter();
    static void printTimer();
    static void printCoverage();
    
  private:
    static const Context *context;
    static std::ofstream fStats;
    static std::set<PCINSTR> covered;
    
    Statistics() {}
  };
  
}

#endif
