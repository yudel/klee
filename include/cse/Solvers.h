#ifndef CSE_SOLVERS_H
#define CSE_SOLVERS_H

#include"cse/Context.h"
#include"cse/MemoryChecker.h"
#include"cse/Summary.h"
#include"cse/UnsatCore.h"

#include"stp/c_interface.h"

#include<z3.h>

namespace cse {

  enum SolverResult {
    SLV_FALSE,
    SLV_UNDEF, // see case Z3_L_UNDEF
    SLV_TRUE
  };
  
  class Solver {
  public:
    Context *context; // why is this public again?
    
  Solver()
    : context(NULL), scopeLevel(0) {}

    // this is never called, do it somewhere!
    // use a reference counter
    virtual ~Solver() {}

    // factory, get a solver impl; init is called here
    static Solver *create(Context *context);
    static Solver *create(); // the imaginary factory, test only

    virtual void init() = 0;

    unsigned int getRestorePoint() { return scopeLevel; }
    virtual unsigned int createRestorePoint() = 0;
    // backtrack to a particular restore point
    virtual void backtrackTo(unsigned int scopeLevel) = 0;

    virtual void prepare(const Summary *sum) = 0;
    virtual bool add(const Constraints &ccs) = 0;

    virtual SolverResult check() = 0;
    virtual UnsatCore &getUnsatCore() = 0; // ugly
    virtual void afterCheck(bool asserting) = 0; // ugly
    virtual void getModel() = 0;
    virtual void display() = 0;

    virtual void dump() = 0;

  protected:
    // this is the ptr size
    // it will be given by the context, if not an imaginary value is used
    unsigned int ptrSize; // in bits
    unsigned int scopeLevel;

    // used to help the solver understand gep operations of struct types
    unsigned int getOffsetInBits(llvm::StructType *structType,
				 const VersionedValue &vv);
  };

  class STPSolver : public Solver {
  public:
    ~STPSolver() { destroy(); }
    
    void init();
    unsigned int createRestorePoint();
    void backtrackTo(unsigned int scopeLevel);

    bool add(const Constraints &ccs);

    SolverResult check();
    void getModel();
    void display();

    void destroy();

    void dump();
    
    static void test(); // debug only

  private:
    VC vc; // it's actually a pointer to void

    // constants
    ::Expr falseConst;

    void initConsts();
    
    // translate to stp expr; names collide
    ::Expr getSTPExpr(const Constraints &ccs);
  };

  class Z3Solver : public Solver {
  public:
    ~Z3Solver() {
      if (model)
	Z3_model_dec_ref(ctx, model);
      Z3_solver_dec_ref(ctx, solver);
      Z3_del_context(ctx);
      // mc automatically deleted
    }
    
    void init();
    unsigned int createRestorePoint();
    void backtrackTo(unsigned int scopeLevel);

    void prepare(const Summary *sum);
    bool add(const Constraints &ccs);

    SolverResult check();
    UnsatCore &getUnsatCore() { return uc; }
    void afterCheck(bool asserting);
    void getModel();
    void display();

    void dump();

    static void test(); // debug only

  private:
    Z3_context ctx;
    Z3_solver solver;
    // this boolean requires that you must get model immediately after
    // successful check, no assertions, push/pops, or things like that
    // in between, except for trivial opertions like printing out queries
    bool modelAvail;
    Z3_model model;

    bool useAssumption;
    const Summary *sum;
    UnsatCore uc;

    // checking memory validity regarding whether or not
    // memory cells are allocated
    MemoryChecker mc;

    std::set<std::string> *inputModelFuncs;

    void prt(Z3_ast ast);
    void prtStats();

    void handleConditional(PCINSTR instr, std::string id, Z3_ast assertion);
    bool add(const Constraint &cc);

    // these two are used by the public check
    Z3_lbool checkAssumptions(); // our own quick assumption checker
    Z3_lbool check(bool assumptionEnabled);

    bool concretise(unsigned int n, Z3_ast *asts, Z3_ast *concretes);

    void display(Z3_func_decl funcDecl);
    void display(Z3_ast ast);
  };
  
}

#endif
