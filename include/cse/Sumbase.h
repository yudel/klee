#ifndef CSE_SUMBASE_H
#define CSE_SUMBASE_H

#include"cse/Summary.h"
#include"cse/Summaries.h"

#if LLVM_VERSION_CODE > LLVM_VERSION(3, 2)
#include"llvm/IR/Instruction.h"
#else
#include"llvm/Instruction.h"
#endif

#include<map>
#include<set>

namespace cse {
  
  class Sumbase {
  public:
    typedef std::map<PCINSTR, Summaries>::iterator MI;
    typedef std::map<const llvm::Function *, Summaries>::iterator FMI;

    // destructor shoud delete, automatically

    // better has a reset function

    // for fri
    static void registerInstrSummary(PCINSTR instr, const Summary *summary);
    static void registerSummary(PCINSTR head, PCINSTR tail,
				const Summary *summary);
    static void registerIncomplete(PCINSTR instr);

    static const Summary *getInstrSum(PCINSTR instr);
    static const Summary *getInstrSum(PCINSTR instr, const llvm::Value *corr);
    static Summaries *getInstrSums(PCINSTR instr);
    static Summaries *getSums(PCINSTR head);
    static bool isIncomplete(PCINSTR instr);

    static bool isKnown(PCINSTR head);
    
    static void addToKnownFuncs(const llvm::Function *func);
    static bool isKnown(const llvm::Function *func);

    static void registerToFuncModel(const llvm::Function *func,
				    const Summary *summary);
    static const Summary *getFromFunctionModel(const llvm::Function *func);

    static void prtSumsCount();

    static void dump();
    
  private:
    // objects in map, not sure if bad
    // final
    // each instruction can have one summary, except that br
    // (or similar control instr) can have more
    static std::map<PCINSTR, Summaries> instrSums;
    // final
    // code fragment summaries ready to use in symbolic execution
    static std::map<PCINSTR, Summaries> sums;
    // the map in the above set could be incomplete due to the undef
    // results from the solver, we record them
    static std::set<PCINSTR> incompletes;
    // functions known to have been instr summarised
    static std::set<const llvm::Function *> knownFuncs;

    // function models, access through the function models class, not here
    // it has nothing to do with known funcs, it only contains models for
    // undefined references
    static std::map<const llvm::Function *, Summaries> funcModels;

    Sumbase() {}
  };

}

#endif
