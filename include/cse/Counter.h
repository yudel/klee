#ifndef CSE_COUNTER_H
#define CSE_COUNTER_H

#define N_RECS 20

namespace cse {

  class Counter {
  public:
    static void reset(int no = 0);
    
    static void inc(int no = 0);
    static void dec(int no = 0);

    static unsigned int get(int no = 0);
    
  private:
    static unsigned int rs[N_RECS];
  };

}

#endif
