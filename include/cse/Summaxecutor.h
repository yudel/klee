#ifndef CSE_SUMMAXECUTOR_H
#define CSE_SUMMAXECUTOR_H

#include"cse/Context.h"
#include"cse/FunctionModels.h"
#include"cse/InterprocState.h"
#include"cse/Solvers.h"
#include"cse/StackFrame.h"
#include"cse/State.h"
#include"cse/Statistics.h"
#include"cse/Sumbase.h"

#include"klee/Config/Version.h"

#include"llvm/Analysis/LoopInfo.h"
#if LLVM_VERSION_CODE > LLVM_VERSION(3, 2)
#include"llvm/IR/Function.h"
#include"llvm/IR/Instruction.h"
#else
#include"llvm/Function.h"
#include"llvm/Instruction.h"
#endif
#include"llvm/Support/raw_ostream.h"

#include<set>
#include<stack>

namespace cse {
  
  class Summaxecutor {
  public:
  Summaxecutor()
    : context(NULL) {}
    
    // never need to use
    Summaxecutor(const Summaxecutor &summaxecutor) = delete;
    Summaxecutor &operator=(const Summaxecutor &other) = delete;

    ~Summaxecutor() { delete context; } // where is the module deleted?
    
    void init(const llvm::Module *module) {
      context = new Context(module);
      Statistics::init(context);
      functionModels.init(context);
    }

    bool summarise(llvm::Function *f);

    bool ready();
    // essentially still summarising, at a higher level
    void execute(llvm::Function *f);
    void finish() { Statistics::printCoverage(); }
    
  private:
    Context *context;
    FunctionModels functionModels;

    /*
     * Part 1, summarising instrs
     */
    /* A program consists of instructions; instructions have operands;
       those operands are values, which can be users, which can be
       instructions or constants; one kind of constants are constant
       expressions; instrs make use of values including other instrs,
       consts, and sometimes const exprs; when we summarise an instr
       one by one, regular consts are understood by the solver, but
       the const exprs still need to be summarised; instrs (all?) and
       const exprs are called operators. */
    void summariseInstr(const llvm::Instruction &instr);
    void summariseConstExpr(Summary *s, const llvm::Value *v);
    void summariseOperator(Summary *s, const llvm::Operator *op);
    /* Part 1, end */

    /*
     * Part 2, summarising blocks
     */
    // call stack
    std::stack<StackFrame> callStack;

    // direction now matters, because it involves not just
    // a single instruction but multiple ones
    void summariseBlock(llvm::Function *f);
    // forward reasoning implementation
    void forwardReasoningImpl(llvm::Function *f);

    void push();
    void pop();
    
    // directly visit the work stack of the top stack frame
    std::stack<State> *workStack();
    
    void setLoopInfo(llvm::LoopInfoBase<llvm::BasicBlock, llvm::Loop> *lib);
    // directly visit
    llvm::LoopInfoBase<llvm::BasicBlock, llvm::Loop> *loopInfo();
    
    bool isLoopHeader(const llvm::BasicBlock *bb);
    bool contains(const llvm::BasicBlock *bb);

    void init(State &state);
    void init(State &state, PCINSTR &instr);
    void quickInit(const llvm::BasicBlock *bb, PCINSTR &instr);
    
    /* Iteratively summarise a unit of code.
       A unit is a code fragment (could be of irregular shape).
       Each time we find a loop header or a call instr we issue
       a call to this function. As a result, a unit always begin
       with a loop header or a function entry. */
    /* Acutally when we see a call instr we call its wrapper
       function fri */
    // fri = forward reasoning impl
    void friProcessUnit(const llvm::BasicBlock *const first);
    /* Part 2, end */

    /*
     * Part 3, executing using summaries
     */
    std::stack<InterprocState> interprocWorkStack;
    // this is not used util we want to implement searchers
    std::set<State> states;

    /* Symbolically execute from an entry function */
    bool withinTL = true;
    
    void friExecute(llvm::Function *f);
    void terminate(InterprocState &state, bool partial = false);
    /* Symbolically execute from an instr, which means starting from
       instr, finding next node with summary/summaries and add it/them
       to the state's choice pool, then pushing the state back to the
       stack */
    void friExecuteFrom(InterprocState &state, PCINSTR instr);
    /* Part 3, end */
  };
  
}

#endif
