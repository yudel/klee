#ifndef CSE_VERSIONEDVALUE_H
#define CSE_VERSIONEDVALUE_H

#include"cse/VersionMap.h"

#include"klee/Config/Version.h"

#if LLVM_VERSION_CODE > LLVM_VERSION(3, 2)
#include"llvm/IR/Value.h"
#else
#include"llvm/Value.h"
#endif

#include<map>
#include<sstream>

namespace cse {

  // assumption: the lhs is the only variable that can be lifted in
  // a single instruction
  // note case like store, where there is no actual lhs and the memory
  // is lifted but the variables are not, so the assumption still holds
  class VersionedValue {
  public:
  VersionedValue(const llvm::Value *value, bool lifted, int memoryVersion)
    : value(value),
      version(lifted ? 1 : 0),
      lifted(lifted),
      memoryVersion(memoryVersion) {}

  VersionedValue(const llvm::Value *value, bool lifted)
    : value(value),
      version(lifted ? 1 : 0),
      lifted(lifted),
      memoryVersion(0) {}
    
  VersionedValue(const VersionedValue &vValue)
    : value(vValue.value),
      version(vValue.version),
      lifted(vValue.lifted),
      memoryVersion(vValue.memoryVersion) {}
    
  VersionedValue(const VersionedValue *vValue)
    : value(vValue->value),
      version(vValue->version),
      lifted(vValue->lifted),
      memoryVersion(vValue->memoryVersion) {}
    
    const llvm::Value *get() const { return value; }

    int getVersion() const { return version; }
    void setVersion(int version) {
      this->version = version;
    }

    bool isLifted() const { return lifted; }

    int getMemoryVersion() const { return memoryVersion; }
    void setMemoryVersion(int memoryVersion) {
      this->memoryVersion = memoryVersion;
    }

    void toVMapPre(VersionMap &vMap) {
      vMap.setPre(value, version, lifted, memoryVersion);
    }

    void toVMapPost(VersionMap &vMap) {
      vMap.setPost(value, version, lifted, memoryVersion);
    }

    void toVMapPreserving(VersionMap &vMap) const {
      vMap.setPreserving(value, version, lifted, memoryVersion);
    }

    unsigned int toInt() const;
    static bool getVersion(unsigned int intName,
			   std::pair<unsigned int, int> &res);
    std::string toString() const;

  private:
    // this will not be deleted upon free
    // ideally once a vv is constructed, it should not have
    // any relation with the v anymore
    const llvm::Value *value;
    // the type of each value is given by the value
    int version;
    bool lifted;

    // use different version numbers for different types of data
    // (if the value has anything to do with the memory, e.g., being a
    // pointer) the memory version is the memory's verion this value
    // is correpsonding to
    // it has nothing to do with lifted, which means the value itself
    // is changed or not
    // default 0
    int memoryVersion;
    /* Additional notes about memory version. In a more general way of
       speaking, memory could also an associated address space. Here we are
       assuming all memory objects are in the same address space. But Ideally,
       there could be additional info that tells us some of them are never
       close to the others. For example, (if) objects in different functions'
       stacks are never clashing, so that we can pretend that they are
       in different address spaces. This assumption could reduce the pressure
       on the solver who has to deal with the memory operation using array
       constraints. Multiple address spaces can be represented by multiple
       small arrays instead of one big array as in single address space.
       Currently the constraints on alloca and the concept of "clashing" is
       not well-defined, so we cannot do this now. */

    static unsigned int unique;
    static std::map<const llvm::Value *, unsigned int> ids;

    unsigned int getId() const;
    unsigned int registerId() const;
  };
  
}

#endif
