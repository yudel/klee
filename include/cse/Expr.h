#ifndef CSE_EXPR_H
#define CSE_EXPR_H

#include"cse/Common.h"
#include"cse/VersionMap.h"
#include"cse/VersionedValue.h"

#if LLVM_VERSION_CODE > LLVM_VERSION(3, 2)
#include"llvm/IR/InstrTypes.h"
#include"llvm/IR/Instructions.h"
#include"llvm/IR/Value.h"
#else
#include"llvm/InstrTypes.h"
#include"llvm/Instructions.h"
#include"llvm/Value.h"
#endif
#include"llvm/IR/Operator.h"
#include"llvm/Support/raw_ostream.h"

#include<cassert>

namespace cse {

  class Expr;
  class vValueIterator : public std::iterator<std::input_iterator_tag,
    VersionedValue> {
  public:
    // end
    vValueIterator();
    vValueIterator(const Expr *expr);
    // no checking, careful
    vValueIterator(const Expr *expr, VersionedValue *vValue);
    vValueIterator(const vValueIterator &vvi);

    vValueIterator &operator++();
    vValueIterator operator++(int);

    bool operator==(const vValueIterator &rhs);
    bool operator!=(const vValueIterator &rhs);

    VersionedValue &operator*() const;
    VersionedValue *operator->() const;
    
  private:
    static const int VVI_INVALID = -1;
    const Expr *expr;
    VersionedValue *vValue;
    int id;

    void pointToEnd();
  };

  /* I probably should call this a "fact" instead of a expression.
     It's directly used as a constraint. */
  // abstract, sh**** cpp
  class Expr {
  public:
    enum Kind {
      // Unary
      IS_TRUE,
      IS_FALSE,

      // Equality
      EQ,
      NEQ, // for switch default

      // Equation
      // actually they all have 3 operands
      EQUATION_BEGIN,
      // Binary operation
      ADD, FADD, SUB, FSUB,
      MUL, FMUL,
      UDIV, SDIV, FDIV,
      UREM, SREM, FREM,

      // Bitwise binary
      SHL, LSHR, ASHR, // for the three shifting operation,
                       // op2 is treated as an unsigned int
      AND, OR, XOR, // these are bitwise as well
      EQUATION_END,

      // Allocation
      ALLOCA,

      // Accessing operation
      LOAD, STORE,

      // Addressing operation
      GEP,

      // Casting
      CASTING_BEGIN,
      TRUNC, ZEXT, SEXT,
      FPTRUNC, FPEXT,
      FP_UI, FP_SI, UI_FP, SI_FP,
      PTR_INT, INT_PTR,
      BC,
      CASTING_END,

      // Comparison
      CMP,

      // ITE
      ITE // for select instr
    };

    Kind kind;

    virtual ~Expr() { delete vValue; }

    Expr *copy() const;

    Kind getKind() const { return kind; }
    
    VersionedValue *getOp0() const { return vValue; }

    unsigned int getNumOfVValues() const { return num; }

    bool isMemoryLifted() const { return memoryLifted; }

    PCINSTR getInstr() const { return instr; }

    virtual vValueIterator &getVValueIterator() const;
    vValueIterator &end() const;

    virtual void dump() const;

  protected: // to make it look like an abstract class
  Expr(Kind kind, VersionedValue *vValue)
    : kind(kind),
      vValue(vValue),
      num(1),
      memoryLifted(false),
      instr(NULL) {}

  Expr(Kind kind, VersionedValue *vValue, int num)
    : kind(kind),
      vValue(vValue),
      num(num),
      memoryLifted(false),
      instr(NULL) {}

  Expr(Kind kind, VersionedValue *vValue, PCINSTR instr)
    : kind(kind),
      vValue(vValue),
      num(1),
      memoryLifted(false),
      instr(instr) {}

  Expr(Kind kind, VersionedValue *vValue, int num, PCINSTR instr)
    : kind(kind),
      vValue(vValue),
      num(num),
      memoryLifted(false),
      instr(instr) {}

  Expr(Kind kind, VersionedValue *vValue, bool memoryLifted)
    : kind(kind),
      vValue(vValue),
      num(1),
      memoryLifted(memoryLifted),
      instr(NULL) {}

  Expr(Kind kind, VersionedValue *vValue, int num, bool memoryLifted)
    : kind(kind),
      vValue(vValue),
      num(num),
      memoryLifted(memoryLifted),
      instr(NULL) {}

  Expr(const Expr& expr)
    : kind(expr.kind),
      vValue(expr.vValue ? new VersionedValue(expr.vValue) : NULL),
      num(expr.num),
      memoryLifted(expr.memoryLifted),
      instr(expr.instr) {}

    VersionedValue *vValue;
    // the number of versioned values in this instruction
    unsigned int num;
    // memories of adjacent versions's difference is cause by
    // only one instruction
    bool memoryLifted;
    // should have another variable specifying which memory version(s)
    // the expr belongs to (is based on), done in vv

    // original instr that produces this constraint, if there is one
    // usually it is the same thing in the v value, but not must
    // we don't need it often and it's default null
    // we will set it when we need it, and at that time it's usually different
    // from the one in the v value
    PCINSTR instr;
  };

  /* This expression contains only one value */
  // this one is used only for conditional branches currently
  class UnaryExpr : public Expr {
  public:
  UnaryExpr(Kind kind, const llvm::Value *cond, PCINSTR instr)
    : Expr(kind, new VersionedValue(cond, false), instr) {
      assert((kind == IS_TRUE || kind == IS_FALSE)
	     && "Expression kind error");
    }

    UnaryExpr(const UnaryExpr &ue);
    
    ~UnaryExpr() {}

    void dump() const;
  };

  /* This is the simple constraint des = src, used between function
     parameters and arguments (order matters) in function calls,
     return and returned values (order matters) in function returns,
     phi values and choices (order matters) in phi nodes,
     values and cases in switch instructions,
     or in other situations where you need equality between two values */
  /* This one is special in that it's also used outside of instr
     summarisation phase, see util */
  class EqualityExpr : public Expr {
  public:
  EqualityExpr(const llvm::Value *des, const llvm::Value *src)
    : Expr(EQ, new VersionedValue(des, true), 2),
      src(new VersionedValue(src, false)) {}
    
  EqualityExpr(const llvm::Value *des, bool liftDes, const llvm::Value *src,
	       PCINSTR instr)
    : Expr(EQ, new VersionedValue(des, liftDes), 2, instr),
      src(new VersionedValue(src, false)) {}
    
    EqualityExpr(const EqualityExpr &ee);

    ~EqualityExpr() { delete src; }

    VersionedValue *getSrc() const { return src; }

    void dump() const;

  private:
    VersionedValue *src;
  };

  /* This expression has the format of v != op1[ && v != opi]* (i <= n) */
  // implicit, n >= 1
  class InequalityExpr : public Expr {
  public:
  InequalityExpr(const llvm::Value *v, unsigned int n, PCINSTR instr)
    : Expr(NEQ, new VersionedValue(v, false), int(n + 1), instr),
      ops(new VersionedValue *[n]) {}

    InequalityExpr(const InequalityExpr &ie);

    ~InequalityExpr() {
      for (unsigned int i = 0; i < num - 1; i++)
	delete ops[i];
      delete ops;
    }

    unsigned int getNumOfIeqs() const { return getNumOfVValues() - 1; }
    
    VersionedValue **getOps() const { return ops; }
    void setOp(unsigned int i, const llvm::Value *op) {
      if (i < num - 1)
	ops[i] = new VersionedValue(op, false); // not deleted old
      else
	llvm::errs() << "Error adding op\n";
    }

    void dump() const;

  private:
    VersionedValue **ops;
  };

 /* This expression has the format of res = op1 <op> op2 */
  class EquationExpr : public Expr {
  public:
  EquationExpr(Kind kind, const llvm::Value *res,
	       llvm::Value *op1, llvm::Value *op2)
    : Expr(kind, new VersionedValue(res, true), 3),
      op1(new VersionedValue(op1, false)),
      op2(new VersionedValue(op2, false)) {
	assert((kind > EQUATION_BEGIN && kind < EQUATION_END)
	       && "Experssion kind error");
    }

    EquationExpr(const EquationExpr &ee);
    
    ~EquationExpr() { delete op1; delete op2; }

    VersionedValue *getOp1() const { return op1; }
    VersionedValue *getOp2() const { return op2; }

    void dump() const;
    
  private:
    VersionedValue *op1;
    VersionedValue *op2;
  };

  /* This expression is declaring valid memory location.
     The precondition is that the memory have free space?
     It has the format of res = alloca ty (of size)[, arraySize] */
  class Allocation : public Expr {
  public:
  Allocation(const llvm::Value *res, unsigned int size)
    : Expr(ALLOCA, new VersionedValue(res, true)),
      arraySpecified(false),
      size(size),
      arraySize(NULL) {}

  Allocation(const llvm::Value *res, unsigned int size,
	     const llvm::Value *arraySize)
    : Expr(ALLOCA, new VersionedValue(res, true), 2),
      arraySpecified(true),
      size(size),
      arraySize(new VersionedValue(arraySize, false)) {}

    Allocation(const Allocation &alloca);
    
    ~Allocation() { delete arraySize; }

    unsigned int getSize() const { return size; }

    bool isArray() const { return arraySpecified; }

    VersionedValue *getArraySize() const { return arraySize; }

    void dump() const;
    
  private:
    bool arraySpecified;
    unsigned int size;
    VersionedValue *arraySize;
  };

  /* Memory accessing includes two operations. 
     For load, the constraint is des == *valueP,
     and for store, the constraint is src == *valueP,
     also that valueP points to a valid location */
  class AccessingOperation : public Expr {
  public:
  AccessingOperation(Kind kind,
		     const llvm::Value *value,
		     const llvm::Value *valueP)
    // the value doesn't point to the instruction in the case of store
    : Expr(kind, kind == LOAD ? new VersionedValue(value, true) : NULL,
	   3, kind == STORE),
      // the whole memory should be lifted in the case of store
      desOrSrc(kind == LOAD ? vValue : new VersionedValue(value, false)),
      // but the pointer itself is not changed
      ptr(new VersionedValue(valueP, false, kind == STORE ? 1 : 0)) {
      assert((kind == LOAD || kind == STORE)
	     && "Expression kind error");
    }

    AccessingOperation(const AccessingOperation &ao);
    
    ~AccessingOperation() {
      if (kind == STORE)
	delete desOrSrc;
      delete ptr;
    }

    virtual vValueIterator &getVValueIterator() const;

    VersionedValue *getPtr() const { return ptr; }

    void dump() const;
    
  private:
    VersionedValue *desOrSrc;
    VersionedValue *ptr;
  };

  /* This doesn't truely visit the memory, so there is no
     need to put validity constraints. The constraint format
     is res == ptr + sizeof(type) * idx0[ + sizeof(subtypei) * idxi]* */
  class AddressingOperation : public Expr {
  public:
  AddressingOperation(const llvm::GetElementPtrInst *res,
		      const llvm::Value *ptr, const llvm::Type *targetType)
    : Expr(GEP, new VersionedValue(llvm::cast<const llvm::Value>(res), true),
	   int(2 + res->getNumIndices())),
      ptr(new VersionedValue(ptr, false)),
      targetType(targetType) {
	for (llvm::User::const_op_iterator opi = res->idx_begin(),
	       opj = res->idx_end(); opi != opj; opi++) {
	  VersionedValue *idx = new VersionedValue(opi->get(), false);
	  indices.push_back(idx);
	}
      }

  AddressingOperation(const llvm::GEPOperator *res,
		      const llvm::Value *ptr, const llvm::Type *targetType)
    : Expr(GEP, new VersionedValue(llvm::cast<const llvm::Value>(res), true),
	   int(2 + res->getNumIndices())),
      ptr(new VersionedValue(ptr, false)),
      targetType(targetType) {
	for (llvm::User::const_op_iterator opi = res->idx_begin(),
	       opj = res->idx_end(); opi != opj; opi++) {
	  VersionedValue *idx = new VersionedValue(opi->get(), false);
	  indices.push_back(idx);
	}
      }

    // unused
  AddressingOperation(const llvm::Value *res,
		      const llvm::Value *ptr, const llvm::Type *targetType,
		      const llvm::Value *idx1, const llvm::Value *idx2)
    : Expr(GEP, new VersionedValue(res, true), 4),
      ptr(new VersionedValue(ptr, false)),
      targetType(targetType) {
	indices.push_back(new VersionedValue(idx1, false));
	indices.push_back(new VersionedValue(idx2, false));
      }

    AddressingOperation(const AddressingOperation &ao);
    
    ~AddressingOperation() {
      delete ptr;
      for (std::vector<VersionedValue *>::iterator
	     i = indices.begin(), j = indices.end(); i != j; i++) {
	delete *i;
      }
      // vector should be deleted automatically
    }

    VersionedValue *getPtr() const { return ptr; }

    const llvm::Type *getTargetType() const { return targetType; }

    // potentially slow because of searching for position?
    // exception
    VersionedValue *getIdx(unsigned int i) const { return indices.at(i); }

    void dump() const;
    
  private:
    VersionedValue *ptr;
    // used to calculate the number of shifted bytes
    const llvm::Type *targetType;
    std::vector<VersionedValue *> indices;
  };

  /* This expression has the format of res = (ty) orig */
  class CastingExpr : public Expr {
  public:
  CastingExpr(Kind kind, const llvm::Value *res,
	      const llvm::Type *type, const llvm::Value *orig)
    : Expr(kind, new VersionedValue(res, true), 2),
      type(type),
      orig(new VersionedValue(orig, false)) {
      assert((kind > CASTING_BEGIN && kind < CASTING_END)
	     && "Expression kind error");
    }

    CastingExpr(const CastingExpr &ce);
    
    ~CastingExpr() { delete orig; }

    VersionedValue *getOrig() const { return orig; }

    void dump() const;
    
  private:
    const llvm::Type *type; // this is pretty useless
    VersionedValue *orig;
  };

  /* This expression has the format of res = op1 <cmp> op2 */
  class ComparisonExpr : public Expr {
  public:
  ComparisonExpr(const llvm::CmpInst *res,
		 llvm::Value *op1, llvm::Value *op2)
    : Expr(CMP, new VersionedValue(res, true), 3),
      predicate(res->getPredicate()),
      op1(new VersionedValue(op1, false)),
      op2(new VersionedValue(op2, false)) {}

    ComparisonExpr(const ComparisonExpr &ce);
    
    ~ComparisonExpr() { delete op1; delete op2; }

    llvm::CmpInst::Predicate getPredicate() const { return predicate; }

    VersionedValue *getOp1() const { return op1; }
    VersionedValue *getOp2() const { return op2; }

    void dump() const;
    
  private:
    const llvm::CmpInst::Predicate predicate;
    VersionedValue *op1;
    VersionedValue *op2;
  };

  /* This expression has the format of res = cond ? op1 : op2 */
  class ITEExpr : public Expr {
  public:
  ITEExpr(const llvm::Value *res, const llvm::Value *cond,
	  const llvm::Value *op1, const llvm::Value *op2)
    : Expr(ITE, new VersionedValue(res, true), 4),
      cond(new VersionedValue(cond, false)),
      op1(new VersionedValue(op1, false)),
      op2(new VersionedValue(op2, false)) {}

    ITEExpr(const ITEExpr &itee);

    ~ITEExpr() { delete cond; delete op1; delete op2; }

    VersionedValue *getCond() const { return cond; }
    VersionedValue *getOp1() const { return op1; }
    VersionedValue *getOp2() const { return op2; }

    void dump() const;
    
  private:
    VersionedValue *cond;
    VersionedValue *op1;
    VersionedValue *op2;
  };

  // Failure and success expression
  /* Failure expression can be used for any instruction that can produce
     runtime error. But first of all, there also needs to be a success
     expression. Failure and success expressions can be used to produce
     summaries when inserted into both the ecs and pcs, in addition to the
     regular expressions. Without these kinds of expressions, the se will
     run indiscriminately when coming across such instructions. */
  
} // end of cse namespace

#endif
