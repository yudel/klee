#ifndef CSE_STATE_H
#define CSE_STATE_H

#include"cse/Counter.h"
#include"cse/Solvers.h"
#include"cse/Summary.h"

#if LLVM_VERSION_CODE > LLVM_VERSION(3, 2)
#include"llvm/IR/Function.h"
#else
#include"llvm/Function.h"
#endif
#include"llvm/Support/raw_ostream.h"

#include<set>

namespace cse {

  struct Choice {
    mutable bool visited;
    mutable SolverResult slvRes; // no init value
    mutable PCINSTR via;
    PCINSTR target;
    // for interproc states and switch only
    // directly from sum base, no deletion
    const Summary *summary;

  Choice(PCINSTR target) // for comparison only
  : visited(false), via(NULL), target(target), summary(NULL) {}
  Choice(PCINSTR via, PCINSTR target)
  : visited(false), via(via), target(target), summary(NULL) {}
  Choice(PCINSTR via, PCINSTR target, const Summary *summary)
  : visited(false), via(via), target(target), summary(summary) {}

    bool operator==(const Choice &other) const;
    bool operator<(const Choice &other) const;
    bool operator>(const Choice &other) const;

    void setSlvRes(SolverResult slvRes) const;
  };
  
  class State {
  public:
    typedef std::set<Choice>::iterator SI;

  State(Context *context)
    // not initialised here
    : summary(NULL),
      virtualHead(NULL),
      virtualTail(NULL),
      nextInstr(NULL),
      numOfChoices(0),
      availChoices(0),
      choicePool(std::set<Choice>()),
      lastChoice(choicePool.end()),
      allChosenFailed(true),
      solver(Solver::create(context)),
      restorePoint(solver->getRestorePoint()),
      genned(false) {}
    
  State(Context *context, Summary *s)
    : summary(s),
      virtualHead(s ? s->getHead() : NULL),
      virtualTail(s ? s->getTail() : NULL),
      nextInstr(NULL),
      numOfChoices(0),
      availChoices(0),
      choicePool(std::set<Choice>()),
      lastChoice(choicePool.end()),
      allChosenFailed(true),
      solver(Solver::create(context)),
      restorePoint(solver->getRestorePoint()),
      genned(false) {}

  State(Context *context, PCINSTR nextInstr)
    : summary(NULL),
      virtualHead(NULL),
      virtualTail(NULL),
      nextInstr(nextInstr), // with extra knowledge about the next job
      numOfChoices(0),
      availChoices(0),
      choicePool(std::set<Choice>()),
      lastChoice(choicePool.end()),
      allChosenFailed(true),
      solver(Solver::create(context)),
      restorePoint(solver->getRestorePoint()),
      genned(false) {}

  State(Context *context, Solver *solver)
    : summary(NULL),
      virtualHead(NULL),
      virtualTail(NULL),
      nextInstr(NULL),
      numOfChoices(0),
      availChoices(0),
      choicePool(std::set<Choice>()),
      lastChoice(choicePool.end()),
      allChosenFailed(true),
      solver(solver), // if NULL, you must make sure it gets one later
      restorePoint(0),
      genned(false) {}

  State(const State &state)
    : summary(state.summary ? new Summary(state.summary) : NULL),
      virtualHead(state.virtualHead),
      virtualTail(state.virtualTail),
      nextInstr(state.nextInstr),
      covered(state.covered),
      numOfChoices(state.numOfChoices),
      availChoices(state.availChoices),
      choicePool(state.choicePool),
      lastChoice(choicePool.end()),
      allChosenFailed(state.allChosenFailed),
      solver(state.solver),
      restorePoint(state.restorePoint),
      genned(state.genned) {}

    State &operator=(const State &other) {
      if (&other == this)
	// this is bad, other might be freed, together with its summary
	return *this;
      if (this->summary)
	delete this->summary;
      this->summary = other.summary ? new Summary(other.summary) : NULL;
      this->virtualHead = other.virtualHead;
      this->virtualTail = other.virtualTail;
      this->nextInstr = other.nextInstr;
      this->covered = other.covered;
      this->numOfChoices = other.numOfChoices;
      this->availChoices = other.availChoices;
      this->choicePool = other.choicePool;
      this->lastChoice = this->choicePool.end();
      this->allChosenFailed = other.allChosenFailed;
      this->solver = other.solver; // old solver not deleted
      this->restorePoint = other.restorePoint;
      this->genned = other.genned;
      return *this;
    }
    
    virtual ~State () { delete summary; } // solver not deleted, need pop
    
    // this is syntactically like assignment operator, however don't
    // mix up with assignment and copy constructor, because it's different
    void copyFrom(const State &other) {
      if (this->summary)
	delete this->summary;
      this->summary = other.summary ? new Summary(other.summary) : NULL;
      this->virtualHead = other.virtualHead;
      this->virtualTail = other.virtualTail;
      this->nextInstr = other.nextInstr;
      this->covered = other.covered;
      // different with copy constructor, choice resetting
      this->numOfChoices = 0;
      this->availChoices = 0;
      this->choicePool = std::set<Choice>();
      this->lastChoice = this->choicePool.end();
      this->allChosenFailed = true;
      // difference ends
      this->solver = other.solver; // old solver not deleted
      this->restorePoint = other.restorePoint;
      this->genned = other.genned;
    }

    void reset() {
      if (this->summary)
	delete this->summary;
      this->summary = NULL;
      this->virtualHead = NULL;
      this->virtualTail = NULL;
      this->nextInstr = NULL;
      this->covered.clear();
      this->numOfChoices = 0;
      this->availChoices = 0;
      this->choicePool.clear();
      this->lastChoice = this->choicePool.end();
      this->allChosenFailed = true;
      // reuse the old context
      // old solver not deleted
      this->solver = Solver::create(this->solver->context);
      this->restorePoint = solver->getRestorePoint();
      this->genned = false;
    }

    /* Topological info */
    PCINSTR getVirtualHead();
    PCINSTR getVirtualTail();
    PCINSTR getHead();
    PCINSTR getTail();
    // in forward reasoning, a state must have a virtual head
    void setVirtualHead(PCINSTR instr) { virtualHead = instr; }

    void cover(PCINSTR instr);

    /* Logical info/operation */
    bool empty() { return summary == NULL; }

    PCINSTR getNextInstr() { return nextInstr; }
    void setNextInstr(PCINSTR nextInstr) { this->nextInstr = nextInstr; }
    
    bool isAllChosen();
    int getAvail() { return availChoices; }
    int getTotal() { return numOfChoices; }
    void addChoice(PCINSTR via, PCINSTR target);
    void addChoice(PCINSTR via, PCINSTR target, const Summary *summary);
    // visit one according to strategy
    // theoretically at least one choice is feasible at each branch
    // temporarily forward reasoning
    State visit(bool &res, bool &clear);
    State visit(bool &res, PCINSTR target);

    void deepen() { summary->deepen(); }
    
    Summary *getSummary();

    void consumeEmpty(PCINSTR instr, PCINSTR nextInstr);
    void consume(const Summary *next, PCINSTR nextInstr,
		 bool preOrPost = false);

    /* Solver management */
    Solver *getSolver() { return solver; } // debug
    unsigned int getRestorePoint() { return restorePoint; } // debug
    
    void createRestorePoint();
    void backtrack();
    bool check(); // not used
    // generate model, only used after the checking successes
    void gen();

    void solverDump();

    /* General */
    void registerSummary();

    void statsDump();

    void dump();
    
  protected:
    /* Summary info */
    // each state has a unique copy of summary, even though
    // what it represents could overlap with those in other states
    Summary *summary;
    PCINSTR virtualHead;
    PCINSTR virtualTail;
    // next instr to be processed
    // not always clear what is the next
    PCINSTR nextInstr;
    // records the covered instrs in case summary is not created
    // like if an instr has empty summary, it wouldn't be recorded as covered
    // in the summary, but will be recorded here if the state visits it
    std::vector<PCINSTR> covered;

    /* Choice pool */
    int numOfChoices;
    int availChoices;
    // vector or set?
    std::set<Choice> choicePool;
    // only used for speeding up search
    SI lastChoice;
    bool allChosenFailed;

    /* Solver info */
    Solver *solver;
    // recording the solver's scope level so that it knows where to
    // backtrack to when visiting this state
    // when processing a state, its retorePoint MUST equals to the solver's
    unsigned int restorePoint;
    bool genned;

    // should only exist in interprocedural state, the general state
    // shouldn't maintain it, it's in the summary
    void tempSaveCovered(PCINSTR instr) {} // unimplemented

    bool hasOnlyChoice();
    const Choice *visitChoice();
    void setSlvRes(const Choice *c, SolverResult slvRes);
    const Choice *visitChoice(PCINSTR target);

    // these functions are used together
    Summary *getNewSummary(const Summary *next);
    bool toSolver(const Summary *newSummary);
    // this deletes the old summary
    void updateSummary(Summary *newSummary);
    void update(Summary *newSummary, PCINSTR nextInstr);
  };

}

#endif
