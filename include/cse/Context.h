#ifndef CSE_CONTEXT_H
#define CSE_CONTEXT_H

#include"cse/Common.h"

#include"klee/Config/Version.h"

#include"llvm/IR/Module.h"
#if LLVM_VERSION_CODE <= LLVM_VERSION(3, 1)
#define DATALAYOUT TargetData
#include"llvm/Target/TargetData.h"
#else
#define DATALAYOUT DataLayout
#include"llvm/IR/DataLayout.h"
#endif

namespace cse {

  class Context {
  public:
    const llvm::Module *module;
    const llvm::DATALAYOUT *dl;
    
  Context(const llvm::Module *module)
    : module(module),
      dl(new llvm::DATALAYOUT(module)),
      moduleName(module->getName()) {}

    ~Context() { delete dl; }

    std::string getName() const {
      return moduleName.str();
    }

    unsigned int getMachineWordSize() {
      return dl->getPointerSizeInBits();
    }
    unsigned int getModelWordSize() {
      if (PTR_SIZE)
	return PTR_SIZE;
      else
	return dl->getPointerSizeInBits();
    }

  private:
    // program under test name
    llvm::StringRef moduleName;
  };

}

#endif
