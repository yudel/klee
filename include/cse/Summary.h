#ifndef CSE_SUMMARY_H
#define CSE_SUMMARY_H

#include"cse/CIStackFrame.h"
#include"cse/Common.h"
#include"cse/Expr.h"

#include<stack>
#include<vector>

#define SUM_BOP multiply
#define SUM_BOP_REVERSE divide

namespace cse {
  
  typedef Expr Constraint; // alias
  typedef std::vector<Expr *> Constraints; // alias

  // could use subclasses
  class Summary {
  public:
  Summary(PCINSTR instr)
    : length(0),
      head(instr), tail(instr),
      correspondent(NULL),
      eCCount(0), pcCount(0) {
      covered.push_back(instr);
    }

  Summary(unsigned int length, PCINSTR head, PCINSTR tail, PCINSTR corr)
    : length(length),
      head(head), tail(tail),
      correspondent(corr),
      eCCount(0), pcCount(0) {
      if (head == tail)
	covered.push_back(head);
      // the rest should be inserted using the cover function in order
      // not safe to do it here
    }

    // create an empty summary with the info provided by the state
    // used to describe a small code fragment that reduces to no constraint
    // the code fragment must have no length, i.e., not including conditional
    // terminator and if it includes a call, it must be at the end and
    // have empty summary, no other special instr can be included, e.g., ret
    // and phi
  Summary(PCINSTR newHead, PCINSTR newTail, std::vector<PCINSTR> &covered)
    : length(0),
      head(newHead), tail(newTail),
      correspondent(NULL),
      covered(covered),
      eCCount(0), pcCount(0) {}

    Summary(const Summary *s);
    // copy with some updated info
    // used in registering, the updated info doesn't invalid the others
    // for safety, don't make irregular use like relate tail with corr
    // which is not supposed to happen by definition
    Summary(const Summary *s, PCINSTR newHead, PCINSTR newTail);

    ~Summary();

    /* Topological info */
    unsigned int getLength() const { return length; }
    void deepen() { length++; } // when seeing a conditional br (or a call..)

    PCINSTR getHead() const { return head; }
    PCINSTR getTail() const { return tail; }

    void setHead(PCINSTR head) { this->head = head; }
    void setTail(PCINSTR tail) { this->tail = tail; }

    PCINSTR getCorr() const { return correspondent; }
    void setCorr(PCINSTR corr) { correspondent = corr; }

    const std::vector<PCINSTR> &getCovered() const { return covered; }
    void cover(PCINSTR instr) {	covered.push_back(instr); }
    // insert at the beginning
    void cover(std::vector<PCINSTR>::const_iterator begin,
	       std::vector<PCINSTR>::const_iterator end) {
      covered.insert(covered.begin(), begin, end);
    }
    
    std::string prepareAssumption(PCINSTR instr, PCINSTR &choice) const;

    /* Logical info/operations */
    const Constraints *getEC() const { return &entryCond; }
    const Constraints *getPc() const { return &postcond; }
    // because pc is always ec && additional
    const Constraints getAdditional(unsigned int from) const;

    /* This function adds a constraint to pc.
       The boolean is specified by the direction of search.
       It has nothing to do with ec or pc */
    // the constraint should be a newed one, no reusing
    void addConstraint(Constraint *c, bool preOrPost);
    /* Different from the above one, this function also adds the
       constraint to the ec. One can see that ec is always a subset of pc
       in terms of the constraints they have */
    void addConstraintBoth(Constraint *c, bool preOrPost);

    // does the order matter? so far it doesn't
    /* This two function differ in that they store constraints differently,
       but logically should be equivalent to the above two */
    void addConstraintFront(Constraint *c, bool preOrPost) {}
    void addConstraintFrontBoth(Constraint *c, bool preOrPost) {}

    void addConstraint(Constraint *c);
    void addConstraintBoth(Constraint *c);

    unsigned int getECCount() const { return eCCount; }
    unsigned int getPcCount() const { return pcCount; }

    const VersionMap *getVMap() const { return &versionMap; }

    bool isEmpty() const;
    bool isECImdTrue() const;
    bool isPcImdTrue() const;

    unsigned int getECLength() const;
    
    static Summary *SUM_BOP(const Summary *op1, const Summary *op2,
			    bool preOrPost);
    // useless until we want to reduce space consumption
    // to remove the last concatenated summary
    static Summary *SUM_BOP_REVERSE(const Summary *op1, bool preOrPost);

    std::stack<CIStackFrame> &getCallStack();
    PCINSTR getLastCallInstr() const;
    llvm::Function *getLastCalledFunction() const;
    llvm::Function *getLastCalledFunction(PCINSTR callInstr) const;
    void push(const llvm::Function *callee, PCINSTR callInstr);
    PCINSTR pop();

    /* General */
    void statsDump() const;

    void dump() const;
    
  private:
    /* Topological info */
    unsigned int length;
    PCINSTR head;
    PCINSTR tail;
    // instruction used by e.g., a terminator to indicate the successor
    // one doesn't always have it
    PCINSTR correspondent;
    // also a complementary vector is stored in state
    std::vector<PCINSTR> covered;

    void cover(const Summary *op1, const Summary *op2);

    /* Logical info/operations */
    // a summary is just like several constraints (for both ec and pc)
    // every constraint in ec must have an entry from pc
    unsigned int eCCount;
    Constraints entryCond;
    unsigned int pcCount;
    Constraints postcond;
    // seems one version map is enough
    VersionMap versionMap;
    // summarises the call stack of the summarised code fragment
    // cannot be "minus", which means it only goes deeper;
    // in instr or block summarisation, stack size is at most 1;
    // note that we don't "solve" this stack, eventually the path constraints
    // will look like they are from a one big function with a lot of inlined
    // functions (if any); this stack is only effective during the building
    // of the constraints
    std::stack<CIStackFrame> callStack;

    void addConstrECNoUpdateAtAll(Constraint *c);
    
    void addConstrECNoUpdate(Constraint *c, bool preOrPost,
			     const VersionMap &another);
    void addConstrPcNoUpdate(Constraint *c, bool preOrPost,
			     const VersionMap &another);

    void updateVVOnly(Constraint *c, bool preOrPost,
		      const VersionMap &another);
    void updateVMapValuePreserving(const Constraint *c);
    void updateVMapMemoryPreserving(const VersionMap &vMap);

    void updateVMap(Constraint *c, bool preOrPost);

    // forward reasoning
    void updateCallStack(const Summary *other);
  };

}

#endif
