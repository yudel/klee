//===-- ExecutionState.h ----------------------------------------*- C++ -*-===//
//
//                     The KLEE Symbolic Virtual Machine
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#ifndef KLEE_EXECUTIONSTATE_H
#define KLEE_EXECUTIONSTATE_H

#include "/home/lenovo/klee_workspace/lib/PortableStats.h"

#include "/home/lenovo/klee/lib/Core/Memory.h" // hack

#include "klee/Constraints.h"
#include "klee/Expr.h"
#include "klee/Internal/ADT/TreeStream.h"

// FIXME: We do not want to be exposing these? :(
#include "../../lib/Core/AddressSpace.h"
#include "klee/Internal/Module/KInstIterator.h"
#include "klee/Internal/Module/KInstruction.h"

#if LLVM_VERSION_CODE >= LLVM_VERSION(3, 3)
#include "llvm/IR/Instructions.h"
#else
#include "llvm/Instructions.h"
#endif

#include <map>
#include <set>
#include <vector>
#include <queue>

namespace klee {
class Array;
class CallPathNode;
struct Cell;
struct KFunction;
struct KInstruction;
class MemoryObject;
class PTreeNode;
struct InstructionInfo;

llvm::raw_ostream &operator<<(llvm::raw_ostream &os, const MemoryMap &mm);

  struct ObjectSnapshot {
    const MemoryObject *mo;
    ObjectState *os;
    const Array *replacedBy;

    ObjectSnapshot(const MemoryObject *mo, ObjectState *os,
		   const Array *replacedBy)
      : mo(mo), os(os), replacedBy(replacedBy) {
      mo->refCount++;
      os->refCount++;
    }
    ObjectSnapshot(const ObjectSnapshot &other)
      : mo(other.mo), os(other.os), replacedBy(other.replacedBy) {
      mo->refCount++;
      os->refCount++;
    }
    ObjectSnapshot &operator=(const ObjectSnapshot &other) {
      if (this != &other) {
	if (mo != other.mo) {
	  if (mo->refCount > 0)
	    mo->refCount--;
	  if (mo->refCount == 0)
	    delete mo;

	  mo = other.mo;
	  mo->refCount++;
	}
	if (os != other.os) {
	  if (os->refCount > 0)
	    os->refCount--;
	  if (os->refCount == 0)
	    delete os;

	  os = other.os;
	  os->refCount++;
	}
	replacedBy = other.replacedBy;
      }
      return *this;
    }

    ~ObjectSnapshot() {
      if (mo->refCount > 0)
	mo->refCount--;
      if (mo->refCount == 0)
	delete mo;
      if (os->refCount > 0)
	os->refCount--;
      if (os->refCount == 0)
	delete os;
    }
  };
  
struct StackFrame {
  static unsigned int assigned;
  unsigned int id;
  KInstIterator caller;
  KFunction *kf;
  CallPathNode *callPathNode;

  std::vector<const MemoryObject *> allocas;
  Cell *locals;

  /// Minimum distance to an uncovered instruction once the function
  /// returns. This is not a good place for this but is used to
  /// quickly compute the context sensitive minimum distance to an
  /// uncovered instruction. This value is updated by the StatsTracker
  /// periodically.
  unsigned minDistToUncoveredOnReturn;

  // For vararg functions: arguments not passed via parameter are
  // stored (packed tightly) in a local (alloca) memory object. This
  // is setup to match the way the front-end generates vaarg code (it
  // does not pass vaarg through as expected). VACopy is lowered inside
  // of intrinsic lowering.
  MemoryObject *varargs;

  // a copy of the memory objects that later should be restored
  std::vector<ObjectSnapshot> addressSpaceRecord;

  StackFrame(KInstIterator caller, KFunction *kf);
  StackFrame(const StackFrame &s);
  ~StackFrame();

  StackFrame &operator=(const StackFrame &other);
};

  struct UnresolvedCaller {
    // the stack frames from main to the caller's parent function
    typedef std::vector<StackFrame> stack_ty;
    stack_ty sfs;
    KInstIterator caller;
    ref<Expr> symRetVal;
    typedef std::vector<ObjectSnapshot> oss_ty;
    oss_ty replaced;

    UnresolvedCaller() {}
    UnresolvedCaller(KInstIterator caller) // for comparison only
      : caller(caller), symRetVal(0) {}
    UnresolvedCaller(KInstIterator caller,
		     ref<Expr> symRetVal, oss_ty replaced)
      : caller(caller), symRetVal(symRetVal), replaced(replaced) {}
    UnresolvedCaller(const UnresolvedCaller &uc)
      : sfs(uc.sfs), caller(uc.caller),
	symRetVal(uc.symRetVal), replaced(uc.replaced) {
      stack_ty::const_iterator othersi = uc.sfs.begin();
      for (stack_ty::iterator si = sfs.begin(), sj = sfs.end();
	   si != sj; ++si) {
	si->addressSpaceRecord = othersi->addressSpaceRecord;
	++othersi;
      }
    }
    UnresolvedCaller &operator=(const UnresolvedCaller &other) {
      if (&other == this)
	return *this;
      this->sfs = other.sfs;
      stack_ty::const_iterator othersi = other.sfs.begin();
      for (stack_ty::iterator si = sfs.begin(), sj = sfs.end();
	   si != sj; ++si) {
	si->addressSpaceRecord = othersi->addressSpaceRecord;
	++othersi;
      }
      this->caller = other.caller;
      this->symRetVal = other.symRetVal;
      this->replaced = other.replaced;
      return *this;
    }

    operator bool() const {
      return caller;
    }
    bool operator==(const UnresolvedCaller &other) const {
      return caller == other.caller;
    }
    bool operator!=(const UnresolvedCaller &other) const {
      return caller != other.caller;
    }

    void recordStackFrame(stack_ty::iterator begin, stack_ty::iterator end);
  };

  struct SkippedCallRecord {
    KInstIterator caller;
    mutable unsigned int times;
    mutable std::vector<unsigned int> skippedAtDepth;

    SkippedCallRecord(KInstIterator caller)
      : caller(caller), times(0) {}
    SkippedCallRecord(const SkippedCallRecord &other)
      : caller(other.caller), times(other.times),
	skippedAtDepth(other.skippedAtDepth) {}
    SkippedCallRecord &operator=(const SkippedCallRecord &other) {
      if (&other == this)
	return *this;
      this->caller = other.caller;
      this->times = other.times;
      this->skippedAtDepth = other.skippedAtDepth;
      return *this;
    }

    bool operator==(const SkippedCallRecord &other) const {
      return caller == other.caller;
    }
    bool operator!=(const SkippedCallRecord &other) const {
      return caller != other.caller;
    }

    bool operator<(const SkippedCallRecord &other) const {
      return std::size_t(this->caller->inst) <
	std::size_t(other.caller->inst);
    }
    bool operator>(const SkippedCallRecord &other) const {
      return std::size_t(this->caller->inst) >
	std::size_t(other.caller->inst);
    }

    void skip(unsigned int atDepth) const {
      skippedAtDepth.push_back(atDepth);
      times++;
    }
  };

/// @brief ExecutionState representing a path under exploration
class ExecutionState {
public:
  typedef std::vector<StackFrame> stack_ty;

private:
  // unsupported, use copy constructor
  ExecutionState &operator=(const ExecutionState &);

  std::map<std::string, std::string> fnAliases;

public:
  // Execution - Control Flow specific

  /// @brief Pointer to instruction to be executed after the current
  /// instruction
  KInstIterator pc;

  /// @brief Pointer to instruction which is currently executed
  KInstIterator prevPC;

  /// @brief Stack representing the current instruction stream
  stack_ty stack;

  /// @brief Remember from which Basic Block control flow arrived
  /// (i.e. to select the right phi values)
  unsigned incomingBBIndex;

  // Overall state of the state - Data specific

  /// @brief Address space used by this state (e.g. Global and Heap)
  AddressSpace addressSpace;

  /// @brief Constraints collected so far
  ConstraintManager constraints;

  /// Statistics and information

  /// @brief Costs for all queries issued for this state, in seconds
  mutable double queryCost;

  /// @brief Weight assigned for importance of this state.  Can be
  /// used for searchers to decide what paths to explore
  double weight;

  /// @brief Exploration depth, i.e., number of times KLEE branched for this state
  unsigned depth;
  PortableCounter PC;

  /// @brief History of complete path: represents branches taken to
  /// reach/create this state (both concrete and symbolic)
  TreeOStream pathOS;

  /// @brief History of symbolic path: represents symbolic branches
  /// taken to reach/create this state
  TreeOStream symPathOS;

  /// @brief Counts how many instructions were executed since the last new
  /// instruction was covered.
  unsigned instsSinceCovNew;

  /// @brief Whether a new instruction was covered in this state
  bool coveredNew;

  /// @brief Disables forking for this state. Set by user code
  bool forkDisabled;

  /// @brief Set containing which lines in which files are covered by this state
  std::map<const std::string *, std::set<unsigned> > coveredLines;

  /// @brief Pointer to the process tree of the current state
  PTreeNode *ptreeNode;

  /// @brief Ordered list of symbolics: used to generate test cases.
  //
  // FIXME: Move to a shared list structure (not critical).
  std::vector<std::pair<const MemoryObject *, const Array *> > symbolics;

  /// @brief Set of used array names for this state.  Used to avoid collisions.
  std::set<std::string> arrayNames;

  // current depth symbolic execution is running on, 0 for main
  int invocationDepth;
  // as name indicates, -1 represents nothing is resolved
  // "resolved" means that the state has reached
  // the end of the path above a certain depth
  int resolvedInvocationDepth;
  // skipped calls
  std::queue<UnresolvedCaller> *skippedCalls;
  // calls to resolve
  std::queue<UnresolvedCaller> *callsToResolve;
  // set to true will resolve the next call seen
  bool resolveNextCall;
  // the call being resolved currently
  UnresolvedCaller tryingToResolve;
  // whether the state came across an error
  bool errorHappened;
  // a flag
  bool directTermOnErr;

  std::set<SkippedCallRecord> skippedCallRecords;

  // new instructions that this state claims to be able to cover
  std::set<unsigned int> proposeToCover;

  std::string getFnAlias(std::string fn);
  void addFnAlias(std::string old_fn, std::string new_fn);
  void removeFnAlias(std::string fn);

private:
  ExecutionState() : ptreeNode(0) {}

public:
  ExecutionState(KFunction *kf);

  // XXX total hack, just used to make a state so solver can
  // use on structure
  ExecutionState(const std::vector<ref<Expr> > &assumptions);

  ExecutionState(const ExecutionState &state);

  ~ExecutionState();

  ExecutionState *branch();

  void pushFrame(KInstIterator caller, KFunction *kf);
  void popFrame();

  void pushAll();
  void popAll();
  void recordStackFrame(UnresolvedCaller &uc);

  void addSymbolic(const MemoryObject *mo, const Array *array);
  void addConstraint(ref<Expr> e) { constraints.addConstraint(e); }

  bool merge(const ExecutionState &b);
  void dumpStack(llvm::raw_ostream &out) const;

  void printSCRs(llvm::raw_ostream *f) const;
};
}

#endif
