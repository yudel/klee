export C_INCLUDE_PATH=$PATH:/usr/include/x86_64-linux-gnu
export CPLUS_INCLUDE_PATH=$PATH:/usr/include/x86_64-linux-gnu
# llvm-3.8
if [ "$1" = "llvm-3.8" ]
then
    ./configure \
	--with-stp=/home/lenovo/stp/build \
	--with-uclibc=/home/lenovo/klee/klee-uclibc-3.8 \
	--enable-posix-runtime \
	--with-llvmsrc=/home/lenovo/llvm-3.8/llvm \
	--with-llvmobj=/home/lenovo/llvm-3.8/llvm/build \
	--with-llvmcc=/home/lenovo/llvm-3.8/llvm/build/Release+Asserts/bin/clang \
	--with-llvmcxx=/home/lenovo/llvm-3.8/llvm/build/Release+Asserts/bin/clang++
elif [ "$1" = "llvm-2.9" ]
then
    ./configure \
	--with-llvm=/home/lenovo/llvm-2.9 \
	--with-stp=/home/lenovo/stp/build \
	--with-uclibc=/home/lenovo/klee/klee-uclibc-old \
	--enable-posix-runtime
fi
