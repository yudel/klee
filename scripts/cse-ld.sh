#!/bin/bash

linkargs=""
for a in "$@"
do
    # hack
    contain=0
    for arg in $linkargs
    do
        if [[ $arg == $a ]];
	then
	    contain=1
	    break
	fi
    done
    if [[ $contain == 1 ]];
    then
	continue;
    fi
    unknown="-g -W -O -D -f -fnested-functions -pthread"
    for arg in $unknown
    do
        if [[ $arg == $a ]];
	then
	    contain=1
	    break
	fi
    done
    if [[ $contain == 1 ]];
    then
        continue
    elif [[ $a == -Wl* ]]; # also unknown
    then
        continue
    elif [[ $a == -l* ]]; # guess don't need
	 # however, llvm-ld is able to do this, it can link with .so
	 # libraries (llvm? or native!) as well as .a libraries (llvm)
	 # damn llvm-link
    then
	continue
    elif [[ $a == *.a ]]; # llvm-link doesn't support archives
    then
	libs=`llvm-ar t $a`
	dir=${a%/*}
	for lib in $libs
	do
	    if [[ $lib == areadlink_with_size.o ]];
	    then
		continue
	    fi
	    if [[ -f $dir/$lib ]];
	    then
		newarg=$dir/$lib
	    else
		madedir=libobjs
		if [[ ! -d $dir/$madedir ]];
		then
		    mkdir $dir/$madedir
		fi
		cp $a $dir/$madedir
		libname=${a##*/}
		origdir=`pwd`
		cd $dir/$madedir
		llvm-ar x $libname $lib
		cd $origdir
		newarg=$dir/$madedir/$lib
	    fi
	    # this will be very slow
	    contain=0
	    for arg in $linkargs
	    do
		if [[ $arg == *.o ]];
		then
		    existlib=${arg##*/}
		else
		    continue
		fi
		if [[ $existlib == $lib ]];
		then
		    contain=1
		    break
		elif [[ $existlib == csplit.o && $lib == xalloc-die.o ]];
		     # multiple definition problem
		then
		    contain=1
		    break
		fi
	    done
	    if [[ $contain == 0 ]];
	    then
		linkargs="$linkargs $newarg"
	    fi
	done
        continue
    fi

    linkargs="$linkargs $a"
done

llvm-link $linkargs
